import React, { useCallback, useContext, useEffect, useState } from 'react';

import { NotifMsg, NOTIF_STATUS, TIMEOUT_RESET_NOTIF, TIMEOUT_RESET_NOTIF_LONG, NOTIF_MSG_DEFAULT } from '../../entities/notification';
import { SYNC_WORKING_STATUS, SYNC_WORKING_STATUS_STR, SYNC_RESULT, HTTP_STATUS, SYNC_RESULT_STR } from '../../entities/status';
import { getYearsToSync, pull, push, sync, SyncMethod, SYNC_METHOD, SYNC_PROVIDER } from '../../entities/sync/sync';
import { isOnLine, isDefined, isTruthy } from '../../utils/utils';
import { Translate } from '../translate/translate';
import { Notification2 } from '../../components/library/notification/notification';
import { AppStateOptional, appStore } from 'app-state/app-state';
import { resetLoginData, setLoginData, setSyncMethod, setSyncStatus, setState } from 'app-state/app-actions';
import { arraysEqual } from 'utils/array';
import { sentenceCase } from 'utils/string';
import { dbxGetExpiresIn, dbxRefreshToken } from 'entities/sync/dropbox';
import { getLogDate, timestamp } from 'utils/date';
import { Log } from 'utils/logs';
import { checkObservationalDaysVersion, migrateObservationalDays } from 'entities/objects/observational-day';
import { useLog } from 'hooks/useLog';


export function Sync() {
  const [displayNotification, setDisplayNotification] = useState<NotifMsg>(NOTIF_MSG_DEFAULT);
  const appState = useContext(appStore);
  const [yearsToSync, setYearsToSync] = useState(getYearsToSync(appState.state.focusedDate));

  const onDiplayNotification = (notif: NotifMsg) => {
    setDisplayNotification({ ...displayNotification, ...notif });
    setTimeout(() => {
      setDisplayNotification(NOTIF_MSG_DEFAULT);
    }, notif.timeout || TIMEOUT_RESET_NOTIF);
  };

  const isConnected = () => appState.state.login.status;

  const onPushSuccess = (_response: any): AppStateOptional => {
    Log.debug('Push successfull');
    return {};
  };

  const onSyncSuccess = (response: any, fullSync = false): AppStateOptional => {
    Log.debug(`Sync result: ${SYNC_RESULT_STR[response.status]}`);
    if (!isDefined(response.data)) {
      throw Error('There is no data in the response');
    } else {
      Log.debug('Sync successfull');
      if (response.status === SYNC_RESULT.UPDATE_CLIENT_DATA
        || response.status === SYNC_RESULT.UPDATE_BOTH_DATA || fullSync) {
        return { observationalDays: response.data };
      } else {
        return {};
      }
    }
  };

  const onFullSyncSuccess = (response: any): AppStateOptional => onSyncSuccess(response, true);

  const onPullSuccess = (response: any): AppStateOptional => {
    if (!isDefined(response.serverData)) {
      throw Error('There is no serverData in the response');
    } else {
      Log.debug('Pull successfull');
      return { observationalDays: response.serverData };
    }
  };

  type SyncActions = {
    [index in SyncMethod]: {
      action: () => any;
      onSuccess: (response: any) => AppStateOptional;
    };
  };

  const syncActions: SyncActions = {
    [SYNC_METHOD.PUSH]: {
      action: () => push(appState.state.login, appState.state.observationalDays),
      onSuccess: onPushSuccess,
    },
    [SYNC_METHOD.SYNC]: {
      action: () => sync(appState.state.login, appState.state.observationalDays, yearsToSync),
      onSuccess: onSyncSuccess,
    },
    [SYNC_METHOD.FULL_SYNC]: {
      action: () => sync(appState.state.login, appState.state.observationalDays, yearsToSync, true),
      onSuccess: onFullSyncSuccess,
    },
    [SYNC_METHOD.PULL]: {
      action: () => pull(appState.state.login),
      onSuccess: onPullSuccess,
    },
  };

  const refreshDBXToken = () => {
    Log.debug('Check DBX refresh token');
    const time = timestamp();
    const expiresIn = appState.state.login.data.expiresIn;// - (4*3600) + 500;
    const timeLog = getLogDate(new Date(time * 1000));
    const expiresInLog = getLogDate(new Date(expiresIn * 1000));
    if (expiresIn <= time) {
      Log.debug(`Refresh DBX token (${expiresInLog} <= ${timeLog})`);
      dbxRefreshToken(appState.state.login.data.refreshToken)
        .then((data: any) => {
          setLoginData(appState, {
            provider: SYNC_PROVIDER.DBX, data: {
              ...appState.state.login.data,
              accessToken: data.access_token,
              expiresIn: dbxGetExpiresIn(data.expires_in),
            },
          });
          Log.debug('DBX token refreshed');
        })
        .catch((error) => {
          Log.error(error);
          onDiplayNotification({ notifStatus: NOTIF_STATUS.KO, message: 'sync.refresh_token_error', timeout: TIMEOUT_RESET_NOTIF_LONG });
          if (isDefined(error.response) && error.response.status === HTTP_STATUS.UNAUTHORIZED) {
            Log.error(`You have been logged out from your ${appState.state.login.provider} account`);
            resetLoginData(appState);
          }
          setSyncStatus(appState, SYNC_WORKING_STATUS.KO);
        });
      return false;
    } else {
      Log.debug(`No need to refresh DBX token (${expiresInLog} > ${timeLog})`);
      return true;
    }
  };

  const doSyncAction = () => {
    const actionName = appState.state.syncMethod;
    const action = syncActions[actionName].action;
    const onSuccess = syncActions[actionName].onSuccess;

    /* Reset sync method */
    setSyncMethod(appState, SYNC_METHOD.SYNC);

    Log.debug(`${sentenceCase(actionName)} launched`);
    action()
      .then((response: any) => onSuccess(response))
      .then((state: AppStateOptional) => setState(appState, { ...state, syncStatus: SYNC_WORKING_STATUS.OK }))
      .catch((error) => {
        if (isDefined(error.response) && error.response.status === SYNC_RESULT.MUST_UPGRADE) {
          Log.error('Data from server too new');
          onDiplayNotification({ notifStatus: NOTIF_STATUS.KO, message: 'sync.must_upgrade', timeout: TIMEOUT_RESET_NOTIF_LONG });
        } else {
          Log.error(`Error with ${appState.state.login.provider} ${actionName}`);
          Log.error(error);
          onDiplayNotification({ notifStatus: NOTIF_STATUS.KO, message: 'sync.ko' });
        }
        setSyncStatus(appState, SYNC_WORKING_STATUS.KO);
      });
  };

  const doMigration = () => {
    Log.debug('Migrate data');
    const migratedObsDays = migrateObservationalDays(appState.state.observationalDays);
    Log.debug('Migration done');
    setDisplayNotification(NOTIF_MSG_DEFAULT);
    setState(appState, { syncStatus: SYNC_WORKING_STATUS.UNKNOWN, observationalDays: migratedObsDays });
  };

  const doCheckAndSync = () => {
    if (!isConnected()) {
      Log.debug(`Not launching ${appState.state.syncMethod}: we are not connected to any sync service`);
      setSyncStatus(appState, SYNC_WORKING_STATUS.UNKNOWN);
      setDisplayNotification(NOTIF_MSG_DEFAULT);
    } else if (!isOnLine()) {
      Log.debug(`Not launching ${appState.state.syncMethod}: we are offline`);
      setSyncStatus(appState, SYNC_WORKING_STATUS.UNKNOWN);
    } else {
      if (appState.state.login.provider === SYNC_PROVIDER.DBX) {
        if (refreshDBXToken()) {
          setSyncStatus(appState, SYNC_WORKING_STATUS.WORKING);
        }
      } else {
        setSyncStatus(appState, SYNC_WORKING_STATUS.WORKING);
      }
    }
  };

  const isSyncFinished = useCallback(
    () =>
      appState.state.syncStatus === SYNC_WORKING_STATUS.OK ||
      appState.state.syncStatus === SYNC_WORKING_STATUS.KO,
    [appState.state.syncStatus],
  );

  useEffect(() => {
    const years = getYearsToSync(appState.state.focusedDate);
    if (!arraysEqual(yearsToSync, years)) {
      setYearsToSync(years);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [appState.state.focusedDate]);

  useEffect(() => {
    Log.debug(`Sync status changed: ${SYNC_WORKING_STATUS_STR[appState.state.syncStatus]}`);
    if (isSyncFinished()) {
      setSyncStatus(appState, SYNC_WORKING_STATUS.UNKNOWN);
    } else if (appState.state.syncStatus === SYNC_WORKING_STATUS.WORKING) {
      doSyncAction();
    } else if (appState.state.syncStatus === SYNC_WORKING_STATUS.TODO) {
      doCheckAndSync();
    } else if (appState.state.syncStatus === SYNC_WORKING_STATUS.PRE_MIGRATE) {
      setDisplayNotification({ title: 'notification.info', notifStatus: NOTIF_STATUS.MIGRATE, message: 'sync.migrate' });
      const syncMethod = (appState.state.syncMethod === SYNC_METHOD.PUSH) ? appState.state.syncMethod : SYNC_METHOD.FULL_SYNC;
      setState(appState, { syncMethod, syncStatus: SYNC_WORKING_STATUS.MIGRATE });
    } else if (appState.state.syncStatus === SYNC_WORKING_STATUS.MIGRATE) {
      doMigration();
    }
  }, [appState.state.syncStatus]);

  useLog(`Online status: ${appState.state.onLineStatus}`, appState.state.onLineStatus);
  useLog('observationalDays changed', appState.state.observationalDays);
  useLog('Login data changed', appState.state.login.data);
  useLog('yearsToSync changed', yearsToSync);

  useEffect(() => {
    const canWeSync = isTruthy(isConnected()) &&
      appState.state.onLineStatus &&
      appState.state.syncStatus === SYNC_WORKING_STATUS.UNKNOWN;

    Log.debug(`State changed: ${SYNC_WORKING_STATUS_STR[appState.state.syncStatus]}`);
    if (!checkObservationalDaysVersion(appState.state.observationalDays)) {
      setSyncStatus(appState, SYNC_WORKING_STATUS.PRE_MIGRATE);
    } else if (canWeSync) {
      Log.debug('trigger sync');
      setSyncStatus(appState, SYNC_WORKING_STATUS.TODO);
    }
  }, [
    appState.state.observationalDays,
    appState.state.onLineStatus,
    appState.state.login.data,
    yearsToSync,
  ]);

  return (
    <>
      {
        displayNotification.notifStatus !== NOTIF_STATUS.NONE && <Notification2 title={<Translate name={displayNotification.title} />} message={<Translate name={displayNotification.message} />}></Notification2>
      }
    </>
  );
}
