import React, { useContext } from 'react';

import { isDefined } from '../../utils/utils';
import { translate, TranslateProps } from 'i18n/i18nService';
import { getLanguage } from 'app-state/app-actions';
import { appStore } from 'app-state/app-state';

export function Translate({
  lang,
  name,
  filter = (s) => s,
  parameters = {},
}: TranslateProps) {
  const appState = useContext(appStore);
  const useLang = isDefined(lang) ? lang : getLanguage(appState);

  return <>{translate({ lang: useLang, name, parameters, filter })}</>;
}
