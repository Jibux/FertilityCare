import React, { useState, useRef, useContext, useEffect, memo } from 'react';
import { useScroll, useWindowSize } from 'react-use';

import { Stack } from '../../layout/stack/stack';
import { Container } from '../../layout/container/container';
import { ColumnRigid } from '../../layout/columns/column-rigid/column-rigid';
import { Inline } from '../../layout/inline/inline';
import { addDays, getYesterday, isBeforeToday, isToday } from '../../utils/date';
import { Children } from '../../types/children';
import { appStore } from 'app-state/app-state';
import { getLanguage } from 'app-state/app-actions';
import { createCycle, getCycleIndex, getCycles, getSpecificCycle, ICycle, isTheSameCycle } from 'entities/cycle/cycle';
import { filterPositiveInteger } from 'utils/utils';
import { Translate } from 'components/translate/translate';
import { getFullDateStringNumeric } from 'i18n/dates';

type Props = {
  focusedDate?: Date;
  dayRenderer: (date: Date) => Children;
  onClick?: (date: Date) => void;
};

const WEEKS_PER_MONTH = 5;
const DAYS_PER_WEEK = 7;
const DEFAULT_SIZE = DAYS_PER_WEEK * WEEKS_PER_MONTH;
const NR_OF_CYCLES_TO_DISPLAY = 5; /* This should be an odd number */

type SlideProps = {
  dayRenderer: Children;
  refProp?: any;
  cycle: ICycle;
  isTheLastCycle: boolean;
  onClick?: (date: Date) => void;
};


const CycleSlide = memo(function CycleSlide0({ cycle, refProp, dayRenderer, isTheLastCycle, onClick }: SlideProps) {
  const size = DEFAULT_SIZE > cycle.length() ? DEFAULT_SIZE : cycle.length() + DAYS_PER_WEEK - (cycle.length() % DAYS_PER_WEEK);
  const days = new Array(size).fill(undefined).map((_, i) => addDays(i, cycle.firstDay));

  const isDayInCycle = (day: Date) => day <= cycle.lastDay;
  const editable = (day: Date) => isDayInCycle(day) || (isTheLastCycle && (isBeforeToday(day) || isToday(day)));
  const getBackgroundCss = (day: Date) =>
    editable(day)
      ? isToday(day)
        ? 'var(--color-primary-light)'
        : 'var(--color-grey-light)'
      : 'var(--color-white)';

  return (
    <div style={{
      position: 'relative',
      width: '100%',
      height: '100%',
      flex: 'none',
      overflowY: 'scroll',
      scrollSnapAlign: 'none center',
    }}
      ref={refProp}
    >
      <Inline height="100%" columns={7} align="STRETCH" justify="SPACE_AROUND">
        {
          days.map((day) => (
            <ColumnRigid height="20%" key={day.valueOf()} contentFit>
              <button
                type="button"
                onClick={() => editable(day) ? onClick(day) : false}
                style={{
                  color: 'var(--color-black)',
                  height: '100%',
                  width: '100%',
                  overflow: 'hidden',
                  boxSizing: 'border-box',
                  border: '0.5px solid',
                  fontSize: '0.5rem',
                  display: 'flex',
                  textAlign: 'start',
                  padding: '0.125rem 0.25rem',
                  background: getBackgroundCss(day),
                }}
              >
                {editable(day) && <div style={{ width: '100%', height: '100%' }}>{dayRenderer(day)}</div>}
              </button>
            </ColumnRigid>
          ))
        }
      </Inline>
    </div>
  );
});


const cyclesNeedUpdate = (cls1: ICycle[], cls2: ICycle[], index: number) => {
  if (cls1.length !== cls2.length || cls1.length === 0)
    return true;
  const c1 = getSpecificCycle(cls1, index);
  const c2 = getSpecificCycle(cls2, index);
  return c1.length() !== c2.length();
};

export function Cycle({ dayRenderer, onClick }: Props) {
  const appState = useContext(appStore);
  const ref = useRef(null);
  const scrollRef = useRef(null);
  const { x } = useScroll(ref);
  const [cycles, setCycles] = useState([]);
  const [cyclesToDisplay, setCyclesToDisplay] = useState([]);
  const [indexRef, setIndexRef] = useState(4);
  const { width, height } = useWindowSize();
  const [lockScroll, setLockScroll] = useState(true);

  const isFocusedCycleTheLastOne = () =>
    cycles.length - 1 === indexRef;

  const getFocusedCycle = () => getSpecificCycle(cycles, indexRef);
  const getLastCycle = () => getSpecificCycle(cycles, cycles.length - 1);
  const isTheLastCycle = (c: ICycle) => isTheSameCycle(c, getLastCycle());

  const scrollToRef = (r) => {
    if (r && r.current) {
      r.current.parentElement.scrollTo(r.current.offsetLeft, 0);
      /* If we have reached the end scroll to bottom */
      if (isFocusedCycleTheLastOne()) {
        r.current.scrollTo(0, height);
      }
    }
    setTimeout(() => setLockScroll(false), 1000);
  };

  const filterIndexRef = (index: number, cls: ICycle[] = cycles) =>
    filterPositiveInteger((index >= cls.length) ? cls.length - 1 : index, 0);

  const updateCycles = (cls: ICycle[]) => cls.length === 0 ? setCycles([createCycle()]) : setCycles(cls);

  const updateIndexRef = (index: number, cls: ICycle[] = cycles) => {
    const newIndex = filterIndexRef(index, cls);
    if (newIndex === 0) {
      const cyclesToAdd = getCycles(appState.state.observationalDays, NR_OF_CYCLES_TO_DISPLAY, getYesterday(getSpecificCycle(cls, 0).firstDay));
      const newCycles = [...cyclesToAdd, ...cls];
      updateCycles(newCycles);
      console.debug(`New cycles ${cls.length}`);
      setIndexRef(filterIndexRef(cyclesToAdd.length, newCycles));
    } else {
      setIndexRef(newIndex);
      console.debug('New index');
      setTimeout(() => setLockScroll(false), 1000);
    }
  };

  const browseCycles = (i: number) => {
    setLockScroll(true);
    const newIndexRef = getCycleIndex(cycles, cyclesToDisplay[i]);
    console.debug(`Browse ${i} - newIndexRef ${newIndexRef}`);
    updateIndexRef(newIndexRef);
  };

  const getDateParameter = (date: Date) =>
    getFullDateStringNumeric(date, getLanguage(appState));

  const initCycles = () => {
    const nr_of_cycles = cycles.length > NR_OF_CYCLES_TO_DISPLAY ? cycles.length : NR_OF_CYCLES_TO_DISPLAY;
    const cls = getCycles(appState.state.observationalDays, nr_of_cycles);
    if (cyclesNeedUpdate(cycles, cls, indexRef)) {
      const index = cls.length - 1;
      updateCycles(cls);
      setIndexRef(filterIndexRef(index, cls));
    }
  };

  const updateCyclesToDisplay = () => {
    if (cycles.length > 0) {
      const nr_of_cycles = NR_OF_CYCLES_TO_DISPLAY - 1;
      const offset = cycles.length - 1 - indexRef;
      const begin = filterPositiveInteger((offset < nr_of_cycles) ? indexRef - (nr_of_cycles - offset) : indexRef - nr_of_cycles / 2, 0);
      const end = begin + nr_of_cycles;
      console.debug(`${begin} ${end}`);
      const cls = cycles.filter((cycle: ICycle, i: number) => (i <= end) && (i >= begin));
      setCyclesToDisplay(cls.length === 0 ? [createCycle()] : cls);
    }
  };

  useEffect(() => {
    if (cyclesToDisplay.length > 0) {
      console.debug(cyclesToDisplay);
      scrollToRef(scrollRef);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cyclesToDisplay]);

  useEffect(() => {
    console.debug(`indexRef ${indexRef}`);
    const fc = getFocusedCycle();
    const cycleIndex = getCycleIndex(cyclesToDisplay, fc);
    if (cycleIndex === 0 || cyclesToDisplay.length === 0 || (cycleIndex === cyclesToDisplay.length - 1 && indexRef < cycles.length - 1)) {
      updateCyclesToDisplay();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [indexRef, cycles]);

  useEffect(() => {
    updateCyclesToDisplay();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cycles]);

  useEffect(() => {
    console.debug('Obs days changed');
    initCycles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [appState.state.observationalDays]);

  useEffect(() => {
    if (ref && cyclesToDisplay.length > 0 && !lockScroll) {
      const w = ref.current.offsetWidth;
      console.debug(`${x} ${w}`);
      if (x % w === 0) {
        browseCycles(x / w);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ref, x]);

  useEffect(() => {
    scrollToRef(scrollRef);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [width]);

  return (
    <Stack height="100%">
      <Container color="SECONDARY" paddingY="XS" paddingX="S">
        <Translate name="cycle.summary.message" parameters={{ beginDate: getDateParameter(getFocusedCycle().firstDay), endDate: getDateParameter(getFocusedCycle().lastDay), length: getFocusedCycle().length() }} />
      </Container>
      <div
        style={{
          position: 'relative',
          height: '100%',
          display: 'flex',
          overflowX: 'scroll',
          overflowY: 'hidden',
          scrollbarWidth: 'none',  /* Firefox */
          scrollSnapType: 'x mandatory',
        }}
        ref={ref}
      >
        {cyclesToDisplay
          .map(
            (c: ICycle, i: number) =>
              (isTheSameCycle(c, getFocusedCycle()))
                ? <CycleSlide key={i} cycle={c} isTheLastCycle={isTheLastCycle(c)} refProp={scrollRef} dayRenderer={dayRenderer} onClick={onClick}></CycleSlide>
                : <CycleSlide key={i} cycle={c} isTheLastCycle={isTheLastCycle(c)} dayRenderer={dayRenderer} onClick={onClick}></CycleSlide>,
          )
        }
      </div>
    </Stack>
  );
}