import React from 'react';

import './overlay.css';
import { Children } from '../../../types/children';

type Props = {
  children: Children;
};
export function Overlay({ children }: Props) {
  return (
    <div className="s-overlay">
      {children}
    </div>
  );
}