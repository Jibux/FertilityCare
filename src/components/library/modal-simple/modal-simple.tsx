import React, { ReactNode } from 'react';
import { createPortal } from 'react-dom';

import { Overlay } from '../overlay/overlay';
import { Card } from '../card/card';
import { Text2 } from '../text/text/text';
import { Stack } from 'layout/stack/stack';

type Props = {
  message: ReactNode;
  children: ReactNode;
  onClose?: () => void;
};
export function ModalSimple({ message, children }: Props) {
  return createPortal(
    <Overlay>
      <div style={{ width: '500px', maxWidth: '95vw' }}>
        <Card>
          <Stack spacing='M'>
            <Text2>{message}</Text2>
            {children}
          </Stack>
        </Card>
      </div>
    </Overlay>
    , document.querySelector('body'),
  );
}