import React, { ReactNode } from 'react';
import { createPortal } from 'react-dom';

import { Overlay } from '../overlay/overlay';
import { CardWithHeader } from '../card-with-header/card-with-header';
import { Label } from '../text/label/label';

type Props = {
  title: ReactNode;
  children: ReactNode;
  onClose?: () => void;
};
export function Modal({ title, children, onClose }: Props) {
  return createPortal(
    <Overlay>
      <div style={{ width: '500px', maxWidth: '95vw' }}>
        <CardWithHeader onClose={onClose} maxHeight="calc(100vh - 6rem)">
          {{
            header: <Label>{title}</Label>,
            content: children,
          }}
        </CardWithHeader>
      </div>
    </Overlay>
    , document.querySelector('body'),
  );
}