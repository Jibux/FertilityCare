import React from 'react';

import './avatar.css';

import { Children } from '../../../types/children';
import { Text2 } from '../text/text/text';
import { Icon } from '../icon/icon';

type Props = {
  image?: string;
  name?: Children;
};
export function Avatar({ image, name }: Props) {
  return (
    <div className="s-avatar-container">
      {image
        ? <div className="s-avatar" style={{ backgroundImage:`url(${image})` }} />
        : <div className="s-avatar">
            <Icon  name="user-unknown" />
          </div>
      }
      { name && <Text2 className="s-avatar__name">{name}</Text2>}
    </div>
  );
}