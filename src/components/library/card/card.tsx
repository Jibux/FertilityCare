import React from 'react';

import { Children } from '../../../types/children';
import { Container } from '../../../layout/container/container';

type Props = {
  children: Children,
};
export function Card({ children }: Props) {
  return (
  <Container color="WHITE" shadow="M" padding="M" radius="default" overflow={false} scroll={false}>
    {children}
  </Container>
  );
}