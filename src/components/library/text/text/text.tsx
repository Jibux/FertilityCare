import React from 'react';

import { Children } from '../../../../types/children';
import { StylableComponent } from '../../../../types/stylable-component';
import './text.css';

type Props = {
  children: Children;
};

export function Text2({ children, className }: StylableComponent<Props>) {
  return (
    <span className={`s-text ${className || ''}`}>
      {children}
    </span>
  );
}