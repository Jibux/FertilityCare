import React from 'react';

import { Children } from '../../../../types/children';
import { StylableComponent } from '../../../../types/stylable-component';
import './label.css';

type Props = {
  children: Children;
  htmlFor?: string; 
};

export function Label({ children, className, htmlFor }: StylableComponent<Props>) {
  return (
    <label htmlFor={htmlFor} className={`s-label ${className || ''}`}>
      {children}
    </label>
  );
}