import React from 'react';

import { Children } from '../../../../types/children';
import { StylableComponent } from '../../../../types/stylable-component';
import './sub-title.css';

type Props = {
  children: Children;
};

export function SubTitle({ children, className }: StylableComponent<Props>) {
  return (
    <h2 className={`s-sub-title ${className || ''}`}>
      {children}
    </h2>
  );
}