import React from 'react';

import { Children } from '../../../../types/children';
import { StylableComponent } from '../../../../types/stylable-component';
import './note.css';

type Props = {
  children: Children;
};
export function Note({ children, className }: StylableComponent<Props>) {
  return (
    <span className={`s-note ${className || ''}`}>
      {children}
    </span>
  );
}