import React from 'react';

import { Children } from '../../../../types/children';
import { StylableComponent } from '../../../../types/stylable-component';
import './title.css';

type Props = {
  children: Children;
};

export function Title({ children, className }: StylableComponent<Props>) {
  return (
    <h1 className={`s-title ${className || ''}`}>
      {children}
    </h1>
  );
}