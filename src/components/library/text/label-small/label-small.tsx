import React from 'react';
import { classNames } from 'utils/classnames';

import { Children } from '../../../../types/children';
import { StylableComponent } from '../../../../types/stylable-component';
import './label-small.css';

type Props = {
  children: Children;
  htmlFor?: string; 
};

export function LabelSmall({ children, className, htmlFor }: StylableComponent<Props>) {
  return (
    <label htmlFor={htmlFor} className={classNames(['s-label-small', `s-label-small--${className || ''}`])}>
      {children}
    </label>
  );
}