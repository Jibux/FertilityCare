import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

import { Children } from '../../../../types/children';
import { StylableComponent } from '../../../../types/stylable-component';
import { ROUTES } from '../../../../routes';
import './link.css';

type Props = {
  children: Children;
  to: ValueOf<typeof ROUTES>;
};

export function Link({ children, to, className }: StylableComponent<Props>) {
  return (
    <RouterLink to={to} className={`s-link ${className || ''}`}>
      {children}
    </RouterLink>
  );
}
