import React, { ReactNode, useState, useRef } from 'react';
import { useKey, useClickAway } from 'react-use';

import { Card } from '../card/card';
import { Children } from '../../../types/children';
import { classNames } from '../../../utils/classnames';
import './dropdown.css';

type Props = {
  children: Children;
  dropDownContent: { [key: string]: ReactNode };
  position?: 'left' | 'right';
  onClick?: (key: string) => void;
};
export function Dropdown({ children, dropDownContent, position = 'left', onClick }: Props) {
  const ref = useRef(null);
  const [isOpened, setOpened] = useState(false);
  useKey('Escape', () => setOpened(false));
  useClickAway(ref, () => setOpened(false));

  const clickOnItem = (key: string) => {
    setOpened(false);
    onClick(key);
  };

  return (
    <div ref={ref} className={classNames(['s-dropdown', 's-dropdown--' + position])}>
      <div className="s-dropdown__label" onClick={() => setOpened(!isOpened)}>
        {children}
      </div>
      {isOpened && (<div className="s-dropdown__content">
        <Card>
          <ul>
          {Object.entries(dropDownContent).map(([key, content]) => <li onClick={() => clickOnItem(key)} key={key}>{content}</li>)}
          </ul>
        </Card>
      </div>)}
    </div>
  );
}