import React from 'react';

import { __Button as Button } from './__button';
import { IconName, Icon } from '../icon/icon';

type Props = {
  onClick: () => void;
  icon: IconName;
  disabled?: boolean;
};

export function ButtonIcon({ onClick, icon, disabled }: Props) {
  return (
    <Button type="no-style" onClick={onClick} disabled={disabled}>
      <div style={{ flex: 'none', display: 'flex', fontSize: '1.5em' }}>
        <Icon name={icon}></Icon>
      </div>
    </Button>
  );
}
