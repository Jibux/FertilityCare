import React from 'react';

import './__button.css';
import { Text2 } from '../text/text/text';
import { Icon, IconName } from '../icon/icon';
import { Children } from '../../../types/children';

export type ButtonProps = {
  onClick: () => void;
  iconLeft?: IconName;
  iconRight?: IconName;
  children: Children;
  disabled?: boolean;
}; 

type Props = {
  type: 'ghost-inverted' | 'ghost' | 'primary' | 'secondary' | 'no-style';
} & ButtonProps;

export const __Button = ({ type, children, onClick, iconLeft, iconRight, disabled = false }: Props) => (
  <button className={`s-button s-button--${type}`} disabled={disabled} onClick={() => {if (!disabled) {onClick();}}}>
    <span className="s-button__content">
      { iconLeft && <span  className="s-button__icon"><Icon name={iconLeft}></Icon></span>}
      <span className="s-button__text">
        <Text2>{children}</Text2>
      </span>
      { iconRight && <span  className="s-button__icon"><Icon name={iconRight}></Icon></span>}
    </span>
  </button>
);