import React from 'react';

import { __Button as Button, ButtonProps } from './__button';

export function ButtonPrimary({ onClick, iconLeft, iconRight, children, disabled }: ButtonProps) {
  return (
    <Button type="primary" onClick={onClick} iconLeft={iconLeft} iconRight={iconRight} disabled={disabled}>
      {children}
    </Button>
  );
}
