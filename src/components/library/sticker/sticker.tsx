import { Size } from 'entities/size';
import React from 'react';

import { StickerColor, stickerHasBaby } from '../../../entities/color';
import { classNames } from '../../../utils/classnames';
import { Icon } from '../icon/icon';
import './sticker.css';

type Props = {
  type: StickerColor
  size?: Size
};
export function Sticker({ type, size = 'L' }: Props) {
  return (
    <div className={classNames(['s-sticker', `s-sticker--${type}`, `s-sticker--${size}`])}>
      { stickerHasBaby(type) && <Icon name="baby" size={size} />}
    </div>
  );
}