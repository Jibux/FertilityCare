import React from 'react';

import './card-with-header.css';
import { Children } from '../../../types/children';
import { Card } from '../card/card';
import { Header } from '../header/header';
import { ButtonIcon } from '../buttons/button-icon';

type Props = {
  onClose?: () => void;
  children: {
    header: Children;
    content: Children;
  };
  maxHeight?: string;
};

export function CardWithHeader({ maxHeight, children: { header, content }, onClose }: Props) {
  return (
    <Card>
      <Header className="s-card-with-header__header">
        <>
          {header}
        </>
        {onClose && <ButtonIcon icon="cross" onClick={onClose}></ButtonIcon>}
      </Header>
      <div style={{
        height: '100%',
        maxHeight,
        overflowY: 'auto',
        overflowX: 'hidden',
      }}>
      {content}
      </div>
    </Card>
  );
}