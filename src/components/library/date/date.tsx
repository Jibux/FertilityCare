import React from 'react';

import { format } from '../../../utils/date';

export function DateComp({ children }: { children: Date }) {
  return <>{format(children)}</>;
}