import React, { ReactNode } from 'react';

import { StylableComponent } from '../../../types/stylable-component';
import { classNames } from '../../../utils/classnames';
import './header.css';

type Props = {
  children: ReactNode | [ReactNode, ReactNode];
};

export function Header({ children, className }: StylableComponent<Props>) {
  const [left, right] = Array.isArray(children)
    ? [children[0], children[1]]
    : [children, undefined];

  return (
    <div className={classNames(['s-header',  className])}>
      {left}
      {right}
    </div>
  );
}