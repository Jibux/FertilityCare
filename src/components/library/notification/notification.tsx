import React, { ReactNode } from 'react';

import './notification.css';
import { Icon } from '../icon/icon';
import { Label } from '../text/label/label';
import { LabelSmall } from '../text/label-small/label-small';

type Props = {
  title: ReactNode;
  message: ReactNode;
};
export function Notification2({ title, message }: Props) {
  return (
    <div className="s-notification">
      <div className="s-notification__icon">
        <Icon name="error"></Icon>
      </div>
      <div className="s-notification__content">
        <Label>{title}</Label>
        <LabelSmall>{message}</LabelSmall>
      </div>
    </div>
  );
}