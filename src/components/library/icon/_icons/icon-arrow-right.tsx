import React from 'react';

export default () => (
  <svg width="1em" height="1em" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
  <rect x="25" y="15" width="2" height="21" rx="1" transform="rotate(90 25 15)" fill="currentColor"/>
  <path fillRule="evenodd" clipRule="evenodd" d="M15.2929 7.29289C15.6834 6.90237 16.3166 6.90237 16.7071 7.29289L24.7071 15.2929C25.0976 15.6834 25.0976 16.3166 24.7071 16.7071L16.7071 24.7071C16.3166 25.0976 15.6834 25.0976 15.2929 24.7071C14.9024 24.3166 14.9024 23.6834 15.2929 23.2929L22.5858 16L15.2929 8.70711C14.9024 8.31658 14.9024 7.68342 15.2929 7.29289Z" fill="currentColor"/>
  </svg>
);