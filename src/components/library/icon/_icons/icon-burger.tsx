import * as React from 'react';

export default () => (
  <svg width="1em" height="1em" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M4 7a1 1 0 011-1h22a1 1 0 110 2H5a1 1 0 01-1-1zM4 16a1 1 0 011-1h22a1 1 0 110 2H5a1 1 0 01-1-1zM4 25a1 1 0 011-1h22a1 1 0 110 2H5a1 1 0 01-1-1z" fill="currentColor" />
  </svg>
);
