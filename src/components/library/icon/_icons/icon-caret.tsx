import React from 'react';

export default () => (
  <svg width="1em" height="1em" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M11.415 13.6508C10.859 13.0021 11.3199 12 12.1742 12H19.8258C20.6801 12 21.141 13.0021 20.585 13.6508L16.7593 18.1142C16.3602 18.5798 15.6398 18.5798 15.2407 18.1142L11.415 13.6508Z" fill="currentColor"/>
  </svg>
);