import React from 'react';

export default () => (
  <svg width="1em" height="1em" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
  <rect x="7" y="17" width="2" height="21" rx="1" transform="rotate(-90 7 17)" fill="currentColor"/>
  <path fillRule="evenodd" clipRule="evenodd" d="M16.7071 24.7071C16.3166 25.0976 15.6834 25.0976 15.2929 24.7071L7.29289 16.7071C6.90237 16.3166 6.90237 15.6834 7.29289 15.2929L15.2929 7.29289C15.6834 6.90237 16.3166 6.90237 16.7071 7.29289C17.0976 7.68342 17.0976 8.31658 16.7071 8.70711L9.41421 16L16.7071 23.2929C17.0976 23.6834 17.0976 24.3166 16.7071 24.7071Z" fill="currentColor"/>
  </svg>
);