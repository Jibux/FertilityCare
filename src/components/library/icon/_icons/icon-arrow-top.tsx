import React from 'react';

export default () => (
  <svg width="1em" height="1em" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
  <rect x="15" y="7" width="2" height="21" rx="1" fill="currentColor"/>
  <path fillRule="evenodd" clipRule="evenodd" d="M7.29289 16.7071C6.90237 16.3166 6.90237 15.6834 7.29289 15.2929L15.2929 7.29289C15.6834 6.90237 16.3166 6.90237 16.7071 7.29289L24.7071 15.2929C25.0976 15.6834 25.0976 16.3166 24.7071 16.7071C24.3166 17.0976 23.6834 17.0976 23.2929 16.7071L16 9.41421L8.70711 16.7071C8.31658 17.0976 7.68342 17.0976 7.29289 16.7071Z" fill="currentColor"/>
  </svg>
);