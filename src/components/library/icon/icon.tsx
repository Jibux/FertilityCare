import ArrowTop from './_icons/icon-arrow-top';
import ArrowBottom from './_icons/icon-arrow-bottom';
import ArrowLeft from './_icons/icon-arrow-left';
import ArrowRight from './_icons/icon-arrow-right';
import Backward from './_icons/icon-backward';
import CalendarCycle from './_icons/icon-calendar-cycle';
import CalendarDaily from './_icons/icon-calendar-daily';
import CalendarDay from './_icons/icon-calendar-day';
import CalendarMonthly from './_icons/icon-calendar-monthly';
import Caret from './_icons/icon-caret';
import Check from './_icons/icon-check';
import ChevronBottom from './_icons/icon-chevron-bottom';
import ChevronLeft from './_icons/icon-chevron-left';
import ChevronRight from './_icons/icon-chevron-right';
import ChevronTop from './_icons/icon-chevron-top';
import Cog from './_icons/icon-cog';
import Cross from './_icons/icon-cross';
import Enter from './_icons/icon-enter';
import IconError from './_icons/icon-error';
import Exit from './_icons/icon-exit';
import EyeNone from './_icons/icon-eye-none';
import Eye from './_icons/icon-eye';
import Forward from './_icons/icon-forward';
import Pen from './_icons/icon-pen';
import Plug from './_icons/icon-plug';
import Sync from './_icons/icon-sync';
import UserAdd from './_icons/icon-user-add';
import UserUnknown from './_icons/icon-user-unknown';
import User from './_icons/icon-user';
import Burger from './_icons/icon-burger';
import Ball from './_icons/icon-ball';
import Baby from './_icons/icon-baby';
import { Spacing } from '../../../entities/spacing';
import { classNames } from '../../../utils/classnames';
import './icon.css';
import './hide-me.css';

export type IconName = 'arrow-top' | 'arrow-bottom' | 'arrow-left' | 'arrow-right' | 'backward' | 'calendar-cycle' | 'calendar-daily' | 'calendar-day' | 'calendar-monthly' | 'caret' | 'check' | 'chevron-bottom' | 'chevron-left' | 'chevron-right' | 'chevron-top' | 'cog' | 'cross' | 'enter' | 'error' | 'exit' | 'eye-none' | 'eye' | 'forward' | 'pen' | 'plug' | 'sync' | 'user-add' | 'user-unknown' | 'user' | 'burger' | 'ball' | 'baby';

const icons: {
  [key in IconName]: ({ animate }) => JSX.Element;
} = {
  'arrow-top': ArrowTop,
  'arrow-bottom': ArrowBottom,
  'arrow-left': ArrowLeft,
  'arrow-right': ArrowRight,
  baby: Baby,
  backward: Backward,
  ball: Ball,
  burger: Burger,
  'calendar-cycle': CalendarCycle,
  'calendar-daily': CalendarDaily,
  'calendar-day': CalendarDay,
  'calendar-monthly': CalendarMonthly,
  caret: Caret,
  'check': Check,
  'chevron-bottom': ChevronBottom,
  'chevron-left': ChevronLeft,
  'chevron-right': ChevronRight,
  'chevron-top': ChevronTop,
  'cog': Cog,
  'cross': Cross,
  'enter': Enter,
  'error': IconError,
  'exit': Exit,
  'eye-none': EyeNone,
  'eye': Eye,
  'forward': Forward,
  'pen': Pen,
  'plug': Plug,
  'sync': Sync,
  'user-add': UserAdd,
  'user-unknown': UserUnknown,
  'user': User,
};

type Props = {
  name: IconName;
  size?: Spacing | 'auto';
  animate?: boolean;
  visible?: boolean;
};
export function Icon({ name, size = 'auto', animate = false, visible = true }: Props) {
  const Component = icons[name];

  return (
    <div className={classNames(['s-icon', `s-icon--${size}`, (!visible) ? 'hideMe' : ''])}>
      <Component animate={animate} />
    </div>
  );
}