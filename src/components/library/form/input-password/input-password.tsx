import React, { useState } from 'react';

import { Icon } from '../../icon/icon';
import { Label } from '../../text/label/label';
import { Children } from '../../../../types/children';
import { uuid } from '../../../../utils/uuid';
import './input-password.css';

type Props = {
  id?: string;
  disabled?: boolean;
  value: string;
  label: Children;
  onChange: (value: string) => void;
};
export function InputPassword({ id, label, disabled, value, onChange }: Props) {
  const [_id] = useState(id || uuid());
  const [show, setShow] = useState(false);
  const [currentValue, setCurrentValue] = useState(value);
  const togglePassword = () => setShow(!show);
  const onValueChange = (v: string) => {
    setCurrentValue(v);
    onChange(v);
  };

  return (
    <span className="s-input-password">
      <input className="s-input-password__field" type={show ? 'text' : 'password'} id={_id} disabled={disabled} value={currentValue} onChange={(e) => onValueChange(e.target.value)}/>
      <span className="s-input-password__icon" onClick={togglePassword}>
        <Icon name={show ? 'eye-none' : 'eye'}></Icon>
      </span>
      <Label htmlFor={_id} className="s-input-password__label">
        {label}
      </Label>
    </span>
  );
}