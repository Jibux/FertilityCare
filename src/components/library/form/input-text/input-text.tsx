import React, { useState } from 'react';

import { Label } from '../../text/label/label';
import { Children } from '../../../../types/children';
import { uuid } from '../../../../utils/uuid';
import './input-text.css';

type Props = {
  id?: string;
  disabled?: boolean;
  value: string;
  label: Children;
  type?: string;
  onChange?: (value: string) => void;
};
export function InputText({ id, label, type = 'text', disabled, value, onChange }: Props) {
  const [_id] = useState(id || uuid());
  const onValueChange = (v: string) => {
    onChange(v);
  };

  return (
    <span className="s-input-text">
      <input className="s-input-text__field" type={type} id={_id} disabled={disabled} value={value} onChange={(e) => onValueChange(e.target.value)} />
      <Label htmlFor={_id} className="s-input-text__label">
        {label}
      </Label>
    </span>
  );
}