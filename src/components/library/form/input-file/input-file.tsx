import { Label } from 'components/library/text/label/label';
import React, { useState } from 'react';
import { Children } from 'types/children';

import { uuid } from '../../../../utils/uuid';

type Props = {
  id?: string;
  disabled?: boolean;
  name?: string;
  accept?: string;
  label?: Children;
  onChange: (value: FileList) => void;
};
export function InputFile({ id, disabled, label, name, accept, onChange }: Props) {
  const [_id] = useState(id || uuid());
  const onValueChange = (v: FileList) => {
    onChange(v);
  };

  return (
    <span className="s-input-text">
      <input className="s-input-text__field" type="file" id={_id} accept={accept} name={name} disabled={disabled} onChange={(e) => onValueChange(e.target.files)}/>
      <Label htmlFor={_id} className="s-input-text__label">
        {label}
      </Label>
    </span>
  );
}