import React, { useState } from 'react';

import { uuid } from '../../../../utils/uuid';
import { Icon } from '../../icon/icon';
import { StickerColor, stickerHasBaby } from '../../../../entities/color';
import { classNames } from '../../../../utils/classnames';
import './radio-sticker.css';

type Props<T> = {
  id?: string;
  checked?: boolean;
  disabled?: boolean;
  name?: string;
  value: T;
  current?: T;
  type: StickerColor;
  onChange?: (value: T) => void;
};

export function RadioSticker<T>({ id, disabled = false, current, onChange, name, value, type }: Props<T>) {
  const [_id] = useState(id || uuid());
  const hasIcon = stickerHasBaby(type);

  return (
    <label htmlFor={_id} className={classNames(['s-radio-sticker', `s-radio-sticker--${type}`])}>
      <input
        type="radio"
        id={_id}
        disabled={disabled}
        name={name}
        value={_id}
        checked={current === value}
        onChange={() => onChange(value)}
      />
      <div className="s-radio-sticker__thumb">
        {/* hasIcon && <div className="s-radio-sticker__baby"><Icon name="baby" /></div> */}
        <div className="s-radio-sticker__icon">
          {hasIcon && <Icon name='baby' />}
        </div>
      </div>
    </label>
  );
}