import { isDefined } from '../../../../utils/utils';
import React, { useState } from 'react';

import { uuid } from '../../../../utils/uuid';
import { Icon } from '../../icon/icon';
import './toggle.css';
import { Children } from 'types/children';
import { Label } from 'components/library/text/label/label';

type Props<T> = {
  id?: string;
  disabled?: boolean;
  name?: string;
  checked?: boolean;
  value?: T;
  children: Children;
  checkedFunction?: () => boolean;
  onChange?: (checked: boolean, value: T) => void;
};
export function Toggle<T>({ id, disabled = false, checked = false, checkedFunction = null, onChange = null, name, children, value }: Props<T>) {
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  const isChecked = (defaultValue = isCheckedState) => (isDefined(checkedFunction)) ? checkedFunction() : defaultValue;
  const [_id] = useState(id || uuid());
  const [isCheckedState, changeIsCheckedState] = useState(isChecked(checked));

  const onValueChange = (v: T) => {
    return (isDefined(onChange)) ? onChange(isChecked(), v) : false;
  };

  const toggle = () => (!isDefined(checkedFunction)) ? changeIsCheckedState(!isChecked()) : false;

  return (
    <Label htmlFor={_id} className="s-toggle-container">
      <div className="s-toggle">
        <input
          type="checkbox"
          id={_id}
          disabled={disabled}
          name={name}
          value={_id}
          onChange={() => onValueChange(value)}
          checked={isChecked()}
          onClick={() => toggle()}
        />
        <div className="s-toggle__slide"></div>
        <div className="s-toggle__thumb">
          {isChecked()
            ? <Icon name="check" />
            : <></>
          }
        </div>
      </div>
      {children}
    </Label>
  );
}