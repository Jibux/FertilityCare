import React, { useContext, useEffect, useState } from 'react';

import { Container } from '../../layout/container/container';
import { Columns } from '../../layout/columns/columns';
import { ColumnFlexible } from '../../layout/columns/column-flexible/column-flexible';
import { ColumnRigid } from '../../layout/columns/column-rigid/column-rigid';
import { ButtonGhost } from '../library/buttons/button-ghost';
import { Dropdown } from '../library/dropdown/dropdown';
import { Text2 } from '../library/text/text/text';
import { Icon, IconName } from '../library/icon/icon';
import { Translate } from '../translate/translate';
import { fold } from '../../utils/union-type-helper';
import { ViewType, VIEW_TYPE } from '../../entities/view-type';
import { getPeriodParameters } from '../../i18n/dates';
import { useQueryParams } from '../../hooks/useQueryParams';
import { QueryParams } from '../../entities/query-params';
import { Translation } from '../../types/translations';
import { Popin, POPIN } from '../../entities/pop-in';
import { Menu, MENU } from '../../entities/menu';
import { SYNC_WORKING_STATUS } from '../../entities/status';
import { always, cond, equals, T } from 'ramda';
import { Inline } from 'layout/inline/inline';
import { getLanguage, setState } from 'app-state/app-actions';
import { appStore } from 'app-state/app-state';
import { SYNC_METHOD } from 'entities/sync/sync';


const MenuItem = ({ key, icon, text, animate = false }: { key: string, icon: IconName, text: Translation, animate?: boolean }) => {
  return {
    [key]: (
      <Columns spacing="XS">
        <ColumnRigid><Icon size="L" name={icon} animate={animate} /></ColumnRigid>
        <ColumnFlexible>
          <Text2><Translate name={text} /></Text2>
        </ColumnFlexible>
      </Columns>
    ),
  };
};

type Props = {
  currentDateView: Date;
};

export function Header({ currentDateView }: Props) {
  const [{ view: viewType = VIEW_TYPE.DEFAULT, popin, ...others }, setQueryParams] = useQueryParams<QueryParams>();
  const appState = useContext(appStore);
  const state = appState.state;
  const [syncIcon, setSyncIcon] = useState<IconName>(null);

  const changeView = (view: ViewType) => {
    setQueryParams({ view, popin, ...others });
  };

  const changePopin = (pop: Popin) => {
    setQueryParams({ view: viewType, popin: pop, ...others });
  };

  const viewTypeMenuItem = (item: { key: ViewType, icon: IconName, text: Translation }) => {
    return item.key === viewType
      ? {}
      : MenuItem(item);
  };

  const viewMenu = {
    //...viewTypeMenuItem({ key: 'daily', icon: 'calendar-daily', text: 'header.viewType.daily' } as const),
    ...viewTypeMenuItem({ key: 'monthly', icon: 'calendar-monthly', text: 'header.viewType.monthly' } as const),
    ...viewTypeMenuItem({ key: 'cycle', icon: 'calendar-cycle', text: 'header.viewType.cycle' } as const),
  };

  const actionMenu = (action: Menu) => {
    switch (action) {
      case MENU.SETTINGS: changePopin(POPIN.SETTINGS); break;
      case MENU.CYCLES: changePopin(POPIN.CYCLES); break;
      case MENU.CONNECT: changePopin(POPIN.CONNECT); break;
      case MENU.LOGOUT: changePopin(POPIN.LOGOUT); break;
      case MENU.SYNC: setState(appState, { syncMethod: SYNC_METHOD.FULL_SYNC, syncStatus: SYNC_WORKING_STATUS.TODO }); break;
      default: console.error(`Unnkown action '${action}'`);
    }
  };

  const getSyncMenuItemText = (): Translation =>
    cond([
      [equals(SYNC_WORKING_STATUS.WORKING), () => 'header.profile.synchronize.working' as Translation],
      [T, always('header.profile.synchronize' as Translation)],
    ])(appState.state.syncStatus);

  const getSyncMenuItemAnimate = (): boolean =>
    appState.state.syncStatus !== SYNC_WORKING_STATUS.WORKING ? false : true;

  const profilMenu = state.login.status
    ? {
      ...MenuItem({ key: MENU.SETTINGS, icon: 'cog', text: 'header.profile.settings' } as const),
      ...MenuItem({ key: MENU.CYCLES, icon: 'calendar-cycle', text: 'header.profile.cycles' } as const),
      ...MenuItem({ key: MENU.SYNC, icon: 'sync', text: getSyncMenuItemText(), animate: getSyncMenuItemAnimate() } as const),
      ...MenuItem({ key: MENU.LOGOUT, icon: 'exit', text: 'header.profile.logout' } as const),
    }
    : {
      ...MenuItem({ key: MENU.SETTINGS, icon: 'cog', text: 'header.profile.settings' } as const),
      ...MenuItem({ key: MENU.CYCLES, icon: 'calendar-cycle', text: 'header.profile.cycles' } as const),
      ...MenuItem({ key: MENU.CONNECT, icon: 'enter', text: 'header.profile.login' } as const),
    };

  const formatDate = (date: Date) => {
    return getPeriodParameters(viewType)(date, getLanguage(appState));
  };

  const icon = fold<ViewType, IconName>({
    cycle: () => 'calendar-cycle',
    //daily: () => 'calendar-daily',
    monthly: () => 'calendar-monthly',
  })(viewType);

  const viewTypeButtonTranslation = fold<ViewType, Translation>({
    cycle: () => 'header.viewTypeButton.cycle',
    //daily: () => "header.viewTypeButton.daily",
    monthly: () => 'header.viewTypeButton.monthly',
  })(viewType);

  const displaySync = () =>
    appState.state.syncStatus === SYNC_WORKING_STATUS.TODO ||
    appState.state.syncStatus === SYNC_WORKING_STATUS.WORKING ||
    appState.state.syncStatus === SYNC_WORKING_STATUS.OK ||
    appState.state.syncStatus === SYNC_WORKING_STATUS.KO;

  useEffect(() => {
    setSyncIcon(
      cond([
        [equals(SYNC_WORKING_STATUS.WORKING), () => 'sync'],
        [equals(SYNC_WORKING_STATUS.TODO), () => 'sync'],
        [equals(SYNC_WORKING_STATUS.OK), () => 'check'],
        [equals(SYNC_WORKING_STATUS.KO), () => 'error'],
        [equals(SYNC_WORKING_STATUS.UNKNOWN), () => syncIcon],
        [T, always(null)],
      ])(appState.state.syncStatus));
  }, [appState.state.syncStatus]);

  return (
    <Container paddingX="M" paddingY="S" color="GRADIENT" zIndex="header">
      <Columns align="CENTER">
        <ColumnFlexible>
          <Dropdown dropDownContent={viewMenu} onClick={changeView} position="left">
            <ButtonGhost onClick={() => false} iconLeft={icon} iconRight="caret">
              <Translate name={viewTypeButtonTranslation} parameters={formatDate(currentDateView)} />
            </ButtonGhost>
          </Dropdown>
        </ColumnFlexible>
        <ColumnRigid>
          <Inline spacing="S">
            {
              syncIcon !== null &&
              <Icon size="L" name={syncIcon}
                animate={getSyncMenuItemAnimate()}
                visible={displaySync()}
              />
            }
            <Dropdown dropDownContent={profilMenu} position="right" onClick={(key: Menu) => actionMenu(key)}>
              <Icon name="burger" size="XL" />
            </Dropdown>
          </Inline>
        </ColumnRigid>
      </Columns>
    </Container>
  );
}
