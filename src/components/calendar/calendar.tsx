import React, { useState, useRef, useEffect, useContext } from 'react';
import { useWindowSize, useScroll } from 'react-use';

import { Stack } from '../../layout/stack/stack';
import { Columns } from '../../layout/columns/columns';
import { Container } from '../../layout/container/container';
import { ColumnRigid } from '../../layout/columns/column-rigid/column-rigid';
import { Inline } from '../../layout/inline/inline';
import { getWeekDays } from '../../i18n/dates';
import { addDays, isBeforeToday, offsetMonth, getFirstDayOfMonth, getMonthCalendarFirstDay, WEEKS_PER_MONTH, DAYS_PER_WEEK, getMonthCalendarLastDay, isToday, getLastDayOfMonth, isDateBefore } from '../../utils/date';
import { Children } from '../../types/children';
import { appStore } from 'app-state/app-state';
import { getFocusedDate, getLanguage, setFocusedDate } from 'app-state/app-actions';
import { FirstDaysOfWeek } from 'entities/locales';

type Props = {
  focusedDate?: Date;
  dayRenderer: (date: Date) => Children;
  onClick?: (date: Date) => void;
};

const scrollToRef = (ref) => {
  if (ref && ref.current) {
    ref.current.parentElement.scrollTo(ref.current.offsetLeft, 0);
  }
};

type SlideProps = {
  focusedDate?: Date;
  dayRenderer: Children;
  refProp?: any;
  onClick?: (date: Date) => void;
};

function CalendarSlide({ focusedDate, refProp, dayRenderer, onClick }: SlideProps) {
  const appState = useContext(appStore);
  const state = appState.state;
  const firstDay = getMonthCalendarFirstDay(state.config.firstDayOfWeek)(focusedDate);
  const days = new Array(WEEKS_PER_MONTH * DAYS_PER_WEEK).fill(undefined).map((_, i) => addDays(i, firstDay));

  return (
    <div style={{
      position: 'relative',
      width: '100%',
      height: '100%',
      flex: 'none',
      scrollSnapAlign: 'center',
    }}
      ref={refProp}
    >
      <Inline height="100%" columns={7} align="STRETCH" justify="SPACE_AROUND">
        {
          days.map((day) => (
            <ColumnRigid height="20%" key={day.valueOf()} contentFit>
              <button
                type="button"
                onClick={() => isBeforeToday(day) ? onClick(day) : false}
                style={{
                  color: 'var(--color-black)',
                  height: '100%',
                  width: '100%',
                  overflow: 'hidden',
                  boxSizing: 'border-box',
                  border: '0.5px solid',
                  fontSize: '0.5rem',
                  display: 'flex',
                  textAlign: 'start',
                  padding: '0.125rem 0.25rem',
                  background: isToday(day)
                    ? 'var(--color-primary-light)'
                    : isBeforeToday(day)
                      ? 'var(--color-grey-light)'
                      : 'var(--color-white)',
                }}
              >
                <div style={{ width: '100%', height: '100%' }}>{dayRenderer(day)}</div>
              </button>
            </ColumnRigid>
          ))
        }
      </Inline>
    </div>
  );
}

const displayLastSlide = (firstDayOfWeek: FirstDaysOfWeek, day: Date) => isDateBefore(getMonthCalendarLastDay(firstDayOfWeek)(day), getLastDayOfMonth());

export function Calendar({ focusedDate, dayRenderer, onClick }: Props) {
  const appState = useContext(appStore);
  const [day, setDay] = useState(getFirstDayOfMonth(focusedDate || getFocusedDate(appState)));
  const ref = useRef(null);
  const scrollRef = useRef(null);
  const { x } = useScroll(ref);
  const { width } = useWindowSize();
  const weekdays = getWeekDays(appState.state.config.firstDayOfWeek)('short', getLanguage(appState));

  const loadDate = (date: Date) => {
    setDay(date);
    setFocusedDate(appState, date);
    scrollToRef(scrollRef);
  };

  useEffect(() => {
    if (ref) {
      const w = ref.current.offsetWidth;
      if (x < 10) {
        loadDate(offsetMonth(day, -1));
      } else if (x + 10 > w * 2) {
        loadDate(offsetMonth(day, 1));
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ref, x, day]);

  useEffect(() => {
    scrollToRef(scrollRef);
  }, [width]);

  useEffect(() => {
    loadDate(day);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [day]);

  return (
    <Stack height="100%">
      <Container color="SECONDARY" paddingY="XS" paddingX="S">
        <Columns justify="SPACE_AROUND">
          {
            weekdays.map(
              d => <ColumnRigid key={d}>{d}</ColumnRigid>,
            )
          }
        </Columns>
      </Container>
      <div
        style={{
          position: 'relative',
          height: '100%',
          display: 'flex',
          overflowX: 'scroll',
          scrollSnapType: 'x mandatory',
        }}
        ref={ref}
      >
        <CalendarSlide focusedDate={offsetMonth(day, -1)} dayRenderer={dayRenderer} onClick={onClick}></CalendarSlide>
        <CalendarSlide refProp={scrollRef} focusedDate={day} dayRenderer={dayRenderer} onClick={onClick}></CalendarSlide>
        {displayLastSlide(appState.state.config.firstDayOfWeek, day) && <CalendarSlide focusedDate={offsetMonth(day, 1)} dayRenderer={dayRenderer} onClick={onClick}></CalendarSlide>}
      </div>
    </Stack>
  );
}