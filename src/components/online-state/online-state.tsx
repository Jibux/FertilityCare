import React, { useContext } from 'react';

import { appStore } from 'app-state/app-state';
import { updateOnLineStatus } from 'app-state/app-actions';


export function OnLineState() {
  const appState = useContext(appStore);

  const handleOnLineStatus = () => {
    updateOnLineStatus(appState);
  };

  window.addEventListener('online', handleOnLineStatus);
  window.addEventListener('offline', handleOnLineStatus);

  return (
    <>
    </>
  );
}
