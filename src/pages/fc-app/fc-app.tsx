import React, { useContext } from 'react';

import { ViewSwitch } from './view-switch/view-switch';

import './fc-app.css';
import { Modals } from './modals/modals';
import { Sync } from '../../components/sync/sync';
import { Header } from 'components/header/header';
import { Stack } from 'layout/stack/stack';
import { getFocusedDate } from 'app-state/app-actions';
import { appStore } from 'app-state/app-state';
import { OnLineState } from 'components/online-state/online-state';

export function FCApp() {
  const appState = useContext(appStore);

  return (
    <Stack height="100%" spacing="NONE" overflowY="hidden">
      <Header currentDateView={getFocusedDate(appState)}></Header>
      <Sync />
      <OnLineState />
      <ViewSwitch />
      <Modals />
    </Stack>
  );
}