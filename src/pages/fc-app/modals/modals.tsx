import React from 'react';
import { LogoutModal } from './logout';
import { SettingsModal } from './settings';
import { POPIN } from '../../../entities/pop-in';
import { DataModal } from './manage-data';
import { ConnectModal } from './connect';
import { DBXModal } from './dropbox';
import { EditDayModal } from './edit-day';
import { WDModal } from './webdav';
import { getDateFromString } from 'utils/date';
import { CyclesModal } from './cycles';
import { useQueryParams } from 'hooks/useQueryParams';
import { QueryParams } from 'entities/query-params';

export const Modals = () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [{ popin, code, date, ...others }, setQueryParams] = useQueryParams<QueryParams>();

  const resetPopin = () => setQueryParams({ popin: POPIN.NONE, code: '', date: '', ...others });

  return (
    <>
      {
        (popin === POPIN.EDIT_DAY) &&
        <EditDayModal date={getDateFromString(date)} onClose={resetPopin} />
      }
      {
        (popin === POPIN.DATA_MANAGE) &&
        <DataModal onClose={resetPopin} />
      }
      {
        (popin === POPIN.CYCLES) &&
        <CyclesModal onClose={resetPopin} />
      }
      {
        (popin === POPIN.CONNECT) &&
        <ConnectModal onClose={resetPopin} />
      }
      {
        (popin === POPIN.LOGIN_DBX) &&
        <DBXModal onClose={resetPopin} />
      }
      {
        (popin === POPIN.LOGIN_WD) &&
        <WDModal onClose={resetPopin} />
      }
      {
        (popin === POPIN.LOGOUT) &&
        <LogoutModal onClose={resetPopin} />
      }
      {
        (popin === POPIN.SETTINGS) &&
        <SettingsModal onClose={resetPopin} />
      }
    </>);
};