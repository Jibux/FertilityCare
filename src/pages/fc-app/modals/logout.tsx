import React, { useContext } from 'react';

import { ModalSimple } from '../../../components/library/modal-simple/modal-simple';
import { Translate } from '../../../components/translate/translate';
import { ButtonPrimary } from '../../../components/library/buttons/button-primary';
import { isOnLine } from '../../../utils/utils';
import { logout } from '../../../entities/sync/sync';
import { sentenceCase } from '../../../utils/string';
import { ColumnRigid } from '../../../layout/columns/column-rigid/column-rigid';
import { Columns } from '../../../layout/columns/columns';
import { ButtonGhost } from 'components/library/buttons/button-ghost';
import { appStore } from 'app-state/app-state';
import { resetLoginData } from 'app-state/app-actions';


export const LogoutModal = ({
  onClose,
}: {
  onClose: () => void;
}) => {
  const appState = useContext(appStore);
  const state = appState.state;

  const redirect = () => {
    onClose();
  };

  const logoutAction = () => {
    console.log('logout');
    logout(state.login)
      .then(() => {
        // console.debug(response);
      })
      .catch((error) => console.error(error))
      .finally(() => {
        if (isOnLine()) {
          resetLoginData(appState);
          redirect();
        }
      });
  };

  return (
    <ModalSimple message={<Translate name='logout.message' />} onClose={onClose}>
      <Columns spacing="S" justify="SPACE_AROUND">
        <ColumnRigid>
          <ButtonGhost onClick={() => redirect()}>
            <Translate name="form.button.cancel" filter={sentenceCase} />
          </ButtonGhost>
        </ColumnRigid>
        <ColumnRigid>&nbsp;</ColumnRigid>
        <ColumnRigid>
          <ButtonPrimary onClick={() => logoutAction()}>
            <Translate name="header.profile.logout" filter={sentenceCase} />
          </ButtonPrimary>
        </ColumnRigid>
      </Columns>
    </ModalSimple>
  );
};
