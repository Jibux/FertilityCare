import React, { useContext, useEffect, useState } from 'react';

import { Modal } from '../../../components/library/modal/modal';
import { Translate } from '../../../components/translate/translate';
import { exportToPDF } from '../../../entities/cycle/cycle-pdf';
import { sentenceCase } from '../../../utils/string';
import { Stack } from '../../../layout/stack/stack';
import { ButtonPrimary } from '../../../components/library/buttons/button-primary';
import { SubTitle } from 'components/library/text/sub-title/sub-title';
import { InputText } from 'components/library/form/input-text/input-text';
import { Text2 } from 'components/library/text/text/text';
import { getDateFromString, getRealFullDayString, getYesterday, today } from 'utils/date';
import { DEFAULT_NR_OF_CYCLES, getCycles, ICycle } from 'entities/cycle/cycle';
import { getLanguage } from 'app-state/app-actions';
import { appStore } from 'app-state/app-state';
import { filterAbsolutePositiveInteger, filterMax } from 'utils/utils';
import { getArrayElemFromEnd, isEmptyArray } from 'utils/array';
import { getDateString } from 'i18n/dates';
import { Label } from 'components/library/text/label/label';
import { Inline } from 'layout/inline/inline';
import { ButtonGhost } from 'components/library/buttons/button-ghost';
import { ButtonSecondary } from 'components/library/buttons/button-secondary';


export const CyclesModal = ({
  onClose,
}: {
  onClose: () => void;
}) => {
  const [cyclesStartDate, setCyclesStartDate] = useState('?');
  const [cyclesEndDate, setCyclesEndDate] = useState('?');
  const [dayBeforeFirstCycle, setDayBeforeFirstCycle] = useState(getRealFullDayString(today(), '-'));
  const [numberOfCycles, setNumberOfCycles] = useState(DEFAULT_NR_OF_CYCLES.toString());
  const [cyclesOffset, setCyclesOffset] = useState(DEFAULT_NR_OF_CYCLES);
  const [cycles, setCycles] = useState<ICycle[]>([]);
  const [cyclesToExport, setCyclesToExport] = useState<ICycle[]>([]);
  const appState = useContext(appStore);
  const state = appState.state;

  const exportPDF = () => {
    console.debug('Export to PDF');
    exportToPDF(cyclesToExport)(getLanguage(appState));
  };

  const changeNumberOfCycles = (n: string) => {
    const ni = filterAbsolutePositiveInteger(parseInt(n), 1);
    setNumberOfCycles(ni.toString());
  };

  const changeCyclesOffset = (n: number) => {
    const ni = filterMax(filterAbsolutePositiveInteger(n + cyclesOffset, 1), cycles.length);
    setCyclesOffset(ni);
  };

  const changeCyclesToExport = () => {
    const day = getDateFromString(dayBeforeFirstCycle, '-');
    const c = cycles.filter((cycle: ICycle) =>
      day < cycle.firstDay,
    ).slice(0, +numberOfCycles);
    setCyclesToExport(c);
  };

  const updateDayBeforeCycle = () => {
    if (!isEmptyArray(cycles)) {
      setDayBeforeFirstCycle(getRealFullDayString(getYesterday(getArrayElemFromEnd(cycles, +cyclesOffset).firstDay), '-'));
    }
  };

  useEffect(() => {
    setCycles(getCycles(state.observationalDays, 500));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!isEmptyArray(cyclesToExport)) {
      setCyclesStartDate(getDateString(cyclesToExport[0].firstDay, getLanguage(appState)));
      setCyclesEndDate(getDateString(cyclesToExport[cyclesToExport.length - 1].lastDay, getLanguage(appState)));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cyclesToExport]);

  useEffect(() => {
    updateDayBeforeCycle();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cycles, cyclesOffset]);

  useEffect(() => {
    changeCyclesToExport();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [numberOfCycles, dayBeforeFirstCycle, numberOfCycles]);

  return (
    <Modal title={<Translate name='cycles.title' />} onClose={onClose}>
      <Stack spacing="L">
        <Stack spacing="S">
          <SubTitle><Translate name="cycles.topdf.title" filter={sentenceCase} /></SubTitle>
          <Label><Translate name="cycles.topdf.fromdate" filter={sentenceCase} /></Label>
          <Text2>{cyclesStartDate}</Text2>
          <Label><Translate name="cycles.topdf.enddate" filter={sentenceCase} /></Label>
          <Text2>{cyclesEndDate}</Text2>
          <InputText onChange={setDayBeforeFirstCycle} value={dayBeforeFirstCycle} label={<Translate name="cycles.topdf.date_before_first_cycle" filter={sentenceCase} />} />
          <InputText type="number" onChange={changeNumberOfCycles} value={numberOfCycles} label={<Translate name="cycles.topdf.number" filter={sentenceCase} />} />
          <Label><Translate name="cycles.topdf.offset" filter={sentenceCase} /></Label>
          <Inline>
            <ButtonGhost onClick={() => changeCyclesOffset(1)}><Translate name="cycles.topdf.previous" filter={sentenceCase} /></ButtonGhost>
            &nbsp;
            <ButtonSecondary onClick={() => changeCyclesOffset(-1)}><Translate name="cycles.topdf.next" filter={sentenceCase} /></ButtonSecondary>
          </Inline>
        </Stack>
        <ButtonPrimary onClick={() => exportPDF()}>
          <Translate name="cycles.topdf" filter={sentenceCase} />
        </ButtonPrimary>
      </Stack>
    </Modal>
  );
};
