import React, { useContext, useEffect, useState } from 'react';
import { Stack } from '../../../layout/stack/stack';
import { Modal } from '../../../components/library/modal/modal';
import { FieldRadio } from '../../../components/library/form/field-radio/field-radio';
import { ButtonPrimary } from '../../../components/library/buttons/button-primary';
import { Columns } from '../../../layout/columns/columns';
import { Toggle } from '../../../components/library/form/toggle/toggle';
import { Inline } from '../../../layout/inline/inline';
import { ColumnRigid } from '../../../layout/columns/column-rigid/column-rigid';
import { Translate } from '../../../components/translate/translate';
import { RadioSticker } from '../../../components/library/form/radio-sticker/radio-sticker';
import { Text2 } from 'components/library/text/text/text';
import {
  KEYS_INFO,
  KEYS_DATA,
  Data,
  ODKey,
  ODValue,
} from '../../../entities/observational-data/observational-data';
import {
  addToObservationalDay,
  removeFromObservationalDay,
  doesObservationalDayContainData,
  IObservationalDay,
  isValueInObservationalDayData,
  getDataMucusDescription,
  getDataPeriodDescription,
  getDataFrequencyDescription,
  getDataMiscDescription,
  getDataSimilarityDescription,
  isAStickerDay,
  isACorrectedDay,
  setOldObservationalDay,
  getStickerRealColor,
  createObservationalDay,
  getComment,
  getDataPicDescription,
  getODVersion,
} from '../../../entities/objects/observational-day';
import { ButtonSecondary } from '../../../components/library/buttons/button-secondary';
import { SubTitle } from '../../../components/library/text/sub-title/sub-title';
import { getDateString } from '../../../i18n/dates';
import { sentenceCase } from '../../../utils/string';
import { getObservationalDay, saveObservationalDay, deleteObservationalDay, getLanguage } from 'app-state/app-actions';
import { appStore } from 'app-state/app-state';
import { isDefined } from 'utils/utils';
import { Translation } from 'types/translations';
import { doesObjectKeyContainData, isExtendable } from 'entities/object-helpers/observational-data';
import { Sticker } from 'components/library/sticker/sticker';
import { ButtonGhost } from 'components/library/buttons/button-ghost';
import { InputText } from 'components/library/form/input-text/input-text';
import { Icon } from 'components/library/icon/icon';
import { isEmptyArray } from 'utils/array';


export const EditDayModal = ({
  date,
  onClose,
}: {
  date: Date;
  onClose: () => void;
}) => {
  const appState = useContext(appStore);
  const [displayEdit, setDisplayEdit] = useState(false);
  const [keyToEdit, setKeyToEdit] = useState<ODKey | undefined>(undefined);
  const [loadedObservationalDay, setLoadedObservationalDay] = useState<IObservationalDay>(getObservationalDay(appState, date));
  const [observationalDayToEdit, setObservationalDayToEdit] = useState<IObservationalDay>(loadedObservationalDay);
  const [viewOldObservationalDay, setViewOldObservationalDay] = useState(false);
  const [switchViewButtonName, setSwitchViewButtonName] = useState<Translation>('form.button.view.old');

  const resetKeyToEdit = () => setKeyToEdit(undefined);

  const editTheDay = () => !doesObservationalDayContainData(loadedObservationalDay) || displayEdit;

  const correctionModeEnabled = () => appState.state.config.correctionMode;

  const displayCorrectedButton = () => !isACorrectedDay(observationalDayToEdit) && correctionModeEnabled();

  const correctDay = () => {
    setObservationalDayToEdit(setOldObservationalDay(observationalDayToEdit));
    setDisplayEdit(true);
  };

  const switchObservationalDayView = () => {
    setViewOldObservationalDay(!viewOldObservationalDay);
  };

  useEffect(() => {
    if (viewOldObservationalDay) {
      setLoadedObservationalDay(loadedObservationalDay.oldObservationalDay);
      setSwitchViewButtonName('form.button.view.corrected');
    } else {
      setLoadedObservationalDay(observationalDayToEdit);
      setSwitchViewButtonName('form.button.view.old');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [viewOldObservationalDay]);

  const removeFromObservationalDayToEdit = (key: ODKey) => (value: number = undefined) => {
    setObservationalDayToEdit(
      removeFromObservationalDay(key)(value)(observationalDayToEdit),
    );
    if (!isDefined(value)) {
      resetKeyToEdit();
    }
  };

  const updateObservationalDayToEdit = (key: ODKey, value: ODValue) => {
    setObservationalDayToEdit(
      addToObservationalDay(key, value, observationalDayToEdit),
    );
  };

  const toggleData = (key: ODKey, checked: boolean, value: number) => {
    if (!checked) {
      updateObservationalDayToEdit(key, value);
    } else {
      removeFromObservationalDayToEdit(key)(value);
    }
  };

  const saveEditDay = () => {
    // console.debug(observationalDayToEdit);
    if (doesObservationalDayContainData(observationalDayToEdit)) {
      saveObservationalDay(appState, date, observationalDayToEdit);
    } else {
      deleteObservationalDay(appState, date);
    }
    onClose();
  };

  const resetEditDay = () => {
    setObservationalDayToEdit(
      createObservationalDay(),
    );
  };

  const radio = (key: ODKey) => (
    <>
      {KEYS_DATA[key].map((value: Data, index: number) => (
        <FieldRadio
          value={index}
          key={`${key}_${index}`}
          checked={isValueInObservationalDayData(key, index, observationalDayToEdit)}
          name={`radio_${key}`}
          onChange={(v) => updateObservationalDayToEdit(key, v)}
        >
          {isDefined(value.description) ? <Translate name={value.description} filter={sentenceCase} /> : value.acronym}
        </FieldRadio>
      ))}
    </>
  );

  const radioView = (key: ODKey) => (
    <Stack spacing="XS">
      <SubTitle>
        {<Translate name={KEYS_INFO[key].name} filter={sentenceCase} />}
      </SubTitle>
      {radio(key)}
    </Stack>
  );

  const toggleView = (key: ODKey) => (
    <Stack spacing="XS">
      <SubTitle>
        {<Translate name={KEYS_INFO[key].name} filter={sentenceCase} />}
      </SubTitle>
      {KEYS_DATA[key].map((value: Data, index: number) => (
        <Toggle
          value={index}
          checkedFunction={() => isValueInObservationalDayData(key, index, observationalDayToEdit)}
          id={`cb_${key}_${index}`}
          key={`cb_${key}_${index}`}
          name={`cb_${key}`}
          onChange={(checked: boolean, v: number) =>
            toggleData(key, checked, v)
          }
        >
          {<Translate name={value.description} filter={sentenceCase} />}
        </Toggle>
      ))}
    </Stack>
  );

  const editSticker = () =>
    <Stack spacing="S">
      <SubTitle>
        {
          <Translate
            name={KEYS_INFO.sticker.name}
            filter={sentenceCase}
          />
        }
      </SubTitle>
      <Inline spacingX="M" align="CENTER">
        &nbsp;&nbsp;&nbsp;
        {KEYS_DATA.sticker.map((color, index) => (
          <RadioSticker
            current={observationalDayToEdit.sticker}
            type={color}
            value={index}
            key={`sticker_${index}`}
            name="radio_sticker"
            onChange={(value) =>
              updateObservationalDayToEdit('sticker', value)
            }
          />
        ))}
      </Inline>
    </Stack>;

  const editPeriod = () =>
    radioView('period');

  const editMucus = () =>
    radioView('mucus');

  const editMucusExtended = () =>
    toggleView('mucusExtended');

  const editFrequency = () =>
    radioView('frequency');

  const editSimilarity = () =>
    radioView('similarity');

  const editMisc = () =>
    toggleView('misc');

  const editPic = () =>
    radioView('pic');

  const editComment = () =>
    <InputText
      value={getComment(observationalDayToEdit)}
      onChange={(value: string) => updateObservationalDayToEdit('comment', value)}
      label={<Translate name={KEYS_INFO.comment.name} filter={sentenceCase} />}
    />;

  type EditKey = {
    [key in Exclude<'sticker', ODKey>]: () => void;
  };

  const editKey: EditKey = {
    period: editPeriod,
    mucus: editMucus,
    mucusExtended: editMucusExtended,
    frequency: editFrequency,
    similarity: editSimilarity,
    misc: editMisc,
    pic: editPic,
    comment: editComment,
  };

  const getNextKeyToEdit = () =>
    (keyToEdit === 'mucus')
      ? (isExtendable(['mucus', observationalDayToEdit.mucus]))
        ? 'mucusExtended'
        : 'frequency'
      : (keyToEdit === 'mucusExtended')
        ? 'frequency'
        : undefined;

  const getButtonAction = () =>
    () => setKeyToEdit(getNextKeyToEdit());

  const getButtonLabel = () =>
    (keyToEdit === 'mucus' || keyToEdit === 'mucusExtended')
      ? 'form.button.next'
      : 'form.button.validate';

  const getCancelButtonLabel = (): Translation =>
    doesObjectKeyContainData(keyToEdit, observationalDayToEdit)
      ? 'form.button.remove'
      : 'form.button.cancel';

  const editData = () =>
    <Stack spacing="S">
      {editKey[keyToEdit]()}
      <Columns spacing="S" justify="SPACE_AROUND">
        <ColumnRigid>
          <ButtonGhost onClick={() => removeFromObservationalDayToEdit(keyToEdit)()}>
            {<Translate name={getCancelButtonLabel()} filter={sentenceCase} />}
          </ButtonGhost>
        </ColumnRigid>
        <ColumnRigid>
          <ButtonPrimary onClick={getButtonAction()}>
            <Translate name={getButtonLabel()} filter={sentenceCase} />
          </ButtonPrimary>
        </ColumnRigid>
      </Columns>
    </Stack>;

  const displaySticker = (obsDay: IObservationalDay) => (
    isAStickerDay(obsDay) &&
    <Stack spacing="XS">
      <SubTitle>
        {
          <Translate
            name={KEYS_INFO.sticker.name}
            filter={sentenceCase}
          />
        }
      </SubTitle>
      <Sticker type={getStickerRealColor(obsDay)} />
    </Stack>
  );

  const displayDataTitle = (key: ODKey) => (
    <SubTitle>
      {editTheDay() && <><ButtonGhost onClick={() => setKeyToEdit(key)} key={key} ><Icon name="pen" /></ButtonGhost>&nbsp;&nbsp;&nbsp;</>}
      {
        <Translate
          name={KEYS_INFO[key].name}
          filter={sentenceCase}
        />
      }
    </SubTitle>
  );

  const displayData = (key: ODKey, data: Translation[]) => (
    <>
      {(editTheDay() || !isEmptyArray(data)) &&
        <Stack spacing="XS">
          {displayDataTitle(key)}
          <Text2>
            {data.map((string: Translation, index) => (
              <span key={`${key}_${index}`}>
                {index !== 0 && <>,&nbsp;</>}
                <Translate name={string} filter={index === 0 ? sentenceCase : (v) => v} />
              </span>
            ))}
          </Text2>
        </Stack>
      }
    </>
  );

  const displayComment = (comment: string) => (
    <>
      {(editTheDay() || comment !== '') &&
        <Stack spacing="XS">
          {displayDataTitle('comment')}
          <Text2>
            {comment}
          </Text2>
        </Stack>
      }
    </>
  );

  const displayODVersion = (version) => (
    <>
      <Stack spacing="XS">
        <Text2>
          v{version}
        </Text2>
      </Stack>
    </>
  );

  const displayDayData = (obsDay: IObservationalDay) => (
    <>
      {correctionModeEnabled() && <Text2><Icon name="error" />&nbsp;<Translate name="settings.correctionmode.warning" /></Text2>}
      {
        editTheDay() ? editSticker() : displaySticker(obsDay)
      }
      <Stack spacing="S">
        {
          displayData('period', getDataPeriodDescription(obsDay))
        }
        {
          displayData('mucus', getDataMucusDescription(obsDay))
        }
        {
          displayData('frequency', getDataFrequencyDescription(obsDay))
        }
        {
          displayData('misc', getDataMiscDescription(obsDay))
        }
        {
          displayData('pic', getDataPicDescription(obsDay))
        }
        {
          displayData('similarity', getDataSimilarityDescription(obsDay))
        }
        {
          displayComment(obsDay.comment || '')
        }
        {
          appState.state.config.devMode && displayODVersion(getODVersion(obsDay))
        }
      </Stack>
      {correctionModeEnabled() && <Text2><Icon name="error" />&nbsp;<Translate name="settings.correctionmode.warning" /></Text2>}
    </>
  );

  const editDayFirstScreen = () => {
    return <Stack spacing="M">
      {displayDayData(observationalDayToEdit)}
      <Columns spacing="S" justify="SPACE_AROUND">
        <ColumnRigid>
          <ButtonSecondary onClick={resetEditDay} disabled={!doesObservationalDayContainData(observationalDayToEdit)}>
            <Translate name="form.button.reset" filter={sentenceCase} />
          </ButtonSecondary>
        </ColumnRigid>
        <ColumnRigid>
          <ButtonPrimary onClick={saveEditDay}>
            <Translate name="form.button.validate" filter={sentenceCase} />
          </ButtonPrimary>
        </ColumnRigid>
      </Columns>
    </Stack>;
  };

  const editDay = () =>
    isDefined(keyToEdit)
      ? editData()
      : editDayFirstScreen();

  const displayDay = () => (
    <Stack spacing="M">
      {displayDayData(loadedObservationalDay)}
      <Columns spacing="S" justify="SPACE_AROUND">
        {isACorrectedDay(observationalDayToEdit) &&
          <ColumnRigid>
            <ButtonGhost onClick={switchObservationalDayView}>
              <Translate name={switchViewButtonName} filter={sentenceCase} />
            </ButtonGhost>
          </ColumnRigid>
        }
        {!viewOldObservationalDay && (
          displayCorrectedButton()
            ?
            <ColumnRigid>
              <ButtonSecondary onClick={correctDay}>
                <Translate name="form.button.correct" filter={sentenceCase} />
              </ButtonSecondary>
            </ColumnRigid>
            :
            <ColumnRigid>
              <ButtonPrimary onClick={() => setDisplayEdit(true)}>
                <Translate name="form.button.edit" filter={sentenceCase} />
              </ButtonPrimary>
            </ColumnRigid>
        )}
      </Columns>
    </Stack>
  );

  return (
    <Modal title={getDateString(date, getLanguage(appState))} onClose={onClose}>
      {
        editTheDay()
          ? editDay()
          : displayDay()
      }
    </Modal>
  );
};
