import React, { useContext, useEffect, useState } from 'react';

import { Modal } from '../../../components/library/modal/modal';
import { Translate } from '../../../components/translate/translate';
import { dbxGetExpiresIn, dbxListFiles, dbxRequestRefreshToken } from '../../../entities/sync/dropbox';
import { useQueryParams } from '../../../hooks/useQueryParams';
import { IHashKeyString, isDefined } from '../../../utils/utils';
import { SYNC_PROVIDER } from '../../../entities/sync/sync';
import { LOGIN_STATUS, LoginStatus } from '../../../entities/status';
import { SubTitle } from '../../../components/library/text/sub-title/sub-title';
import { appStore } from 'app-state/app-state';
import { resetLoginData, setLoginData } from 'app-state/app-actions';

const REDIRECT_TIMEOUT = {
  OK: 2000,
  KO: 5000,
} as const;

type RedirectTimeout = ValueOf<typeof REDIRECT_TIMEOUT>;


export const DBXModal = ({
  onClose,
}: {
  onClose: () => void;
}) => {
  const [{ code, error }] = useQueryParams();
  const [redirectTimeout, setRedirectTimeout] = useState<RedirectTimeout>(REDIRECT_TIMEOUT.OK);
  const [loginStatus, setLoginStatus] = useState<LoginStatus>(LOGIN_STATUS.ONGOING);
  const appState = useContext(appStore);

  const redirect = () => {
    console.debug('Closing dropbox modal');
    setTimeout(() => {
      onClose();
    }, redirectTimeout);
  };

  const testDBXaccess = (data: IHashKeyString) => {
    dbxListFiles({ accessToken: data.access_token, tokenType: data.token_type })
      .then(() => {
        // console.debug(response);
        setLoginData(appState, {
          provider: SYNC_PROVIDER.DBX, data: {
            accessToken: data.access_token,
            refreshToken: data.refresh_token,
            expiresIn: dbxGetExpiresIn(data.expires_in),
            tokenType: data.token_type,
            accountId: data.account_id,
          },
        });
        setLoginStatus(LOGIN_STATUS.OK);
      })
      .catch((err) => {
        setLoginStatus(LOGIN_STATUS.KO);
        setRedirectTimeout(REDIRECT_TIMEOUT.KO);
        resetLoginData(appState);
        console.error('Error with Dropbox login');
        console.error(err);
      });
  };

  const requestDBXRefreshToken = () => {
    dbxRequestRefreshToken(code)
      .then((data) => {
        testDBXaccess(data);
      })
      .catch((err) => console.error(err));
  };

  const loginMessage = () =>
    loginStatus === LOGIN_STATUS.KO
      ? <SubTitle><Translate name={'connect.dbx.ko'} /></SubTitle>
      : loginStatus === LOGIN_STATUS.OK
        ? <SubTitle><Translate name={'connect.dbx.ok'} /></SubTitle>
        : <SubTitle><Translate name={'connect.dbx.ongoing'} /></SubTitle>;

  /*useEffect(() => {
    (isDefined(access_token) && isDefined(token_type) && isDefined(account_id))
      ? testDBXaccess()
      : console.debug('No access token requested');
      // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [access_token, account_id, token_type]);*/

  useEffect(() => {
    if (isDefined(code)) {
      requestDBXRefreshToken();
    } else {
      console.debug('No code requested');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [code]);

  useEffect(() => {
    if (isDefined(error)) {
      setLoginStatus(LOGIN_STATUS.KO);
      redirect();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);

  useEffect(() => {
    if (loginStatus !== LOGIN_STATUS.ONGOING) {
      redirect();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loginStatus]);

  return (
    <Modal title={<Translate name='connect.dbx' />} onClose={onClose}>
      {loginMessage()}
    </Modal>
  );
};
