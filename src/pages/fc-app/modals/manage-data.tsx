import React, { useContext, useState } from 'react';

import { Modal } from '../../../components/library/modal/modal';
import { Translate } from '../../../components/translate/translate';
import { downloadData, uploadData } from '../../../utils/files';
import { IObservationalDays } from '../../../entities/objects/observational-day';
import { ButtonPrimary } from '../../../components/library/buttons/button-primary';
import { sentenceCase } from '../../../utils/string';
import { InputFile } from '../../../components/library/form/input-file/input-file';
import { Stack } from '../../../layout/stack/stack';
import { Notification2 } from '../../../components/library/notification/notification';
import { FC_JSON_FILE } from '../../../entities/sync/config';
import { NotifStatus, NOTIF_STATUS } from '../../../entities/notification';
import { setState, setSyncStatus } from 'app-state/app-actions';
import { appStore } from 'app-state/app-state';
import { ButtonSecondary } from 'components/library/buttons/button-secondary';
import { DEFAULT_NR_OF_CYCLES_TO_GENERATE, generateData } from 'entities/data-generator/generate-data';
import { ButtonGhost } from 'components/library/buttons/button-ghost';
import { SubTitle } from 'components/library/text/sub-title/sub-title';
import { Columns } from 'layout/columns/columns';
import { ColumnRigid } from 'layout/columns/column-rigid/column-rigid';
import { InputText } from 'components/library/form/input-text/input-text';
import { POPIN } from 'entities/pop-in';
import { QueryParams } from 'entities/query-params';
import { useQueryParams } from 'hooks/useQueryParams';
import { Inline } from 'layout/inline/inline';
import { SYNC_METHOD } from 'entities/sync/sync';
import { Log } from 'utils/logs';
import { SYNC_WORKING_STATUS } from 'entities/status';


export const DataModal = ({
  onClose,
}: {
  onClose: () => void;
}) => {
  const appState = useContext(appStore);
  const state = appState.state;
  const [displayNotification, setDisplayNotification] = useState<NotifStatus>(NOTIF_STATUS.NONE);
  const [displayReset, setDisplayReset] = useState(false);
  const [displayRandomData, setDisplayRandomData] = useState(false);
  const [numberOfCycles, setNumberOfCycles] = useState(DEFAULT_NR_OF_CYCLES_TO_GENERATE.toString());
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [{ popin, ...others }, setQueryParams] = useQueryParams<QueryParams>();

  const onDiplayNotification = (status: NotifStatus) => {
    setDisplayNotification(status);
    setTimeout(() => {
      setDisplayNotification(NOTIF_STATUS.NONE);
    }, 3000);
  };

  const uploadTheData = (files: any) => {
    const file = files[0];
    uploadData(file)
      .then((jsonContent: IObservationalDays) => setState(appState, { syncMethod: SYNC_METHOD.PUSH, observationalDays: jsonContent }))
      .then(() => {
        onDiplayNotification(NOTIF_STATUS.OK);
      })
      .catch((error) => {
        onDiplayNotification(NOTIF_STATUS.KO);
        Log.error(error);
      });
  };

  const migrateData = () => {
    setSyncStatus(appState, SYNC_WORKING_STATUS.PRE_MIGRATE);
  };

  const resetData = () => {
    setState(appState, { syncMethod: SYNC_METHOD.PUSH, observationalDays: {} });
  };

  const generateObsDaysData = () => {
    setState(appState, { syncMethod: SYNC_METHOD.PUSH, observationalDays: generateData(parseInt(numberOfCycles)) });
  };

  const confirmFrom = (validate: Function, close: Function) =>
    <>
      <SubTitle><Translate name="datamanage.warning.destroy" /></SubTitle>
      {displayRandomData && validate === generateObsDaysData &&
        <InputText onChange={setNumberOfCycles} value={numberOfCycles} label={<Translate name="datamanage.cycle.number" filter={sentenceCase} />} />}
      <Columns spacing="S" justify="SPACE_AROUND">
        <ColumnRigid>
          <ButtonGhost onClick={() => close()}>
            <Translate name="form.button.cancel" filter={sentenceCase} />
          </ButtonGhost>
        </ColumnRigid>
        <ColumnRigid>
          <ButtonPrimary onClick={() => { validate(); onClose(); }}>
            <Translate name="form.button.validate" filter={sentenceCase} />
          </ButtonPrimary>
        </ColumnRigid>
      </Columns>
    </>;

  return (
    <Modal title={<Translate name='datamanage.title' />} onClose={onClose}>
      <Stack spacing="M">
        {
          displayNotification === NOTIF_STATUS.KO && <Notification2 title={<Translate name='notification.error' />} message={<Translate name='datamanage.uploaded.error' />}></Notification2>
        }
        <Inline justify="START">
          <ButtonPrimary onClick={() => downloadData(state.observationalDays, FC_JSON_FILE)}>
            <Translate name="datamanage.download" filter={sentenceCase} />
          </ButtonPrimary>
        </Inline>
        <Stack spacing="S">
          <SubTitle><Translate name="datamanage.warning.message" /></SubTitle>
          <InputFile
            accept='application/json'
            onChange={(files) => uploadTheData(files)}
            label={<Translate name="datamanage.upload" filter={sentenceCase} />}
          />
          <Inline justify="START">
            <ButtonSecondary onClick={() => migrateData()}>
              <Translate name="datamanage.migrate" filter={sentenceCase} />
            </ButtonSecondary>
          </Inline>
          {appState.state.config.devMode &&
            <>
              <Inline justify="START">
                <ButtonSecondary onClick={() => setDisplayReset(true)}>
                  <Translate name="datamanage.reset" filter={sentenceCase} />
                </ButtonSecondary>
              </Inline>
              {displayReset && confirmFrom(resetData, () => setDisplayReset(false))}
              <Inline justify="START">
                <ButtonSecondary onClick={() => setDisplayRandomData(true)}>
                  <Translate name="datamanage.generate" filter={sentenceCase} />
                </ButtonSecondary>
              </Inline>
              {displayRandomData && confirmFrom(generateObsDaysData, () => setDisplayRandomData(false))}
            </>
          }
        </Stack>
        <Inline justify="CENTER">
          <ButtonGhost onClick={() => setQueryParams({ popin: POPIN.SETTINGS, ...others })}>
            <Translate name="form.button.return" filter={sentenceCase} />
          </ButtonGhost>
        </Inline>
      </Stack>
    </Modal>
  );
};
