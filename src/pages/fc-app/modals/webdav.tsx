import React, { useContext, useEffect, useState } from 'react';

import { Modal } from '../../../components/library/modal/modal';
import { Translate } from '../../../components/translate/translate';
import { isDefined, isStringEmpty } from '../../../utils/utils';
import { SYNC_PROVIDER } from '../../../entities/sync/sync';
import { HTTP_STATUS, LoginStatus, LOGIN_STATUS } from '../../../entities/status';
import { Stack } from '../../../layout/stack/stack';
import { InputText } from '../../../components/library/form/input-text/input-text';
import { InputPassword } from '../../../components/library/form/input-password/input-password';
import { ButtonPrimary } from '../../../components/library/buttons/button-primary';
import { ColumnRigid } from '../../..//layout/columns/column-rigid/column-rigid';
import { Columns } from '../../../layout/columns/columns';
import { sentenceCase } from '../../../utils/string';
import { wdInitPath, WD_DEFAULT_PATH } from '../../../entities/sync/webdav';
import { Translation } from '../../../types/translations';
import { SubTitle } from '../../../components/library/text/sub-title/sub-title';
import { setLoginData } from 'app-state/app-actions';
import { appStore } from 'app-state/app-state';
import { ButtonGhost } from 'components/library/buttons/button-ghost';
import { useQueryParams } from 'hooks/useQueryParams';
import { QueryParams } from 'entities/query-params';
import { POPIN } from 'entities/pop-in';


export const WDModal = ({
  onClose,
}: {
  onClose: () => void;
}) => {
  const [url, setUrl] = useState('');
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [directory, setDirectory] = useState(WD_DEFAULT_PATH);
  const [message, setMessage] = useState<Translation | string>('');
  const [loginStatus, setLoginStatus] = useState<LoginStatus>(LOGIN_STATUS.ONGOING);
  const [disableButtons, setDisableButtons] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [{ popin, ...others }, setQueryParams] = useQueryParams<QueryParams>();
  const appState = useContext(appStore);

  const redirect = () => {
    console.debug('Redirect');
    setTimeout(() => {
      onClose();
    }, 2000);
  };

  const validate = () => {
    if (isStringEmpty(url) || isStringEmpty(login) || isStringEmpty(password)) {
      setMessage('connect.wd.fieldsempty');
    } else {
      setDisableButtons(true);
      setMessage('connect.ongoing');
      const usedDirectory = (isStringEmpty(directory)) ? '/' : directory;
      const credentials = { server: url, username: login, password: password };
      wdInitPath(credentials, usedDirectory)
        .then(() => {
          // console.debug(response);
          setLoginData(appState, {
            provider: SYNC_PROVIDER.WD, data: { credentials, directory: usedDirectory },
          });
          setLoginStatus(LOGIN_STATUS.OK);
        })
        .catch((error) => {
          setDisableButtons(false);
          setLoginStatus(LOGIN_STATUS.KO);
          if (isDefined(error.response)) {
            if (error.response.status === HTTP_STATUS.UNAUTHORIZED) {
              setMessage('connect.badcredentials');
            } else if (error.response.status === HTTP_STATUS.NOT_FOUND) {
              setMessage('connect.notfound');
            } else {
              setMessage('connect.wd.errorpath');
              console.error(error.response);
            }
          } else {
            setMessage(`General error - are the CORS correctly set for "${url}"?`);
            console.error(error);
          }
        });
    }
  };

  useEffect(() => {
    if (loginStatus === LOGIN_STATUS.OK) {
      setMessage('connect.ok');
      redirect();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loginStatus]);

  const displayMessage = () =>
    (message !== '') ?
      (<Translate name={message as Translation} />)
      : <>&nbsp;</>;

  return (
    <Modal title={<Translate name='connect.wd' />} onClose={onClose}>
      <Stack spacing="M">
        <Stack spacing="S">
          <InputText id='wd_url' label={<Translate name="connect.wd.url" />} value={url} onChange={setUrl} />
          <InputText id='wd_login' label={<Translate name="connect.wd.login" />} value={login} onChange={setLogin} />
          <InputPassword id='wd_password' label={<Translate name="connect.wd.password" />} value={password} onChange={setPassword} />
          <InputText id='wd_directory' label={<Translate name="connect.wd.directory" />} value={directory} onChange={setDirectory} />
        </Stack>
        <SubTitle>{displayMessage()}</SubTitle>
        <Columns spacing="S" justify="SPACE_AROUND">
          <ColumnRigid>
            <ButtonGhost disabled={disableButtons} onClick={() => setQueryParams({ popin: POPIN.CONNECT, ...others })}>
              <Translate name="form.button.prev" filter={sentenceCase} />
            </ButtonGhost>
          </ColumnRigid>
          <ColumnRigid>
            <ButtonPrimary disabled={disableButtons} onClick={() => validate()}>
              <Translate name="form.button.validate" filter={sentenceCase} />
            </ButtonPrimary>
          </ColumnRigid>
        </Columns>
      </Stack>
    </Modal>
  );
};
