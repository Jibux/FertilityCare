import React, { useContext, useEffect, useState } from 'react';

import { Modal } from '../../../components/library/modal/modal';
import { Translate } from '../../../components/translate/translate';
import { sentenceCase } from '../../../utils/string';
import { FieldRadio } from '../../../components/library/form/field-radio/field-radio';
import { SubTitle } from '../../../components/library/text/sub-title/sub-title';
import { Stack } from '../../../layout/stack/stack';
import { LANGUAGE_CHOICE, LANGUAGES_DESC } from '../../../entities/languages';
import { FIRST_DAYS_OF_WEEK } from '../../../entities/locales';
import { getWeekDays } from '../../../i18n/dates';
import { ButtonSecondary } from 'components/library/buttons/button-secondary';
import { POPIN } from 'entities/pop-in';
import { useQueryParams } from 'hooks/useQueryParams';
import { QueryParams } from 'entities/query-params';
import { appStore } from 'app-state/app-state';
import { setConfigData, getLanguage } from 'app-state/app-actions';
import { Toggle } from 'components/library/form/toggle/toggle';
import { IConfig } from 'entities/app-config';
import { Inline } from 'layout/inline/inline';
import { Log } from 'utils/logs';
import { ButtonPrimary } from 'components/library/buttons/button-primary';

export const SettingsModal = ({
  onClose,
}: {
  onClose: () => void;
}) => {
  const appState = useContext(appStore);
  const [config, setConfig] = useState<IConfig>(appState.state.config);
  const weekDays = getWeekDays(FIRST_DAYS_OF_WEEK.SUNDAY)('long', getLanguage(appState));
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [{ popin, ...others }, setQueryParams] = useQueryParams<QueryParams>();

  useEffect(() => {
    setConfigData(appState, config);
  }, [config]);

  return (
    <Modal title={<Translate name='header.profile.settings' />} onClose={onClose}>
      <Stack spacing="M">
        <Stack spacing="XS">
          <SubTitle>
            {<Translate name='settings.language' filter={sentenceCase} />}
          </SubTitle>
          {Object.keys(LANGUAGE_CHOICE).map(key =>
            (
            <FieldRadio
              value={LANGUAGE_CHOICE[key]}
              key={`lang_${LANGUAGE_CHOICE[key]}`}
              checked={config.lang === LANGUAGE_CHOICE[key]}
              name={'radio_lang'}
              onChange={(lang) => setConfig({ ...config, lang })}
            >
              <Translate name={LANGUAGES_DESC[key]} filter={sentenceCase} />
            </FieldRadio>
            ),
          )}
        </Stack>
        <Stack spacing="XS">
          <SubTitle>
            {<Translate name='settings.weekfirstday' filter={sentenceCase} />}
          </SubTitle>
          {Object.keys(FIRST_DAYS_OF_WEEK).map(key =>
            (
            <FieldRadio
              value={FIRST_DAYS_OF_WEEK[key]}
              key={`lang_${sentenceCase(key)}`}
              checked={config.firstDayOfWeek === FIRST_DAYS_OF_WEEK[key]}
              name={'radio_week1stday'}
              onChange={(firstDayOfWeek) => setConfig({ ...config, firstDayOfWeek })}
            >
              {sentenceCase(weekDays[FIRST_DAYS_OF_WEEK[key]])}
            </FieldRadio>
            ),
          )}
        </Stack>
        <Stack spacing="XS">
          <SubTitle>
            {<Translate name='settings.correctionmode' filter={sentenceCase} />}
          </SubTitle>
          <Toggle
            checked={config.correctionMode}
            id={'cb_correction'}
            key={'cb_correction'}
            name={'toggle_correction'}
            onChange={() => setConfig({ ...config, correctionMode: !config.correctionMode })}
          >
            {config.correctionMode ? 'ON' : 'OFF'}
          </Toggle>
        </Stack>
        <Stack spacing="XS">
          <SubTitle>
            {<Translate name='settings.devmode' filter={sentenceCase} />}
          </SubTitle>
          <Toggle
            checked={config.devMode}
            id={'cb_devmode'}
            key={'cb_devmode'}
            name={'cb_devmode'}
            onChange={() => setConfig({ ...config, devMode: !config.devMode })}
          >
            {config.devMode ? 'ON' : 'OFF'}
          </Toggle>
        </Stack>
        <Inline>
          <ButtonSecondary onClick={() => setQueryParams({ popin: POPIN.DATA_MANAGE, ...others })}>
            <Translate name="datamanage.title" filter={sentenceCase} />
          </ButtonSecondary>
        </Inline>
        {config.devMode &&
          <Inline>
            <ButtonPrimary onClick={Log.download}>
              <Translate name="settings.downloadlogs" filter={sentenceCase} />
            </ButtonPrimary>
          </Inline>
        }
        <Stack spacing="XS">
          <Translate name="version" filter={sentenceCase} parameters={{ version: process.env.REACT_APP_VERSION }} />
        </Stack>
      </Stack>
    </Modal>
  );
};
