import React, { useContext, useEffect } from 'react';

import { Modal } from '../../../components/library/modal/modal';
import { Translate } from '../../../components/translate/translate';
import { ButtonPrimary } from '../../../components/library/buttons/button-primary';
import { ButtonSecondary } from '../../../components/library/buttons/button-secondary';
import { Stack } from '../../../layout/stack/stack';
import { dbxLogin } from '../../../entities/sync/dropbox';
import { POPIN } from '../../../entities/pop-in';
import { useQueryParams } from 'hooks/useQueryParams';
import { QueryParams } from 'entities/query-params';
import { SubTitle } from 'components/library/text/sub-title/sub-title';
import { sentenceCase } from 'utils/string';
import { LOGIN_SYNC_METHOD, SYNC_METHOD_DESCRIPTION } from 'entities/sync/sync';
import { FieldRadio } from 'components/library/form/field-radio/field-radio';
import { appStore } from 'app-state/app-state';
import { resetLoginSyncMethod, setSyncMethod } from 'app-state/app-actions';


export const ConnectModal = ({
  onClose,
}: {
  onClose: () => void;
}) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [{ popin, ...others }, setQueryParams] = useQueryParams<QueryParams>();
  const appState = useContext(appStore);

  useEffect(() => {
    resetLoginSyncMethod(appState);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const goToDBX = () => {
    console.debug('Log in to Dropbox');
    setQueryParams({ popin: POPIN.LOGIN_DBX, ...others });
    dbxLogin();
  };

  const goToWD = () => {
    console.debug('Log in to WebDav');
    setQueryParams({ popin: POPIN.LOGIN_WD, ...others });
  };

  return (
    <Modal title={<Translate name='header.profile.login' />} onClose={onClose}>
      <Stack spacing="M">
        <ButtonPrimary onClick={() => goToDBX()}>
          <Translate name="connect.dbx" />
        </ButtonPrimary>
        <ButtonSecondary onClick={() => goToWD()}>
          <Translate name="connect.wd" />
        </ButtonSecondary>
        <Stack spacing="XS">
          <SubTitle>
            {<Translate name='sync_method.title' filter={sentenceCase} />}
          </SubTitle>
          {Object.keys(LOGIN_SYNC_METHOD).map(key => (
            <FieldRadio
              value={LOGIN_SYNC_METHOD[key]}
              key={`sync_method_${sentenceCase(key)}`}
              checked={appState.state.syncMethod === LOGIN_SYNC_METHOD[key]}
              name={'radio_sync_method'}
              onChange={(syncMethod) => setSyncMethod(appState, syncMethod)}
            >
              <Translate name={SYNC_METHOD_DESCRIPTION[key]} filter={sentenceCase} />
            </FieldRadio>
          ),
          )}
        </Stack>
      </Stack>
    </Modal>
  );
};
