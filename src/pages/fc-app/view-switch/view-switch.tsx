import React, { useContext } from 'react';

import { Calendar } from '../../../components/calendar/calendar';
import { Stack } from '../../../layout/stack/stack';
import { Columns } from '../../../layout/columns/columns';
import { LabelSmall } from '../../../components/library/text/label-small/label-small';
import { Sticker } from '../../../components/library/sticker/sticker';
import {
  getDataFrequencySummary,
  getDataMucusSummary,
  getDataPeriodSummary,
  getDataSimilaritySummary,
  getIntercourseValue,
  getPicDataSummary,
  getStickerRealColor,
  IObservationalDay,
  isACommentDay,
  isACorrectedDay,
  isAFrequencyDay,
  isAnAppointmentDay,
  isAStickerDay,
} from '../../../entities/objects/observational-day';
import { POPIN } from '../../../entities/pop-in';
import { getRealFullDayString } from 'utils/date';
import { useQueryParams } from 'hooks/useQueryParams';
import { QueryParams } from 'entities/query-params';
import { getLanguage, getObservationalDayLazy } from 'app-state/app-actions';
import { appStore } from 'app-state/app-state';
import { ColumnRigid } from 'layout/columns/column-rigid/column-rigid';
import { isDefined } from 'utils/utils';
import { Icon } from 'components/library/icon/icon';
import { ViewType, VIEW_TYPE } from 'entities/view-type';
import { getDateStringNumeric } from 'i18n/dates';
import { Cycle } from 'components/cycle/cycle';
import { fold } from 'utils/union-type-helper';


export function ViewSwitch() {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [{ popin, date, view: viewType = VIEW_TYPE.DEFAULT, ...others }, setQueryParams] = useQueryParams<QueryParams>();
  const appState = useContext(appStore);

  const getOldSticker = (observationalDay: IObservationalDay) => {
    if (isACorrectedDay(observationalDay) && isAStickerDay(observationalDay.oldObservationalDay)) {
      return <Sticker size='S' type={getStickerRealColor(observationalDay.oldObservationalDay)} />;
    }
  };

  const dayString = (day: Date) => getDateStringNumeric(day, getLanguage(appState));

  const dayRenderer = (day: Date) => {
    const observationalDay = getObservationalDayLazy(appState, day);
    return (
      <>
        <Stack spacing="NONE">
          <Columns justify="SPACE_BETWEEN">
            <ColumnRigid>
              {isDefined(observationalDay) && isAnAppointmentDay(observationalDay) ?
                <LabelSmall className='RED'>
                  {dayString(day)}
                </LabelSmall>
                :
                <LabelSmall>
                  {dayString(day)}
                </LabelSmall>
              }
            </ColumnRigid>
            {isDefined(observationalDay) && isACommentDay(observationalDay) &&
              <ColumnRigid>
                <LabelSmall><Icon name='ball' size='S' /></LabelSmall>
              </ColumnRigid>
            }
          </Columns>
          {isDefined(observationalDay) &&
            <>
              <Columns justify="CENTER" align="CENTER">
                <ColumnRigid>
                  <LabelSmall>{getIntercourseValue(observationalDay)}</LabelSmall>
                </ColumnRigid>
                <ColumnRigid>&nbsp;</ColumnRigid>
                <ColumnRigid>{getOldSticker(observationalDay)}</ColumnRigid>
                <ColumnRigid>&nbsp;</ColumnRigid>
                <ColumnRigid>
                  {isAStickerDay(observationalDay) ?
                    <Sticker type={getStickerRealColor(observationalDay)} />
                    : ''
                  }
                </ColumnRigid>
                <ColumnRigid>&nbsp;</ColumnRigid>
                <ColumnRigid>
                  <LabelSmall>{getPicDataSummary(observationalDay)}</LabelSmall>
                </ColumnRigid>
              </Columns>
              <LabelSmall>{getDataPeriodSummary(observationalDay)}</LabelSmall>
              <LabelSmall>{getDataMucusSummary(observationalDay)}</LabelSmall>
              <LabelSmall>
                {isAFrequencyDay(observationalDay) && <>{getDataFrequencySummary(observationalDay)}&nbsp;</>}
                {getDataSimilaritySummary(observationalDay)}
              </LabelSmall>
            </>
          }
        </Stack>
      </>
    );
  };

  const onClick = (d: Date) => {
    setQueryParams({ popin: POPIN.EDIT_DAY, date: getRealFullDayString(d), view: viewType, ...others });
  };

  const getRenderer = fold<ViewType, JSX.Element>({
    monthly: () => (<Calendar
      dayRenderer={dayRenderer}
      onClick={onClick}
    />),
    cycle: () => (<Cycle
      dayRenderer={dayRenderer}
      onClick={onClick}
    />),
  })(viewType);

  return getRenderer;
}