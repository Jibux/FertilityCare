import React, { useState } from 'react';

import { ComponentPreview } from '../components-preview/components-preview';
import { Header } from '../../../components/header/header';
import { Calendar } from '../../../components/calendar/calendar';
import { Avatar } from '../../../components/library/avatar/avatar';
import { isTheSameDay } from '../../../utils/date';
import { Icon } from '../../../components/library/icon/icon';
import { Stack } from '../../../layout/stack/stack';
import { Modal } from '../../../components/library/modal/modal';
import { DateComp as DateFormatter } from '../../../components/library/date/date';
import { FieldRadio } from '../../../components/library/form/field-radio/field-radio';
import { ButtonPrimary } from '../../../components/library/buttons/button-primary';
import { ColumnFlexible } from '../../../layout/columns/column-flexible/column-flexible';
import { ColumnRigid } from '../../../layout/columns/column-rigid/column-rigid';
import { Columns } from '../../../layout/columns/columns';
import { ButtonGhost } from '../../../components/library/buttons/button-ghost';
import { Toggle } from '../../../components/library/form/toggle/toggle';
import { Label } from '../../../components/library/text/label/label';
import { Inline } from '../../../layout/inline/inline';
import { Container } from '../../../layout/container/container';
import { LabelSmall } from '../../../components/library/text/label-small/label-small';
import { Sticker } from '../../../components/library/sticker/sticker';

type ModalState = { isVisible: boolean, date: Date | undefined };

const EditDayModal = ({ date, onClose }: { date: Date, onClose: () => void }) => {
  const [value, setValue] = useState<string | undefined>();
  return (
    <Modal title={<DateFormatter>{date}</DateFormatter>} onClose={onClose}>
      <Stack spacing="M">
        <Stack spacing="S">
          <Container paddingY="M">
            <Inline spacingX="M" align="CENTER">
              <Toggle id="toggle" checked={true} onChange={() => {}} >Label</Toggle>
              <Label htmlFor="toggle">Hello World</Label>
            </Inline>
          </Container>
          <FieldRadio value={'value1'} checked={value === 'value1'} name="exemple" onChange={setValue}>
            Value 1
          </FieldRadio>
          <FieldRadio value={'value2'} checked={value === 'value2'} name="exemple" onChange={setValue}>
            Value 2
          </FieldRadio>
          <FieldRadio value={'value3'} checked={value === 'value3'} name="exemple" onChange={setValue}>
            Value 3
          </FieldRadio>
          <FieldRadio value={'value4'} checked={value === 'value4'} name="exemple" onChange={setValue}>
            Value 4
          </FieldRadio>
          <FieldRadio value={'value5'} checked={value === 'value5'} name="exemple" onChange={setValue}>
            Value 5
          </FieldRadio>
          <FieldRadio value={'value6'} checked={value === 'value6'} name="exemple" onChange={setValue}>
            Value 6
          </FieldRadio>
        </Stack>
        <Columns spacing="M">
          <ColumnFlexible>&nbsp;</ColumnFlexible>
          <ColumnRigid>
            <ButtonGhost onClick={onClose}>
              Cancel
            </ButtonGhost>
          </ColumnRigid>
          <ColumnRigid>
            <ButtonPrimary onClick={() => {}} iconRight="chevron-right">Validate</ButtonPrimary>
          </ColumnRigid>
        </Columns>
      </Stack>
    </Modal>
  );
};
export function Templates() {
  const [modalState, setModalState] = useState<ModalState>({ isVisible: false, date: undefined });

  const dayRenderer = (day: Date) => (
    <>
      {`${day.getDate()}`.padStart(2, '0')}
      /
      {`${day.getMonth()}`.padStart(2, '0')}
    </>
  );

  const onClick = (day:Date) => {
    console.log('Clicked', day);
    setModalState({ isVisible: true, date: day });
  };

  const focusedDate = new Date();
  focusedDate.setDate(focusedDate.getMonth() + 5);
  const today = new Date();

  return (
    <>
      <h2>Templates</h2>
      <ComponentPreview label="Header">
        <Header currentDateView={new Date()}></Header>
        <div style={{ marginTop: '3rem' }}>
          <Header currentDateView={new Date()}></Header>
        </div>
      </ComponentPreview>
      <ComponentPreview label="Calendar">
        <Calendar focusedDate={focusedDate} dayRenderer={dayRenderer} onClick={onClick} />
        <Calendar
          dayRenderer={(day: Date) => (
            <>
              {`${day.getDate()}`.padStart(2, '0')}
              /
              {`${day.getMonth()}`.padStart(2, '0')}
              /
              {`${day.getFullYear()}`}

             {isTheSameDay(today, day) ? <Avatar /> : <Icon name="user-add" size="L"/>}
            </>)
          }
          onClick={onClick}
        />
      </ComponentPreview>
      <ComponentPreview label="Mise en situation">
        <Stack spacing="NONE">
          <Header currentDateView={new Date()}></Header>
          <Calendar
            dayRenderer={(day: Date) => (
              <Stack spacing="M">
              <Label>
                {`${day.getDate()}`.padStart(2, '0')}
                /
                {`${day.getMonth()}`.padStart(2, '0')}
                /
                {`${day.getFullYear()}`}
              </Label>

              <Columns justify="CENTER" align="CENTER">
                {isTheSameDay(today, day) ? <Sticker type="RED" /> : <Sticker type="LIGHT_YELLOW"/>}
              </Columns>
              <LabelSmall>Description</LabelSmall>
              </Stack>)
            }
            onClick={onClick}
          />
        </Stack>
          {
            modalState.isVisible 
              ? <EditDayModal date={modalState.date} onClose={() => setModalState({ isVisible: false, date: undefined })}/>
              : <></>
          }
      </ComponentPreview>
    </>
  );
}