import React from 'react';

import { ComponentPreview } from '../components-preview/components-preview';
import { Icon } from '../../../components/library/icon/icon';

export function Icons() {
  return (
    <>
      <h2>Icons</h2>
      <ComponentPreview label="arrow-top">
        <Icon name="arrow-top"></Icon>
      </ComponentPreview>
      <ComponentPreview label="arrow-bottom">
        <Icon name="arrow-bottom"></Icon>
      </ComponentPreview>
      <ComponentPreview label="arrow-left">
        <Icon name="arrow-left"></Icon>
      </ComponentPreview>
      <ComponentPreview label="arrow-right">
        <Icon name="arrow-right"></Icon>
      </ComponentPreview>
      <ComponentPreview label="backward">
        <Icon name="backward"></Icon>
      </ComponentPreview>
      <ComponentPreview label="burger">
        <Icon name="burger"></Icon>
      </ComponentPreview>
      <ComponentPreview label="calendar-cycle">
        <Icon name="calendar-cycle"></Icon>
      </ComponentPreview>
      <ComponentPreview label="calendar-daily">
        <Icon name="calendar-daily"></Icon>
      </ComponentPreview>
      <ComponentPreview label="calendar-day">
        <Icon name="calendar-day"></Icon>
      </ComponentPreview>
      <ComponentPreview label="calendar-monthly">
        <Icon name="calendar-monthly"></Icon>
      </ComponentPreview>
      <ComponentPreview label="check">
        <Icon name="check"></Icon>
      </ComponentPreview>
      <ComponentPreview label="chevron-bottom">
        <Icon name="chevron-bottom"></Icon>
      </ComponentPreview>
      <ComponentPreview label="chevron-left">
        <Icon name="chevron-left"></Icon>
      </ComponentPreview>
      <ComponentPreview label="chevron-right">
        <Icon name="chevron-right"></Icon>
      </ComponentPreview>
      <ComponentPreview label="chevron-top">
        <Icon name="chevron-top"></Icon>
      </ComponentPreview>
      <ComponentPreview label="cog">
        <Icon name="cog"></Icon>
      </ComponentPreview>
      <ComponentPreview label="cross">
        <Icon name="cross"></Icon>
      </ComponentPreview>
      <ComponentPreview label="enter">
        <Icon name="enter"></Icon>
      </ComponentPreview>
      <ComponentPreview label="error">
        <Icon name="error"></Icon>
      </ComponentPreview>
      <ComponentPreview label="exit">
        <Icon name="exit"></Icon>
      </ComponentPreview>
      <ComponentPreview label="eye-none">
        <Icon name="eye-none"></Icon>
      </ComponentPreview>
      <ComponentPreview label="eye">
        <Icon name="eye"></Icon>
      </ComponentPreview>
      <ComponentPreview label="forward">
        <Icon name="forward"></Icon>
      </ComponentPreview>
      <ComponentPreview label="pen">
        <Icon name="pen"></Icon>
      </ComponentPreview>
      <ComponentPreview label="plug">
        <Icon name="plug"></Icon>
      </ComponentPreview>
      <ComponentPreview label="sync">
        <Icon name="sync"></Icon>
      </ComponentPreview>
      <ComponentPreview label="user-add">
        <Icon name="user-add"></Icon>
      </ComponentPreview>
      <ComponentPreview label="user-unknown">
        <Icon name="user-unknown"></Icon>
      </ComponentPreview>
      <ComponentPreview label="user">
        <Icon name="user"></Icon>
      </ComponentPreview>
    </>
  );
}