import React from 'react';

import { Icons } from './icons/icons';
import { Texts } from './texts/texts';
import { Components } from './components/components';
import { Templates } from './templates/templates';

import './components-library.css';

export function ComponentLibrary() {
  return (
    <div className="container">
      <Icons></Icons>
      <Texts></Texts>
      <Components></Components>
      <Templates></Templates>
    </div>
  );
}