import React, { useState } from 'react';

import { ComponentPreview } from '../../components-preview/components-preview';
import { Header } from '../../../../components/library/header/header';
import { Text2 } from '../../../../components/library/text/text/text';
import { Title } from '../../../../components/library/text/title/title';
import { ButtonIcon } from '../../../../components/library/buttons/button-icon';
import { Card } from '../../../../components/library/card/card';
import { CardWithHeader } from '../../../../components/library/card-with-header/card-with-header';
import { Sticker } from '../../../../components/library/sticker/sticker';
import { ButtonPrimary } from '../../../../components/library/buttons/button-primary';
import { Notification2 } from '../../../../components/library/notification/notification';
import { Overlay } from '../../../../components/library/overlay/overlay';
import { Dropdown } from '../../../../components/library/dropdown/dropdown';
import { Avatar } from '../../../../components/library/avatar/avatar';
import { Translate } from '../../../../components/translate/translate';

export function Others() {
  const log = (str: string) => () => console.log(str);
  const [displayNotification, setDisplayNotification] = useState(false);
  const [displayOverlay, setDisplayOverlay] = useState(false);

  const onDiplayNotification = () => {
    setDisplayNotification(true);
    setTimeout(() => {
      setDisplayNotification(false);
    }, 5000);
  };

  const onDiplayOverlay = () => {
    setDisplayOverlay(true);
    setTimeout(() => {
      setDisplayOverlay(false);
    }, 5000);
  };

  return (
    <>
      <h3>Others</h3>
      <ComponentPreview label="Avatar">
        <Avatar/>
        <Avatar image="https://s2.qwant.com/thumbr/700x0/7/a/3d924739998ba71b9e1181aa375b139838d4abf77520983293c55e296456ee/Luke-Skywalker-in-Star-Wars-Explosion.jpg?u=http%3A%2F%2Fscreenrant.com%2Fwp-content%2Fuploads%2F2017%2F01%2FLuke-Skywalker-in-Star-Wars-Explosion.jpg&q=0&b=1&p=0&a=1" name="42"/>
        <Avatar image="https://s2.qwant.com/thumbr/700x0/7/a/3d924739998ba71b9e1181aa375b139838d4abf77520983293c55e296456ee/Luke-Skywalker-in-Star-Wars-Explosion.jpg?u=http%3A%2F%2Fscreenrant.com%2Fwp-content%2Fuploads%2F2017%2F01%2FLuke-Skywalker-in-Star-Wars-Explosion.jpg&q=0&b=1&p=0&a=1"/>
        <Avatar name="42"/>
      </ComponentPreview>

      <ComponentPreview label="Header">
        <Header>
          <Text2>Left</Text2>
          <Text2>Right</Text2>
        </Header>
        <Header>
          <Title>Hello World</Title>
        </Header>
        <Header>
          <Title>Hello World</Title>
          <ButtonIcon icon="cross" onClick={log('close')}></ButtonIcon>
        </Header>
      </ComponentPreview>

      <ComponentPreview label="Card">
        <Card>Hello World</Card>
      </ComponentPreview>

      <ComponentPreview label="CardWithHeader">
        <CardWithHeader>
          {{
            header: 'My Awesome Title',
            content: 'Hello World',
          }}
        </CardWithHeader>
        <CardWithHeader onClose={log('close')}>
          {{
            header: (<>My Awesome Title</>),
            content: <Text2>Hello World</Text2>,
          }}
        </CardWithHeader>
      </ComponentPreview>

      <ComponentPreview label="Sticker">
        <Sticker type="RED"></Sticker>
        <Sticker type="WHITE"></Sticker>
        <Sticker type="LIGHT_YELLOW"></Sticker>
        <Sticker type="LIGHT_GREEN"></Sticker>
        <Sticker type="DARK_YELLOW"></Sticker>
        <Sticker type="DARK_GREEN"></Sticker>
      </ComponentPreview>

      <ComponentPreview label="Notification2">
        <ButtonPrimary disabled={displayNotification} onClick={onDiplayNotification}>Show Notification2</ButtonPrimary>
        {
          displayNotification && <Notification2 title="Hello World" message="An awesome message, is always the best solution"></Notification2>
        }
      </ComponentPreview>

      <ComponentPreview label="Overlay">
        <ButtonPrimary disabled={displayOverlay} onClick={onDiplayOverlay}>Show Overlay</ButtonPrimary>
        {
          displayOverlay && <Overlay>Hello World</Overlay>
        }
      </ComponentPreview>
      <ComponentPreview label="Dropdown">
          <Dropdown dropDownContent={{
            menu1: (<>Hello</>),
            menu2: (<>World</>),
            menu3: (<>!!!</>),
          }}>
            Hello World
          </Dropdown>
          <Dropdown position="right" dropDownContent={{
            menu1: (<>Hello</>),
            menu2: (<>World</>),
            menu3: (<>!!!</>),
          }}>
            Hello World
          </Dropdown>
      </ComponentPreview>
      <ComponentPreview label="Translate">
        <div>
          <Translate name="componentsLibrary.welcomeMessage" parameters={{ userName: 'Luke Skywalker' }}/>
        </div>
        <div>
          <Translate lang="en" name="componentsLibrary.welcomeMessage" parameters={{ userName: 'Luke Skywalker' }}/>
        </div>
      </ComponentPreview>
    </>
  );
}