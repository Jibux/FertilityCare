import React from 'react';

import { ComponentPreview } from '../../components-preview/components-preview';
import { ButtonPrimary } from '../../../../components/library/buttons/button-primary';
import { ButtonSecondary } from '../../../../components/library/buttons/button-secondary';
import { ButtonGhost } from '../../../../components/library/buttons/button-ghost';
import { ButtonGhostInverted } from '../../../../components/library/buttons/button-ghost-inverted';
import { ButtonIcon } from '../../../../components/library/buttons/button-icon';

export function Buttons() {
  const log = (str: string) => () => console.log(str);
  
  return (
    <>
      <h3>Buttons</h3>
      <ComponentPreview label="ButtonPrimary">
        <ButtonPrimary onClick={log('click')}>Hello World</ButtonPrimary>
        <ButtonPrimary iconLeft="cog" onClick={log('click')}>Hello World</ButtonPrimary>
        <ButtonPrimary iconRight="cog" onClick={log('click')}>Hello World</ButtonPrimary>
        <ButtonPrimary iconLeft="cog" iconRight="cog" onClick={log('click')}>Hello World</ButtonPrimary>
      </ComponentPreview>
      <ComponentPreview label="ButtonSecondary">
        <ButtonSecondary onClick={log('click')}>Hello World</ButtonSecondary>
        <ButtonSecondary iconLeft="cog" onClick={log('click')}>Hello World</ButtonSecondary>
        <ButtonSecondary iconRight="cog" onClick={log('click')}>Hello World</ButtonSecondary>
        <ButtonSecondary iconLeft="cog" iconRight="cog" onClick={log('click')}>Hello World</ButtonSecondary>
      </ComponentPreview>
      <ComponentPreview label="ButtonGhost">
        <ButtonGhost onClick={log('click')}>Hello World</ButtonGhost>
        <ButtonGhost iconLeft="cog" onClick={log('click')}>Hello World</ButtonGhost>
        <ButtonGhost iconRight="cog" onClick={log('click')}>Hello World</ButtonGhost>
        <ButtonGhost iconLeft="cog" iconRight="cog" onClick={log('click')}>Hello World</ButtonGhost>
      </ComponentPreview>
      <ComponentPreview label="ButtonGhostInverted">
        <div style={{ display: 'flex', flexDirection: 'column', background: '#000', color: 'white', margin: '0 -3rem', padding: '3rem' }}>
            <ButtonGhostInverted onClick={log('click')}>Hello World</ButtonGhostInverted>
            &nbsp;
            <ButtonGhostInverted iconLeft="cog" onClick={log('click')}>Hello World</ButtonGhostInverted>
            &nbsp;
            <ButtonGhostInverted iconRight="cog" onClick={log('click')}>Hello World</ButtonGhostInverted>
            &nbsp;
            <ButtonGhostInverted iconLeft="cog" iconRight="cog" onClick={log('click')}>Hello World</ButtonGhostInverted>
        </div>
      </ComponentPreview>
      <ComponentPreview label="ButtonIcon">
        <ButtonIcon icon="cog" onClick={log('click')}></ButtonIcon>
      </ComponentPreview>
    </>
  );
}