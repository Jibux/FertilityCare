import React, { useState } from 'react';

import { ComponentPreview } from '../../components-preview/components-preview';
import { InputText } from '../../../../components/library/form/input-text/input-text';
import { InputPassword } from '../../../../components/library/form/input-password/input-password';
import { Toggle } from '../../../../components/library/form/toggle/toggle';
import { InputRadio } from '../../../../components/library/form/input-radio/input-radio';
import { RadioSticker } from '../../../../components/library/form/radio-sticker/radio-sticker';
import { FieldRadio } from '../../../../components/library/form/field-radio/field-radio';

export function Forms() {
  const log = (str: string) => () => console.log(str);

  const [currentRadio, setCurrentRadio] = useState('Hello');
  const [currentFieldRadio, setCurrentFieldRadio] = useState('Hello');
  const [currentRadioSticker, setCurrentRadioSticker] = useState('DARK_GREEN');

  return (
    <>
      <h3>Form</h3>
      <ComponentPreview label="InputText">
        <InputText label="Text2" value="Hello World" onChange={log('change')}/>
        <InputText label="Text2" disabled value="Hello World" onChange={log('change')}/>
      </ComponentPreview>
      <ComponentPreview label="InputPassword">
        <InputPassword label="Password" value="Hello World" onChange={log('change')}/>
        <InputPassword label="Password" disabled value="Hello World" onChange={log('change')}/>
      </ComponentPreview>
      <ComponentPreview label="Toggle">
        <Toggle checked={true} onChange={log('change')}>Label</Toggle>
        <Toggle checked={false} onChange={log('change')}>Label</Toggle>
        <Toggle disabled checked={true} onChange={log('change')}>Label</Toggle>
        <Toggle disabled checked={false} onChange={log('change')}>Label</Toggle>
      </ComponentPreview>
      <ComponentPreview label="InputRadio">
        <InputRadio value="Hello" name="radio1" checked={currentRadio === 'Hello'} onChange={setCurrentRadio}/>
        <InputRadio value="World" name="radio1" checked={currentRadio === 'World'} onChange={setCurrentRadio}/>
        <InputRadio value="___" name="radio1" disabled checked={currentRadio === '___'} onChange={setCurrentRadio}/>
      </ComponentPreview>
      <ComponentPreview label="RadioSticker">
        <RadioSticker current={currentRadioSticker} type="DARK_GREEN" value="DARK_GREEN" name="radio10" onChange={setCurrentRadioSticker}/>
        <RadioSticker current={currentRadioSticker} type="DARK_YELLOW" value="DARK_YELLOW" name="radio10" onChange={setCurrentRadioSticker}/>
        <RadioSticker current={currentRadioSticker} type="LIGHT_GREEN" value="LIGHT_GREEN" name="radio10" onChange={setCurrentRadioSticker}/>
        <RadioSticker current={currentRadioSticker} type="LIGHT_YELLOW" value="LIGHT_YELLOW" name="radio10" onChange={setCurrentRadioSticker}/>
        <RadioSticker current={currentRadioSticker} type="RED" value="RED" name="radio10" onChange={setCurrentRadioSticker}/>
        <RadioSticker current={currentRadioSticker} type="WHITE" value="WHITE" name="radio10" onChange={setCurrentRadioSticker}/>
        <RadioSticker current={currentRadioSticker} type="WHITE" disabled value="___" name="radio10" onChange={setCurrentRadioSticker}/>
      </ComponentPreview>
      <ComponentPreview label="FieldRadio">
        <FieldRadio value="Hello" name="radio2" checked={currentFieldRadio === 'Hello'} onChange={setCurrentFieldRadio}>
          Hello
        </FieldRadio>
        <FieldRadio value="World" name="radio2" checked={currentFieldRadio === 'World'} onChange={setCurrentFieldRadio}>
          World
        </FieldRadio>
        <FieldRadio value="___" name="radio2" disabled checked={currentFieldRadio === '___'} onChange={setCurrentFieldRadio}>
          PLOP
        </FieldRadio>
      </ComponentPreview>
    </>
  );
}