import React from 'react';

import { Buttons } from './buttons/buttons';
import { Forms } from './forms/forms';
import { Others } from './others/others';
import { Layout } from './layout/layout';

export function Components() {
  return (
    <>
      <h2>Components</h2>
      <Buttons></Buttons>
      <Forms></Forms>
      <Others></Others>
      <Layout></Layout>
    </>
  );
}