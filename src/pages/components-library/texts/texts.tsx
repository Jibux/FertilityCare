import React from 'react';

import { ComponentPreview } from '../components-preview/components-preview';
import { Title } from '../../../components/library/text/title/title';
import { SubTitle } from '../../../components/library/text/sub-title/sub-title';
import { Label } from '../../../components/library/text/label/label';
import { LabelSmall } from '../../../components/library/text/label-small/label-small';
import { Text2 } from '../../../components/library/text/text/text';
import { Link } from '../../../components/library/text/link/link';
import { ROUTES } from '../../../routes';
import { Note } from '../../../components/library/text/note/note';

export function Texts() {
  return (
    <>
      <h2>Text2</h2>
      <ComponentPreview label="Title">
        <Title>Hello World</Title>
      </ComponentPreview>
      <ComponentPreview label="Subtitle">
        <SubTitle>Hello World</SubTitle>
      </ComponentPreview>
      <ComponentPreview label="Label">
        <Label>Hello World</Label>
      </ComponentPreview>
      <ComponentPreview label="LabelSmall">
        <LabelSmall>Hello World</LabelSmall>
      </ComponentPreview>
      <ComponentPreview label="TextDefault">
        <Text2>Hello World</Text2>
      </ComponentPreview>
      <ComponentPreview label="Link">
        <Link to={ROUTES.COMPONENTS_LIBRARY}>Hello World</Link>
      </ComponentPreview>
      <ComponentPreview label="Note">
        <Note>Hello World</Note>
      </ComponentPreview>
    </>
  );
}