import { Translation } from '../../types/translations';
import { StickerColor, STICKER_COLOR_WITH_BABY, STICKER_COLOR } from '../color';

export const OD_VERSIONS = {
  0: 'Initial version',
  1: 'Add similarity',
  2: 'Add seperate pic value',
} as const;

const OD_VERSIONS_KEYS = Object.keys(OD_VERSIONS).map((s) => parseInt(s));

export const OD_MIN_VERSION = Math.min(...OD_VERSIONS_KEYS);
export const OD_MAX_VERSION = Math.max(...OD_VERSIONS_KEYS);

export type ODVersions = keyof typeof OD_VERSIONS;
// TODO: Function that takes param and return param
export type ODMigrations = { [key in ODVersions]: Function };

export type Data = {
  readonly acronym: string;
  readonly description?: Translation;
  readonly extendable?: boolean;
};

export type RODataArray = readonly Data[];

type KeysData = {
  readonly sticker: StickerColor[];
  readonly period: RODataArray;
  readonly mucus: RODataArray;
  readonly mucusExtended: RODataArray;
  readonly frequency: RODataArray;
  readonly similarity: RODataArray;
  readonly misc: RODataArray;
  readonly pic: RODataArray;
};

export type ODKey = keyof KeysData | 'comment';
export type ODValue = number | string;

type KeysInfo = {
  [key in ODKey]: {
    readonly name: Translation,
    readonly multiple: boolean,
    readonly valueType: ValueOf<typeof VALUE_TYPE>,
    readonly keyContentType: ValueOf<typeof KEY_CONTENT_TYPE>,
    readonly linkedToKey?: ODKey,
  };
};


export const VALUE_TYPE = {
  STRING: 'string',
  NUMBER: 'number',
} as const;

export const KEY_CONTENT_TYPE = {
  ARRAY: 'array',
  SINGLE_VALUE: 'single',
} as const;

export const KEYS_ACRONYM = {
  P_H: 'H',
  P_M: 'M',
  P_L: 'L',
  P_VL: 'VL',
  P_B: 'B',
  M_0: '0',
  M_2: '2',
  M_2W: '2W',
  M_4: '4',
  M_6: '6',
  M_8: '8',
  M_10: '10',
  M_10DL: '10DL',
  M_10SL: '10SL',
  M_10WL: '10WL',
  M_C: 'C',
  M_K: 'K',
  M_L: 'L',
  M_G: 'G',
  M_P: 'P',
  M_Y: 'Y',
  M_B: 'B',
  F_x1: 'x1',
  F_x2: 'x2',
  F_x3: 'x3',
  F_AD: 'AD',
  SIM_UP: '↑',
  SIM_DOWN: '↓',
  SIM_YES: 'YES',
  SIM_NO: 'NO',
  MISC_P: 'P',
  MISC_I: 'I',
  MISC_A: 'A',
  PIC_P: 'P',
  PIC_1: '1',
  PIC_2: '2',
  PIC_3: '3',
} as const;


export const OD_PERIOD: RODataArray = [
  { acronym: KEYS_ACRONYM.P_H, description: 'observationalData.period.H' },
  { acronym: KEYS_ACRONYM.P_M, description: 'observationalData.period.M' },
  { acronym: KEYS_ACRONYM.P_L, description: 'observationalData.period.L' },
  { acronym: KEYS_ACRONYM.P_VL, description: 'observationalData.period.VL' },
  { acronym: KEYS_ACRONYM.P_B, description: 'observationalData.period.B' },
] as const;

export const OD_MUCUS: RODataArray = [
  { acronym: KEYS_ACRONYM.M_0, description: 'observationalData.mucus.0' },
  { acronym: KEYS_ACRONYM.M_2, description: 'observationalData.mucus.2' },
  { acronym: KEYS_ACRONYM.M_2W, description: 'observationalData.mucus.2W' },
  { acronym: KEYS_ACRONYM.M_4, description: 'observationalData.mucus.4' },
  { acronym: KEYS_ACRONYM.M_6, description: 'observationalData.mucus.6', extendable: true },
  { acronym: KEYS_ACRONYM.M_8, description: 'observationalData.mucus.8', extendable: true },
  { acronym: KEYS_ACRONYM.M_10, description: 'observationalData.mucus.10', extendable: true },
  { acronym: KEYS_ACRONYM.M_10DL, description: 'observationalData.mucus.10DL' },
  { acronym: KEYS_ACRONYM.M_10SL, description: 'observationalData.mucus.10SL' },
  { acronym: KEYS_ACRONYM.M_10WL, description: 'observationalData.mucus.10WL' },
] as const;

export const OD_MUCUS_EXTENDED: RODataArray = [
  { acronym: KEYS_ACRONYM.M_C, description: 'observationalData.mucus.C' },
  { acronym: KEYS_ACRONYM.M_K, description: 'observationalData.mucus.K' },
  { acronym: KEYS_ACRONYM.M_L, description: 'observationalData.mucus.L' },
  { acronym: KEYS_ACRONYM.M_G, description: 'observationalData.mucus.G' },
  { acronym: KEYS_ACRONYM.M_P, description: 'observationalData.mucus.P' },
  { acronym: KEYS_ACRONYM.M_Y, description: 'observationalData.mucus.Y' },
  { acronym: KEYS_ACRONYM.M_B, description: 'observationalData.mucus.B' },
] as const;

export const OD_FREQUENCY: RODataArray = [
  { acronym: KEYS_ACRONYM.F_x1, description: 'observationalData.frequency.x1' },
  { acronym: KEYS_ACRONYM.F_x2, description: 'observationalData.frequency.x2' },
  { acronym: KEYS_ACRONYM.F_x3, description: 'observationalData.frequency.x3' },
  { acronym: KEYS_ACRONYM.F_AD, description: 'observationalData.frequency.AD' },
] as const;

export const OD_SIMILARITY: RODataArray = [
  { acronym: KEYS_ACRONYM.SIM_YES, description: 'observationalData.similarity.yes' },
  { acronym: KEYS_ACRONYM.SIM_UP, description: 'observationalData.similarity.up' },
  { acronym: KEYS_ACRONYM.SIM_DOWN, description: 'observationalData.similarity.down' },
] as const;

export const OD_MISC: RODataArray = [
  { acronym: KEYS_ACRONYM.MISC_I, description: 'observationalData.misc.I' },
  { acronym: KEYS_ACRONYM.MISC_A, description: 'observationalData.misc.A' },
] as const;

export const OD_PIC: RODataArray = [
  { acronym: KEYS_ACRONYM.PIC_P, description: 'observationalData.pic.P' },
  { acronym: KEYS_ACRONYM.PIC_1, description: 'observationalData.pic.1' },
  { acronym: KEYS_ACRONYM.PIC_2, description: 'observationalData.pic.2' },
  { acronym: KEYS_ACRONYM.PIC_3, description: 'observationalData.pic.3' },
] as const;


export const KEYS_DATA: KeysData = {
  sticker: STICKER_COLOR.map(v => v),
  period: OD_PERIOD,
  mucus: OD_MUCUS,
  mucusExtended: OD_MUCUS_EXTENDED,
  frequency: OD_FREQUENCY,
  similarity: OD_SIMILARITY,
  misc: OD_MISC,
  pic: OD_PIC,
} as const;

export const KEYS_INFO: KeysInfo = {
  sticker: {
    name: 'observationalData.key.sticker',
    multiple: false,
    valueType: VALUE_TYPE.NUMBER,
    keyContentType: KEY_CONTENT_TYPE.ARRAY,
  },
  period: {
    name: 'observationalData.key.period',
    multiple: false,
    valueType: VALUE_TYPE.NUMBER,
    keyContentType: KEY_CONTENT_TYPE.ARRAY,
  },
  mucus: {
    name: 'observationalData.key.mucus',
    multiple: false,
    valueType: VALUE_TYPE.NUMBER,
    keyContentType: KEY_CONTENT_TYPE.ARRAY,
    linkedToKey: 'mucusExtended',
  },
  mucusExtended: {
    name: 'observationalData.key.mucus',
    multiple: true,
    valueType: VALUE_TYPE.NUMBER,
    keyContentType: KEY_CONTENT_TYPE.ARRAY,
  },
  frequency: {
    name: 'observationalData.key.frequency',
    multiple: false,
    valueType: VALUE_TYPE.NUMBER,
    keyContentType: KEY_CONTENT_TYPE.ARRAY,
  },
  similarity: {
    name: 'observationalData.key.similarity',
    multiple: false,
    valueType: VALUE_TYPE.NUMBER,
    keyContentType: KEY_CONTENT_TYPE.ARRAY,
  },
  misc: {
    name: 'observationalData.key.misc',
    multiple: true,
    valueType: VALUE_TYPE.NUMBER,
    keyContentType: KEY_CONTENT_TYPE.ARRAY,
  },
  pic: {
    name: 'observationalData.key.pic',
    multiple: false,
    valueType: VALUE_TYPE.NUMBER,
    keyContentType: KEY_CONTENT_TYPE.ARRAY,
  },
  comment: {
    name: 'observationalData.key.comment',
    multiple: false,
    valueType: VALUE_TYPE.STRING,
    keyContentType: KEY_CONTENT_TYPE.SINGLE_VALUE,
  },
} as const;

export const STICKER_COLOR_WITH_BABY_IDX = STICKER_COLOR_WITH_BABY.map((color: StickerColor) => STICKER_COLOR.indexOf(color));
