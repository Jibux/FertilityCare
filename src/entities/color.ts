export const STICKER_COLOR = ['RED', 'WHITE', 'LIGHT_GREEN', 'DARK_GREEN', 'LIGHT_YELLOW', 'DARK_YELLOW'] as const;
export const STICKER_COLOR_HEX = ['#ad301a', '#d5dce8', '#86a87a', '#486d3b', '#f4c964', '#c99e10'] as const;
export const STICKER_COLOR_WITH_BABY = ['WHITE', 'LIGHT_GREEN', 'LIGHT_YELLOW'];

const THEME_COLOR = [
  'GRADIENT',
  'PRIMARY',
  'PRIMARY_LIGHT',
  'PRIMARY_DARK',
  'SECONDARY',
  'SECONDARY_LIGHT',
  'SECONDARY_DARK',
  'WHITE',
  'GREY',
  'GREY_LIGHT',
  'GREY_DARK',
  'BLACK',
] as const;
const STATUS_COLOR = [
  'INFO',
  'WARNING',
  'ERROR',
  'SUCCESS',
] as const;

export type StickerColor = typeof STICKER_COLOR[number];
export type StatusColor = typeof STATUS_COLOR[number];
export type ThemeColor = typeof THEME_COLOR[number];

export type AllColors = StickerColor | ThemeColor | StatusColor;

export const stickerHasBaby = (color: StickerColor) => STICKER_COLOR_WITH_BABY.includes(color);