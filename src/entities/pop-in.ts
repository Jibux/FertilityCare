export const POPIN = {
  NONE: '',
  EDIT_DAY: 'edit-day',
  LOGIN_DBX: 'login-dbx',
  LOGIN_WD: 'login-wd',
  SETTINGS: 'settings',
  DATA_MANAGE: 'data-manage',
  CYCLES: 'cycles',
  CONNECT: 'connect',
  LOGOUT: 'logout',
} as const;

export type Popin = ValueOf<typeof POPIN>;
