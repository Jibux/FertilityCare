export const DATE_FORMAT = {
  FR: 'fr', 
  EN: 'en',
} as const;

export const FIRST_DAYS_OF_WEEK = {
  SUNDAY: 0,
  MONDAY: 1,
} as const;

export type DateFormat = ValueOf<typeof DATE_FORMAT>;
export type FirstDaysOfWeek = ValueOf<typeof FIRST_DAYS_OF_WEEK>;