export const VIEW_TYPE = {
  // DAILY: 'daily',
  MONTHLY: 'monthly',
  CYCLE: 'cycle',
  DEFAULT: 'monthly',
} as const;

export type ViewType = ValueOf<typeof VIEW_TYPE>;
