import { IHashKeyString, isTruthy } from '../../utils/utils';
import { observationalDaysVersionTooNew, extractObservationalDays, IObservationalDays } from '../objects/observational-day';
import { dbxLogout, dbxFileExists, dbxGetFile, dbxUploadData } from './dropbox';
import { wdLogout, wdFileExists, wdGetFile, wdUploadData } from './webdav';
import { mergeData } from './merge_data';
import { SYNC_RESULT } from '../status';
import { getDateFromString, isCurrentYear } from 'utils/date';
import { Translation } from 'types/translations';

export const SYNC_PROVIDER = {
  DBX: 'Dropbox',
  WD: 'WebDAV',
  NONE: 'none',
} as const;

export type SyncProvider = ValueOf<typeof SYNC_PROVIDER>;

export const LOGIN_SYNC_METHOD = {
  FULL_SYNC: 'full_sync',
  PUSH: 'push',
  PULL: 'pull',
} as const;

export const SYNC_METHOD = {
  SYNC: 'sync',
  ...LOGIN_SYNC_METHOD,
} as const;

export type SyncMethod = ValueOf<typeof SYNC_METHOD>;
export type SyncMethodKeys = keyof typeof SYNC_METHOD;

export type SyncMethodDescription = {
  [key in SyncMethodKeys]: Translation;
};

export const SYNC_METHOD_DESCRIPTION: SyncMethodDescription = {
  SYNC: 'sync_method.sync',
  FULL_SYNC: 'sync_method.full_sync',
  PUSH: 'sync_method.push',
  PULL: 'sync_method.pull',
} as const;

export type ServerData = {
  serverData: IObservationalDays
};


export interface ILogin {
  [key: string]: any;
  /* Are we logged in or not */
  status: boolean;
  provider: SyncProvider;
  /* Misc data like 'auth token' */
  data: IHashKeyString;
}


const getFunctions = (login: IHashKeyString) => {
  if (login.provider === SYNC_PROVIDER.DBX) {
    return {
      fileExists: dbxFileExists,
      getFile: dbxGetFile,
      uploadData: dbxUploadData,
    };
  } else if (login.provider === SYNC_PROVIDER.WD) {
    return {
      fileExists: wdFileExists,
      getFile: wdGetFile,
      uploadData: wdUploadData,
    };
  } else {
    throw new Error(`${login.provider} sync provider not available`);
  }
};

export const resetLogin = (): ILogin => {
  // console.debug('RESET LOGIN');
  return {
    status: false,
    provider: SYNC_PROVIDER.NONE,
    data: {},
  };
};

export const logout = (login: IHashKeyString = {}) => {
  if (login.provider === SYNC_PROVIDER.DBX) {
    return dbxLogout(login.data);
  } else if (login.provider === SYNC_PROVIDER.WD) {
    return wdLogout();
  } else {
    throw new Error(`${login.provider} sync provider not available`);
  }
};

export const push = (login: IHashKeyString, observationalDays: IObservationalDays) => {
  return new Promise((resolve, reject) => {
    const f = getFunctions(login);
    f.uploadData(login.data, observationalDays)
      .then((_response: any) => {
        // console.debug(response);
        resolve(true);
      })
      .catch((error: any) => reject(error));
  });
};

export const pull = (login: IHashKeyString): Promise<ServerData> => {
  return new Promise((resolve, reject) => {
    const f = getFunctions(login);
    f.fileExists(login.data)
      .then((exists: boolean) => {
        if (isTruthy(exists)) {
          return f.getFile(login.data)
            /* "response" should be the file */
            .then((response: any) => {
              // console.debug(response);
              return response;
            })
            .catch((error: any) => reject(error));
        } else {
          console.debug('File not found');
          return {};
        }
      })
      .then((response) => resolve({ serverData: response }))
      .catch((error: any) => reject(error));
  });
};

function DataTooNewException() {
  this.response = { status: SYNC_RESULT.MUST_UPGRADE };
}

/* TODO: Think of a system with one json file per year to limit the network consumption */
export const sync = (login: IHashKeyString, observationalDays: IObservationalDays, years: number[], fullSync = false) => {
  return new Promise((resolve, reject) => {
    pull(login)
      .then((response: any) => {
        const dataFromServer = response.serverData;
        if (observationalDaysVersionTooNew(dataFromServer).length > 0) {
          throw new DataTooNewException();
        } else {
          return dataFromServer;
        }
      })
      .then((dataFromServer: any) => {
        const obsDaysToSync = extractObservationalDays(observationalDays, years);
        const dataToSync = extractObservationalDays(dataFromServer, years);
        const dataSyncedOutput = mergeData(obsDaysToSync, dataToSync);
        // console.debug(dataSyncedOutput);
        if (dataSyncedOutput.status === SYNC_RESULT.UPDATE_SERVER_DATA ||
          dataSyncedOutput.status === SYNC_RESULT.UPDATE_BOTH_DATA || fullSync) {
          const outputData = fullSync ? {
            server: { ...dataFromServer, ...observationalDays, ...dataSyncedOutput.data },
            local: { ...dataFromServer, ...observationalDays, ...dataSyncedOutput.data },
          } : {
            server: { ...dataFromServer, ...dataSyncedOutput.data },
            local: { ...observationalDays, ...dataSyncedOutput.data },
          };
          push(login, outputData.server)
            .then(() => {
              resolve({
                status: dataSyncedOutput.status,
                data: outputData.local,
              });
            });
        } else {
          resolve({
            status: dataSyncedOutput.status,
            data: { ...observationalDays, ...dataSyncedOutput.data },
          });
        }
      })
      .catch((error: any) => reject(error));
  });
};

export const getYearsToSync = (str: string) => {
  const date = getDateFromString(str);
  const year = date.getFullYear();
  const years = [year - 1, year];
  return (isCurrentYear(date)) ? years : [...years, year + 1];
};
