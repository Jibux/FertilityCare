import { IObservationalDay, IObservationalDays } from '../objects/observational-day';
import { SYNC_RESULT } from '../status';
import { isDefined, IHashKeyString } from '../../utils/utils';

const isDataKeyUsable = (data: IHashKeyString, key: string) =>
  (isDefined(data) && isDefined(data[key]));
const isDataUsable = (data: IObservationalDay) => isDataKeyUsable(data, 'modificationDate');
const isResultUsable = (result: IHashKeyString) =>
  (isDataKeyUsable(result, 'status') && result.status !== SYNC_RESULT.KO);

const handleSyncStatus = (status: number, lastStatus: number) => status | lastStatus;

const getLatestData = (data1: IObservationalDay, data2: IObservationalDay) => {
  if (isDataUsable(data1)) {
    if (isDataUsable(data2)) {
      /* Both data can be compared */
      if (data2.modificationDate > data1.modificationDate) {
        return { status: SYNC_RESULT.UPDATE_CLIENT_DATA, data: data2 };
      } else if (data2.modificationDate !== data1.modificationDate) {
        return { status: SYNC_RESULT.UPDATE_SERVER_DATA, data: data1 };
      } else {
        /* No modification here */
        return { status: SYNC_RESULT.NO_CHANGE, data: data1 };
      }
    } else {
      /* data1 is usable and data2 is not usable */
      return { status: SYNC_RESULT.UPDATE_SERVER_DATA, data: data1 };
    }
  } else if (isDataUsable(data2)) {
    /* data1 is not usable and data2 is usable */
    return { status: SYNC_RESULT.UPDATE_CLIENT_DATA, data: data2 };
  } else {
    /* Both data are not usable */
    return { status: SYNC_RESULT.KO, data: null };
  }
};

/* We have to replace {data} by key */
const getLatestDataByKey = (
  data1: IObservationalDays, data2: IObservationalDays) =>
  (key: any) => ({ ...getLatestData(data1[key], data2[key]), key });

/* We will use this function to merge data1 and data2 */
export const mergeData = (data1: IObservationalDays, data2: IObservationalDays) => {
  const dataKeyNames = Object.keys(data1).concat(Object.keys(data2));

  const getLatestDataByKeyResult = dataKeyNames
    .map(getLatestDataByKey(data1, data2))
    .filter(isResultUsable);

  const data3 = getLatestDataByKeyResult
    .reduce(
      (acc, { data, key }) => ({ ...acc, [key]: data }),
      {},
    );

  const finalStatus = getLatestDataByKeyResult
    .reduce(
      (acc, { status }) => handleSyncStatus(acc, status),
      0,
    );

  /*
    updateStatus:
    - 0: no change at all
    - 1: data2 has been updated
    - 2: data1 has been updated
    - 3: data1 and data2 have been updated
  */
  return { status: finalStatus, data: data3 };
};
