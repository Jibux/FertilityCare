import { IHashKeyString, isDefined } from '../../utils/utils';
import { FC_JSON_FILE } from './config';
import { postData } from '../../utils/rest_api_client';
import { IObservationalDays } from '../objects/observational-day';
import { newBlob } from '../../utils/files';
import { sentenceCase } from '../../utils/string';
import qs from 'qs';
import { timestamp } from 'utils/date';

export const DBX_APP_KEY = 'vq4jw0ewpec3dxe';
export const DBX_APP_SECRET = '3z17h253yjjzodi';

export const DBX_URLS = {
  LOGIN: 'https://www.dropbox.com/oauth2/authorize',
  TOKEN: 'https://api.dropboxapi.com/oauth2/token',
  LIST: 'https://api.dropboxapi.com/2/files/list_folder',
  SEARCH: 'https://api.dropboxapi.com/2/files/search_v2',
  LOGOUT: 'https://api.dropboxapi.com/2/auth/token/revoke',
  DOWNLOAD: 'https://content.dropboxapi.com/2/files/download',
  UPLOAD: 'https://content.dropboxapi.com/2/files/upload',
};

export const DBX_PATH = '/Apps/FertilityCare';
export const DBX_FC_JSON_FILE = DBX_PATH + '/' + FC_JSON_FILE;

const EXPIRES_OFFSET = 300; // Be sure we refresh the token in time

const commonHeaders = (loginData: IHashKeyString, contentType = 'application/json') => ({
  'Content-Type': contentType,
  'Authorization': `${sentenceCase(loginData.tokenType)} ${loginData.accessToken}`,
});

/*{
  ".tag": "file",
  "name": "FertilityCareData.json",
  "path_lower": "/fertilitycaredata.json",
  "path_display": "/FertilityCareData.json",
  "id": "id:VlQFiFyMf6UAAAAAAAAefw",
  "client_modified": "2021-02-11T22:13:25Z",
  "server_modified": "2021-02-11T22:13:25Z",
  "rev": "015bb16d417864200000001855aa370",
  "size": 71840,
  "is_downloadable": true,
  "content_hash": "f353eb9e6e85b4a09f2012564d5488925733da86dc883174e2d95433a111643b"
}*/

interface IDBXFile {
  '.tag': string;
  name: string;
  path_lower: string;
  path_display: string;
  id: string;
  client_modified: string;
  server_modified: string;
  rev: string;
  size: number;
  is_downloadable: boolean;
  content_hash: string;
}

const isFcfile = (fileObject: IDBXFile) => fileObject.path_display === '/' + FC_JSON_FILE;

const dbxFindFile = (fileObjectList: IDBXFile[]) =>
  fileObjectList.find(isFcfile);

export const dbxLogin = () => {
  /* TODO: Maybe redirect to current url instead of window.location.origin (but should setup Dropbox API accordingly) */
  // tslint:disable-next-line:max-line-length
  const fullUrl = `${DBX_URLS.LOGIN}?client_id=${DBX_APP_KEY}&response_type=code&token_access_type=offline&redirect_uri=${window.location.origin}/?popin=login-dbx`;
  console.log(`Redirecting to ${fullUrl}`);
  window.location = fullUrl as any;
};

export const dbxRequestRefreshToken = (code: string): Promise<any> =>
  new Promise((resolve, reject) => {
    postData(DBX_URLS.TOKEN, qs.stringify({
      code: code, grant_type: 'authorization_code', client_id: DBX_APP_KEY, client_secret: DBX_APP_SECRET,
      redirect_uri: `${window.location.origin}/?popin=login-dbx`,
    }),
    { 'Content-Type': 'application/x-www-form-urlencoded' })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => reject(error));
  });

export const dbxRefreshToken = (refresh_token: string): Promise<any> =>
  new Promise((resolve, reject) => {
    postData(DBX_URLS.TOKEN,
      qs.stringify({
        refresh_token: refresh_token, grant_type: 'refresh_token', client_id: DBX_APP_KEY, client_secret: DBX_APP_SECRET,
      }),
      { 'Content-Type': 'application/x-www-form-urlencoded' })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => reject(error));
  });

export const dbxListFiles = (loginData: IHashKeyString) =>
  postData(DBX_URLS.LIST, { path: '' }, commonHeaders(loginData));

export const dbxSearchFile = (loginData: IHashKeyString) =>
  postData(DBX_URLS.SEARCH, {
    query: FC_JSON_FILE,
    options: {
      path: '',
      filename_only: true,
      file_extensions: ['json'],
    },
  },
  commonHeaders(loginData));

/* dbxSearchFile does not work as expected */
export const dbxFileExists = (loginData: IHashKeyString) =>
  dbxListFiles(loginData)
    .then((response) =>
      isDefined(response.data.entries)
        ? dbxFindFile(response.data.entries)
        : false,
    );

export const dbxLogout = (loginData: IHashKeyString) =>
  postData(DBX_URLS.LOGOUT, 'null', commonHeaders(loginData));

export const dbxGetFile = (loginData: IHashKeyString) =>
  new Promise((resolve, reject) => {
    postData(DBX_URLS.DOWNLOAD, '', {
      'Dropbox-API-Arg': JSON.stringify({ path: '/' + FC_JSON_FILE }),
      ...commonHeaders(loginData, 'text/plain'),
    })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => reject(error));
  });

export const dbxPutFile = (loginData: IHashKeyString, data: any, type: string) =>
  postData(DBX_URLS.UPLOAD, data, {
    'Dropbox-API-Arg': JSON.stringify({ path: '/' + FC_JSON_FILE, mode: 'overwrite' }),
    ...commonHeaders(loginData, type),
  });

export const dbxUploadData = (loginData: IHashKeyString, data: IObservationalDays) => {
  const type = 'application/octet-stream';
  const blob = newBlob(JSON.stringify(data), type);
  return dbxPutFile(loginData, blob, type);
};

export const dbxGetExpiresIn = (expiresIn: string) =>
  timestamp() + parseInt(expiresIn) - EXPIRES_OFFSET;
