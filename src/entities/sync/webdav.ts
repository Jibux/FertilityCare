import { IHashKeyString, isDefined, isNotEmpty } from '../../utils/utils';
import { FC_JSON_FILE } from './config';
import { IObservationalDays } from '../objects/observational-day';
import { newBlob } from '../../utils/files';
import { HTTP_STATUS } from '../status';
import { without, find, propEq } from 'ramda';

// tslint:disable-next-line:no-var-requires
const { createClient } = require('webdav');

export const WD_DEFAULT_PATH = '/Apps/FertilityCare';


export const wdLogin = (data: IHashKeyString) =>
  createClient(
    data.server,
    {
      username: data.username,
      password: data.password,
    },
  );

export const wdLogout = (): Promise<any> =>
  new Promise((resolve) => resolve(true));

export const wdGetDirectoryContents = (client: any, directory: string): Promise<any> =>
  client.getDirectoryContents(directory);

export const wdCreateDirectory = (client: any, directory: string): Promise<any> =>
  client.createDirectory(directory);

export const wdStat = (client: any, directory: string): Promise<any> =>
  client.stat(directory);

export const wdTestAndCreateDirectory = (client: any, directory: string): Promise<any> =>
  new Promise((resolve, reject) => {
    wdStat(client, directory)
      .then((response) => {
        // console.debug(response);
        if (response.type === 'directory') {
          console.debug(`Directory "${directory}" exists`);
          resolve(true);
        } else {
          reject(`File "${directory}" exists, but is not a directory!`);
        }
      })
      .catch((error) => {
        if (isDefined(error.response)) {
          if (error.response.status === HTTP_STATUS.NOT_FOUND) {
            console.debug(`Creating directory "${directory}"`);
            wdCreateDirectory(client, directory)
              .then(() => resolve(true))
              .catch(() => reject(error));
          } else {
            reject(error);
          }
        } else {
          reject(error);
        }
      });
  });

export const wdCreateDirectories = (client: any, directories: string[]): Promise<any> =>
  new Promise((resolve, reject) => {
    if (directories.length === 0) {
      resolve(true);
    } else {
      const directory = directories[0];
      wdTestAndCreateDirectory(client, directory)
        .then(() => {
          wdCreateDirectories(client, without([directory], directories))
            .then(() => resolve(true))
            .catch((error) => reject(error));
        },
        )
        .catch((error) => reject(error));
    }
  });

/*export const wdTestConnection = (client: any): Promise<any> =>
  wdGetDirectoryContents(client, '/');*/

export const wdInitPath = (credentials: IHashKeyString, path: string): Promise<any> => {
  const client = wdLogin(credentials);
  const directories = path.split('/')
    .filter(isNotEmpty)
    .reduce((acc: string[], name: string) => {
      const index = acc.length - 1;
      const element = (index > 0) ? acc[index] + '/' : acc[index];
      return acc.concat([element + name]);
    }, ['/']);
  // console.debug(directories);
  return wdCreateDirectories(client, directories);
};

export const wdSearchFile = (loginData: IHashKeyString): Promise<any> =>
  new Promise((resolve, reject) => {
    const client = wdLogin(loginData.credentials);
    wdGetDirectoryContents(client, loginData.directory)
      .then((response) => {
        // console.debug(response);
        resolve(find(propEq('basename', FC_JSON_FILE))(response));
      })
      .catch((error) => reject(error));
  });

export const wdFileExists = (loginData: IHashKeyString): Promise<any> =>
  wdSearchFile(loginData)
    .then((response) => {
      // console.debug(response);
      return isDefined(response);
    });

export const wdGetFileName = (directory: IHashKeyString) =>
  directory + '/' + FC_JSON_FILE;

export const wdGetFileContent = (client: any, file: string): Promise<any> =>
  client.getFileContents(file, { format: 'text' });

export const wdGetFile = (loginData: IHashKeyString): Promise<any> =>
  new Promise((resolve, reject) => {
    const client = wdLogin(loginData.credentials);
    wdGetFileContent(client, wdGetFileName(loginData.directory))
      .then((response) => {
        resolve(response);
      })
      .catch((error) => reject(error));
  });

export const wdPutFile = (client: any, data: any, directory: string): Promise<any> =>
  client.putFileContents(directory + '/' + FC_JSON_FILE, data);

export const wdUploadData = (loginData: IHashKeyString, data: IObservationalDays): Promise<any> => {
  const type = 'application/json';
  const blob = newBlob(JSON.stringify(data), type);
  const client = wdLogin(loginData.credentials);
  return wdPutFile(client, blob, loginData.directory);
};
