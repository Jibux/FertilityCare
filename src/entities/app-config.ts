import { DateFormat, DATE_FORMAT, FirstDaysOfWeek, FIRST_DAYS_OF_WEEK } from './locales';
import { LanguageChoice, LANGUAGE_CHOICE } from './languages';

export interface IConfig {
  lang: LanguageChoice;
  dateFormat: DateFormat;
  firstDayOfWeek: FirstDaysOfWeek;
  correctionMode: boolean;
  devMode: boolean;
}

export const defaultConfig = (): IConfig => {
  const lang = LANGUAGE_CHOICE.BROWSER;
  return {
    lang: lang,
    dateFormat: DATE_FORMAT.FR,
    firstDayOfWeek: FIRST_DAYS_OF_WEEK.MONDAY,
    correctionMode: false,
    devMode: false,
  };
};
