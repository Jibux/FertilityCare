import { createVersionedObservationalDay, IObservationalDay, IObservationalDays, updateModificationDate } from 'entities/objects/observational-day';
import { getRealFullDayString, today } from 'utils/date';
import { filterMax, getRandomInt } from 'utils/utils';
import { filterPositiveInteger } from 'utils/utils';

export const DEFAULT_NR_OF_CYCLES_TO_GENERATE = Math.floor(365 * 1 / 28);
export const MAX_NR_OF_CYCLES_TO_GENERATE = Math.floor(365 * 45 / 28);

const nonFertileDay = (): IObservationalDay =>
  createVersionedObservationalDay({
    sticker: 3,
    mucus: 3,
    frequency: getRandomInt(0, 3),
    misc: getRandomInt(0, 1) === 1 ? [0] : [],
  });

const nonFertileDay2 = (): IObservationalDay =>
  createVersionedObservationalDay({
    sticker: 3,
    mucus: getRandomInt(7, 9),
    frequency: getRandomInt(0, 3),
  });


const postPicDay = (): IObservationalDay =>
  createVersionedObservationalDay({ ...nonFertileDay(), sticker: 2, misc: [] });

const fertileDay = (): IObservationalDay =>
  createVersionedObservationalDay({
    sticker: 1,
    mucus: getRandomInt(4, 6),
    mucusExtended: [0, 1, 2],
    frequency: getRandomInt(0, 3),
  });

const picDay = (): IObservationalDay =>
  createVersionedObservationalDay({ ...fertileDay(), pic: 0 });

const periodDay = (): IObservationalDay =>
  createVersionedObservationalDay({
    sticker: 0,
    period: getRandomInt(0, 4),
  });

const getDataDay = (fun: Function) =>
  updateModificationDate(fun());

const addNewDataDay = (date: Date, obsDays: IObservationalDays, fun: Function) =>
  obsDays[getRealFullDayString(date)] = getDataDay(fun);

const addNewDataDays = (date: Date, obsDays: IObservationalDays, fun: Function, maxCount: number) => {
  for (let count = 0; count < maxCount; count++) {
    addNewDataDay(date, obsDays, fun);
    date.setDate(date.getDate() - 1);
  }
  return date;
};

export const generateData = (numberOfCycles: number) => {
  const obsDays: IObservationalDays = {};
  const n = filterMax(filterPositiveInteger(numberOfCycles, DEFAULT_NR_OF_CYCLES_TO_GENERATE), MAX_NR_OF_CYCLES_TO_GENERATE);
  let date = today();

  for (let count = 0; count < n; count++) {
    date = addNewDataDays(date, obsDays, nonFertileDay, getRandomInt(10, 12));
    date = addNewDataDays(date, obsDays, postPicDay, 3);
    date = addNewDataDays(date, obsDays, picDay, 1);
    date = addNewDataDays(date, obsDays, fertileDay, getRandomInt(5, 7));
    date = addNewDataDays(date, obsDays, nonFertileDay2, getRandomInt(2, 3));
    date = addNewDataDays(date, obsDays, periodDay, getRandomInt(5, 7));
  }

  return obsDays;
};
