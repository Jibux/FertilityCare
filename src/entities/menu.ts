export const MENU = {
  NONE: '',
  EDIT_DAY: 'edit-day',
  LOGIN_DBX: 'login-dbx',
  LOGIN_WD: 'login-wd',
  SETTINGS: 'settings',
  DATA_MANAGE: 'data-manage',
  CYCLES: 'cycles',
  CONNECT: 'connect',
  LOGOUT: 'logout',
  SYNC: 'SYNC',
} as const;

export type Menu = ValueOf<typeof MENU>;