export type User = {
  id: string;
  email: string;
  pseudo: string;
  firstname?: string;
  lastname?: string;
  avatar?: string;
  syncronizationType?: 'dropbox' | 'webdav';
};