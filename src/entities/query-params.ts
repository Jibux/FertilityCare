import { ViewType } from './view-type';
import { Popin } from './pop-in';

export type QueryParams = {
  view: ViewType,
  popin: Popin,
  date: string,
  code: string,
};