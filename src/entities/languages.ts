export const LANGUAGES = {
  FR: 'fr',
  EN: 'en',
} as const;

export const LANGUAGE_CHOICE = {
  ...LANGUAGES,
  BROWSER: 'browser',
} as const;

export const LANGUAGES_DESC = {
  FR: 'language.french',
  EN: 'language.english',
  BROWSER: 'language.browser',
} as const;

export type Language = ValueOf<typeof LANGUAGES>;
export type LanguageChoice = ValueOf<typeof LANGUAGE_CHOICE>;
