import { Translation } from 'types/translations';

export const NOTIF_STATUS = {
  NONE: '',
  OK: 'OK',
  KO: 'KO',
  MIGRATE: 'MIGRATE',
} as const;

export const TIMEOUT_RESET_NOTIF = 3000;
export const TIMEOUT_RESET_NOTIF_LONG = 5000;

export type NotifStatus = ValueOf<typeof NOTIF_STATUS>;
export type NotifMsg = { notifStatus: NotifStatus, title?: Translation, message?: Translation, timeout?: number };

export const NOTIF_MSG_DEFAULT: NotifMsg = { notifStatus: NOTIF_STATUS.NONE, title: 'notification.error' };