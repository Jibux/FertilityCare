import {
  curry,
} from 'ramda';

import { KEYS_ACRONYM, KEYS_DATA, KEYS_INFO, ODKey, ODValue, OD_MIN_VERSION, OD_MAX_VERSION } from '../observational-data/observational-data';
import {
  isMultiple, isExtendable, linkedToKey, isKeyDefined, validateKey,
  getAcronymList, doesObjectKeyContainData, doesObjectKeyContainThatValue, getDescriptionList, OD_MIGRATIONS,
} from '../object-helpers/observational-data';
import {
  isDefined, setProperty, removeProperty,
} from '../../utils/utils';
import { getRealFullDayString, today } from '../../utils/date';
import { removeArrayProperty, setArrayProperty } from 'utils/array';
import { Optional } from 'types/optional';
import { StickerColor, STICKER_COLOR } from 'entities/color';

export type ObservationalDayKey = ODKey | 'creationDate' | 'modificationDate' | 'oldObservationalDay' | 'v';

export interface IObservationalDay {
  sticker?: number;
  period?: number;
  mucus?: number;
  mucusExtended?: number[];
  frequency?: number;
  misc?: number[];
  pic?: number;
  similarity?: number;
  comment?: string;
  oldObservationalDay?: IObservationalDay;
  creationDate: number;
  modificationDate: number;
  v?: number;
}

/* Verify keys of IObservationalDay */
type KeysMissingFromIObservationalDay = Exclude<ObservationalDayKey, keyof IObservationalDay>;
type ExtraKeysInIObservationalDay = {
  [K in keyof IObservationalDay]: Extract<ObservationalDayKey, K> extends never ? K : never
}[keyof IObservationalDay];
export type VerifyIObservationalDay<
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Missing extends never = KeysMissingFromIObservationalDay,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Extra extends never = ExtraKeysInIObservationalDay,
> = 0;

/* Create IObservationalDay with optional properties */
type IObservationalDayOptional = Optional<IObservationalDay>;

export interface ICycleDay {
  [index: string]: IObservationalDay | Date;
  observationalDay: IObservationalDay;
  date: Date;
}

export interface IObservationalDays { [key: string]: IObservationalDay; }

export const observationalDayDefaults: IObservationalDay = {
  creationDate: 0,
  modificationDate: 0,
};

export const createObservationalDay = (initialValues: IObservationalDayOptional = null): IObservationalDay => (
  isDefined(initialValues) ?
    /* initialValues may have version */
    {
      ...observationalDayDefaults,
      ...initialValues,
    }
    /* New observationalDay must have latest version */
    : { ...observationalDayDefaults, v: OD_MAX_VERSION }
);

export const createVersionedObservationalDay = (initialValues: IObservationalDayOptional): IObservationalDay => (
  { ...createObservationalDay(), ...initialValues }
);

export const removeFromObservationalDay =
  (key: ODKey) => (value: number = undefined) => (observationalDay: IObservationalDay): IObservationalDay =>
    (isKeyDefined(key))
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      ? triggerRemove(key)(
        (isMultiple(key) && isDefined(value))
          /*
            If the key is multiple and we want to remove a specific value from it
            We do not care about the existence of 'value'
          */
          ? removeArrayProperty(key, value)(observationalDay)
          /* If it is a simple key to remove */
          : removeProperty(key)(observationalDay))
      : observationalDay;

/* When removing a key linked to one another, remove the linked key too (ex. remove mucus => remove mucusExtended) */
const triggerRemove =
  curry((key: ODKey, observationalDay: IObservationalDay): IObservationalDay =>
    (linkedToKey(key))
      ? removeFromObservationalDay(linkedToKey(key))()(observationalDay)
      : observationalDay,
  );

/* When changing a key linked to one another and the new value of this key is not extendable,
remove the linked key too (ex. change from mucus = 4 to mucus = 1 => remove mucusExtended) */
const triggerAdd =
  curry((key: ODKey, value: ODValue, observationalDay: IObservationalDay): IObservationalDay =>
    (linkedToKey(key))
      ? isExtendable([key, value])
        ? observationalDay
        : removeFromObservationalDay(linkedToKey(key))()(observationalDay)
      : observationalDay,
  );

/*
  Returns if the observationalDay property contains some data
  != empty [] for a multiple one
*/
export const doesObservationalDayKeyContainData = (key: ODKey, observationalDay: IObservationalDay) =>
  doesObjectKeyContainData(key, observationalDay);

export const doesObservationalDayContainData = (observationalDay: IObservationalDay) =>
  Object.keys(KEYS_DATA).reduce((acc, key: ODKey) => acc || doesObjectKeyContainData(key, observationalDay), false);

export const addValueMultiple = curry(
  (key: ODKey, value: ODValue, observationalDay: IObservationalDay): IObservationalDay =>
    setArrayProperty(key, value)(
      {
        type: KEYS_INFO[key].valueType,
        sorted: true,
      },
    )(observationalDay),
);

export const addToObservationalDay = curry(
  (key: ODKey, value: ODValue, observationalDay: IObservationalDay): IObservationalDay =>
    validateKey(key, value)
      ? triggerAdd(key, value)(
        (isMultiple(key))
          ? addValueMultiple(key, value)(observationalDay)
          : setProperty(key, value)(observationalDay))
      : observationalDay,
);

const updateCreationDate = (observationalDay: IObservationalDay): IObservationalDay =>
  (observationalDay.creationDate === 0)
    ? setProperty('creationDate')(observationalDay.modificationDate)(observationalDay)
    : observationalDay;

export const updateModificationDate = (observationalDay: IObservationalDay): IObservationalDay =>
  updateCreationDate(setProperty('modificationDate')(today().getTime())(observationalDay));

export const isValueInObservationalDayData = (key: ODKey, value: ODValue, observationalDay: IObservationalDay) =>
  doesObjectKeyContainThatValue(key, value, observationalDay);

export const isACorrectedDay = (observationalDay: IObservationalDay) =>
  doesObservationalDayContainData(observationalDay) &&
  isDefined(observationalDay.oldObservationalDay) &&
  doesObservationalDayContainData(observationalDay.oldObservationalDay);

export const setOldObservationalDay = (observationalDay: IObservationalDay) =>
  !doesObservationalDayContainData(observationalDay) || isACorrectedDay(observationalDay)
    ? observationalDay
    : { ...observationalDay, oldObservationalDay: observationalDay };

export const isAStickerDay = (observationalDay: IObservationalDay) =>
  doesObjectKeyContainData('sticker', observationalDay);

export const getStickerRealColor = (observationalDay: IObservationalDay): StickerColor =>
  isDefined(STICKER_COLOR[observationalDay.sticker])
    ? STICKER_COLOR[observationalDay.sticker]
    : 'WHITE';

export const getStickerColor = (observationalDay: IObservationalDay) =>
  isAStickerDay(observationalDay)
    ? getStickerRealColor(observationalDay)
    : '';

export const isAPeriodDay = (observationalDay: IObservationalDay) =>
  doesObjectKeyContainData('period', observationalDay);

export const isAMucusDay = (observationalDay: IObservationalDay) =>
  doesObjectKeyContainData('mucus', observationalDay);

export const isAFrequencyDay = (observationalDay: IObservationalDay) =>
  doesObjectKeyContainData('frequency', observationalDay);

export const isAMiscDay = (observationalDay: IObservationalDay) =>
  doesObjectKeyContainData('misc', observationalDay);

export const isAPicDay = (observationalDay: IObservationalDay) =>
  doesObjectKeyContainData('pic', observationalDay) && isValueInObservationalDayData('pic', 0, observationalDay);

export const isASimilarityDay = (observationalDay: IObservationalDay) =>
  doesObjectKeyContainData('similarity', observationalDay);

export const isACommentDay = (observationalDay: IObservationalDay) =>
  doesObjectKeyContainData('comment', observationalDay);

export const validateObservationalDay = (observationalDay: IObservationalDay) =>
  isAPeriodDay(observationalDay) || isAMucusDay(observationalDay);

export const getComment = (observationalDay: IObservationalDay) =>
  isACommentDay(observationalDay) ? observationalDay.comment : '';

export const getDataPeriodSummary = (observationalDay: IObservationalDay) =>
  getAcronymList(['period'], observationalDay);

/* Will return list of acronyms for general data */
export const getDataMucusSummary = (observationalDay: IObservationalDay) =>
  getAcronymList(['mucus', 'mucusExtended'], observationalDay);

export const getDataFrequencySummary = (observationalDay: IObservationalDay) =>
  getAcronymList(['frequency'], observationalDay);

/* Will return list of acronyms for misc data */
export const getMiscDataSummary = (observationalDay: IObservationalDay) =>
  getAcronymList(['misc'], observationalDay);

/* Will return list of acronyms for pic data */
export const getPicDataSummary = (observationalDay: IObservationalDay) =>
  getAcronymList(['pic'], observationalDay);

/* Will return list of acronyms for essential similarity data */
export const getDataSimilaritySummary = (observationalDay: IObservationalDay) =>
  getAcronymList(['similarity'], observationalDay);

export const getDataMucusDescription = (observationalDay: IObservationalDay) =>
  getDescriptionList(['mucus', 'mucusExtended'], observationalDay);

export const getDataPeriodDescription = (observationalDay: IObservationalDay) =>
  getDescriptionList(['period'], observationalDay);

export const getDataFrequencyDescription = (observationalDay: IObservationalDay) =>
  getDescriptionList(['frequency'], observationalDay);

export const getDataMiscDescription = (observationalDay: IObservationalDay) =>
  getDescriptionList(['misc'], observationalDay);

export const getDataPicDescription = (observationalDay: IObservationalDay) =>
  getDescriptionList(['pic'], observationalDay);

export const getDataSimilarityDescription = (observationalDay: IObservationalDay) =>
  getDescriptionList(['similarity'], observationalDay);

export const isValueInMiscData = (value: ODValue, observationalDay: IObservationalDay) =>
  isValueInObservationalDayData('misc', value, observationalDay);

export const isAnIntercourseDay = (observationalDay: IObservationalDay) =>
  isValueInMiscData(0, observationalDay);

export const isAnAppointmentDay = (observationalDay: IObservationalDay) =>
  isValueInMiscData(1, observationalDay);

export const getIntercourseValue = (observationalDay: IObservationalDay) =>
  isAnIntercourseDay(observationalDay) ? KEYS_ACRONYM.MISC_I : '';

export const getDataSummaryToString = (observationalDay: IObservationalDay, lineBreak = '\n'): string =>
  getDataPeriodSummary(observationalDay)
    .join('')
    .concat(getDataMucusSummary(observationalDay).join(''))
    .concat(lineBreak)
    .concat(getDataFrequencySummary(observationalDay).join(''))
    .concat(lineBreak)
    .concat(getDataSimilaritySummary(observationalDay).join(''));

export const getObservationalDaysRange = (observationalDays: IObservationalDays, dateRange: Date[]): ICycleDay[] =>
  dateRange
    .map((date) => ({ dateString: getRealFullDayString(date), date }))
    .map(({ dateString, date }) =>
      (isDefined(observationalDays[dateString]))
        ? { observationalDay: observationalDays[dateString], date }
        : { observationalDay: createObservationalDay(), date });

export const extractObservationalDays = (observationalDays: IObservationalDays, years: number[]): IObservationalDays =>
  Object.keys(observationalDays).reduce((stack, key) => {
    for (let year of years) {
      if (key.startsWith(year.toString())) {
        return { [key]: observationalDays[key], ...stack };
      }
    }
    return stack;
  }, {});

export const extractObservationalDaysBefore = (observationalDays: IObservationalDays, day: string): IObservationalDays =>
  Object.keys(observationalDays).reduce((stack, key) => {
    if (key <= day) {
      return { [key]: observationalDays[key], ...stack };
    }
    return stack;
  }, {});

export const getODVersion = (observationalDay: IObservationalDay): number =>
  isDefined(observationalDay.v) ? observationalDay.v : OD_MIN_VERSION;

export const migrateObservationalDay = (observationalDay: IObservationalDay): IObservationalDay => {
  const v = getODVersion(observationalDay);
  const nextV = v + 1;
  const migratedObsDay = (v < OD_MAX_VERSION) ?
    migrateObservationalDay({ ...OD_MIGRATIONS[nextV](observationalDay), v: nextV })
    : observationalDay;
  return isACorrectedDay(migratedObsDay)
    ? { ...migratedObsDay, oldObservationalDay: migrateObservationalDay(migratedObsDay.oldObservationalDay) }
    : migratedObsDay;
};

export const observationalDaysVersionTooNew = (observationalDays: IObservationalDays): string[] =>
  Object.keys(observationalDays).filter((key) => getODVersion(observationalDays[key]) > OD_MAX_VERSION &&
    (!isACorrectedDay(observationalDays[key]) || getODVersion(observationalDays[key].oldObservationalDay) > OD_MAX_VERSION));

export const getObservationalDayKeysToMigrate = (observationalDays: IObservationalDays): string[] =>
  Object.keys(observationalDays).filter((key) => getODVersion(observationalDays[key]) < OD_MAX_VERSION &&
    (!isACorrectedDay(observationalDays[key]) || getODVersion(observationalDays[key].oldObservationalDay) < OD_MAX_VERSION));

export const checkObservationalDaysVersion = (observationalDays: IObservationalDays): boolean =>
  getObservationalDayKeysToMigrate(observationalDays).length === 0;

export const migrateObservationalDays = (observationalDays: IObservationalDays): IObservationalDays => ({
  ...observationalDays,
  ...getObservationalDayKeysToMigrate(observationalDays).reduce((stack, key) => {
    return { ...stack, [key]: migrateObservationalDay(observationalDays[key]) };
  }, {}),
});
