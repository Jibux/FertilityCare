import {
  pipe, always,
} from 'ramda';

import { KEYS_INFO, KEY_CONTENT_TYPE, KEYS_DATA, STICKER_COLOR_WITH_BABY_IDX, ODKey, ODValue, ODMigrations } from '../observational-data/observational-data';
import {
  isDefined, getDeepProperty, getProperty,
  IHashKeyString, getKeyOfHashMatchingValue,
} from '../../utils/utils';
import { STICKER_COLOR_HEX } from '../color';
import { isEmptyArray, isValueInArray } from 'utils/array';

/*
  - multiple: we can have multiple values for that key
  - linkedToKey: if a key can be related to one another
  - extendable:
    . if the key has a value extendable, then we can add values for the related 'linkedToKey'
    . if not, the 'linkedToKey' key should not be modified
*/
export const isMultiple = (key: ODKey) =>
  KEYS_INFO[key].multiple;

export const isExtendable = (keys: Array<ODValue>) =>
  pipe(
    always(KEYS_DATA),
    getDeepProperty([...keys, 'extendable']),
    isDefined,
  )();

export const hasAcronym = (keys: Array<ODValue>) =>
  pipe(
    always(KEYS_DATA),
    getDeepProperty([...keys, 'acronym']),
    isDefined,
  )();

export const hasDescription = (keys: Array<ODValue>) =>
  pipe(
    always(KEYS_DATA),
    getDeepProperty([...keys, 'description']),
    isDefined,
  )();

export const linkedToKey = (key: ODKey) =>
  KEYS_INFO[key].linkedToKey;

const isKeyAnArray = (key: ODKey) =>
  KEYS_INFO[key].keyContentType === KEY_CONTENT_TYPE.ARRAY;

/*
  'KEYS_DATA' contains ARRAY of possible keys, but for keys like 'comment',
  there is no reference into 'KEYS_DATA' because they just contain a SINGLE_VALUE
  value defaults to '0' as all hash keys has it
*/
export const isKeyDefined = (key: ODKey, value: ODValue = 0) =>
  isDefined(KEYS_INFO[key]) &&
  (!isKeyAnArray(key) || ((value >= 0) &&
    isDefined(KEYS_DATA[key][value])));

export const isValueCorrectType = (key: ODKey, value: ODValue) =>
  KEYS_INFO[key].valueType === typeof (value);

export const validateKey = (key: ODKey, value: ODValue) =>
  isKeyDefined(key, value) && isValueCorrectType(key, value);

export const doesObjectContainKey = (key: ODKey, object: IHashKeyString) =>
  isDefined(object[key]);

export const doesObjectKeyContainData = (key: ODKey, object: IHashKeyString) =>
  doesObjectContainKey(key, object) && !(isMultiple(key) && isEmptyArray(object[key]));

export const doesObjectKeyContainThatValue = (key: ODKey, value: ODValue, object: IHashKeyString) =>
  (isDefined(object[key]) && isKeyDefined(key, value))
    ? (isMultiple(key))
      ? isValueInArray(getProperty(key, object), value)
      : getProperty(key, object) === value
    : false;

export const getKeyAcronym = (key: ODKey, value: ODValue) =>
  hasAcronym([key, value])
    ? KEYS_DATA[key][value].acronym
    : '';

export const getKeyDescription = (key: ODKey, value: ODValue) =>
  hasDescription([key, value])
    ? KEYS_DATA[key][value].description
    : '';

export const getAcronymList = (keys: ODKey[], object: IHashKeyString) =>
  keys.reduce((list: string[], key: ODKey) => {
    if (doesObjectKeyContainData(key, object)) {
      const value = object[key];
      const acronyms = (isMultiple(key))
        ? value.map((tmpValue: ODValue) => getKeyAcronym(key, tmpValue))
        : [getKeyAcronym(key, value)];
      return [...list, ...acronyms].filter((el) => el !== '');
    } else {
      return list;
    }
  }, []);

export const getDescriptionList = (keys: ODKey[], object: IHashKeyString) =>
  keys.reduce((list: string[], key: ODKey) => {
    if (doesObjectKeyContainData(key, object)) {
      const value = object[key];
      const description = (isMultiple(key))
        ? value.map((tmpValue: ODValue) => getKeyDescription(key, tmpValue))
        : [getKeyDescription(key, value)];
      return [...list, ...description].filter((el) => el !== '');
    } else {
      return list;
    }
  }, []);

export const isFilledWithBabyPicture = (stickerColor: string) => {
  const key = getKeyOfHashMatchingValue(stickerColor, STICKER_COLOR_HEX);
  return isDefined(key)
    ? isValueInArray(STICKER_COLOR_WITH_BABY_IDX, parseInt(key as string, 10))
    : false;
};

export const OD_MIGRATIONS : ODMigrations = {
  0: (o: IHashKeyString) => o,
  1: (o: IHashKeyString) => o,
  2: (o: IHashKeyString) => {
    const pic = (doesObjectContainKey('misc', o) && doesObjectKeyContainThatValue('misc', 0, o)) ? { pic: 0 } : {};
    const misc = (doesObjectContainKey('misc', o) ? { misc: o.misc.map((v: number) => v - 1).filter((v: number) => v >= 0) } : {});
    return { ...o, ...pic, ...misc };
  },
};