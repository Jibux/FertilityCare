import { reverseObject } from 'utils/utils';

export const SW_STATUS = {
  KO: -1,
  OK: 0,
  NO_CHANGE: 0,
  UPDATE_DATA: 1,
} as const;

export const SYNC_WORKING_STATUS = {
  TODO: 3,
  UNKNOWN: 0,
  WORKING: 2,
  PRE_MIGRATE: 4,
  MIGRATE: 5,
  OK: 1,
  KO: -1,
} as const;

export const SYNC_WORKING_STATUS_STR = reverseObject(SYNC_WORKING_STATUS);

export const SYNC_RESULT = {
  KO: -1,
  OK: 0,
  NO_CHANGE: 0,
  UPDATE_CLIENT_DATA: 1,
  UPDATE_SERVER_DATA: 2,
  UPDATE_BOTH_DATA: 3,
  MUST_UPGRADE: 4,
} as const;

export const SYNC_RESULT_STR = reverseObject(SYNC_RESULT);

export const LOGIN_STATUS = {
  ONGOING: -1,
  OK: 0,
  KO: 1,
} as const;

export const HTTP_STATUS = {
  OK: 200,
  UNAUTHORIZED: 401,
  NOT_FOUND: 404,
} as const;

export type SyncResult = ValueOf<typeof SYNC_RESULT>;
export type HttpStatus = ValueOf<typeof HTTP_STATUS>;
export type SyncWorkingStatus = ValueOf<typeof SYNC_WORKING_STATUS>;
export type LoginStatus = ValueOf<typeof LOGIN_STATUS>;
