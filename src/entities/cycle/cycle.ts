import {
  IObservationalDays, getObservationalDaysRange, isAPeriodDay,
  ICycleDay,
  isAPicDay,
} from '../objects/observational-day';
import { isDefined, keysOfHashSorted } from '../../utils/utils';
import { getRealFullDayString, getDateFromString, getYesterday, getDateRange, isDateBefore, getDateDiffInDays, today, isTheSameDay } from '../../utils/date';

export const DEFAULT_NR_OF_CYCLES = 5;
export const MAX_NR_OF_BACK_TO_BACK_DAYS_WITHOUT_DATA = 15;

export interface ICycle {
  [index: string]: any;
  data: ICycleDay[];
  length: any;
  firstDay: Date;
  lastDay: Date;
}

export const createCycle = (initialValues: any = {}): ICycle => {
  const cycle = {
    data: [],
    firstDay: today(),
    lastDay: today(),
    ...initialValues,
  };

  cycle.length = function () { return this.data.length; };

  return cycle;
};

/*
  To get first day of cycle we will look go through all days with data
  until we have found either
    * The 1st period days
    * Enough days without data (ex. after a post-pregnancy period)
*/
export const getFirstDayOfCycle =
  (data: IObservationalDays, dataKeys: string[], position: number): Date => {
    if (position < 1) {
      return getDateFromString(dataKeys[0]);
    }
    const dayIndex = dataKeys[position];
    const dayBeforeIndex = dataKeys[position - 1];

    const day = getDateFromString(dayIndex);
    const dayBefore = getDateFromString(dayBeforeIndex);

    /* It will be the number of days without data between day, dayBefore */
    const dayDiff = getDateDiffInDays(day, dayBefore);

    /*
      1st case: Number of days without data > MAX_NR_OF_BACK_TO_BACK_DAYS_WITHOUT_DATA
      2nd case: It is period day and the day before is not => 1st day of cycle
    */
    if (dayDiff > MAX_NR_OF_BACK_TO_BACK_DAYS_WITHOUT_DATA
      || (isAPeriodDay(data[dayIndex]) && !isAPeriodDay(data[dayBeforeIndex]))) {
      return day;
    } else {
      return getFirstDayOfCycle(data, dataKeys, position - 1);
    }
  };

/* Last day should have some data */
const getLastDayOfCycleKey =
  (data: IObservationalDays, firstDayOfDataToString: string, day: Date): string => {
    const fullStringDay = getRealFullDayString(day);
    if (isDefined(data[fullStringDay])) {
      /* The last day has some data so we will use it */
      return fullStringDay;
    } else if (fullStringDay < firstDayOfDataToString) {
      /* We have reached the oldest day possible of data */
      return firstDayOfDataToString;
    } else {
      /* No data corresponding to this day so we will look for yesterday */
      return getLastDayOfCycleKey(data, firstDayOfDataToString, getYesterday(day));
    }
  };

/* Adjust (if needed) last day to the last day of data */
const adjustLastDay = (lastDay: Date, lastDayOfData: Date) =>
  (isDateBefore(lastDayOfData, lastDay)) ? lastDayOfData : lastDay;

export const getCycle = (observationalDays: IObservationalDays, lastDay = today()): ICycle => {
  const cycle = createCycle();
  const dataKeys = keysOfHashSorted(observationalDays);

  /* No observationalDay to compute */
  if (dataKeys.length === 0) {
    return cycle;
  }

  const firstDayOfDataToString = dataKeys[0];
  const lastDayOfDataToString = dataKeys[dataKeys.length - 1];
  const adjustedLastDay = adjustLastDay(lastDay, getDateFromString(lastDayOfDataToString));

  const lastDayOfCycleToString = getLastDayOfCycleKey(observationalDays, firstDayOfDataToString, adjustedLastDay);
  const position = dataKeys.lastIndexOf(lastDayOfCycleToString);
  const lastDayOfCycle = getDateFromString(lastDayOfCycleToString);

  /*
    position should never be < 0 anyway
    but we have to stop here if the last day of cycle data = first day of data
  */
  if (position < 0 || lastDayOfCycleToString === firstDayOfDataToString) {
    return cycle;
  }

  const firstDayOfCycle = getFirstDayOfCycle(observationalDays, dataKeys, position);

  if (isDateBefore(lastDayOfCycle, firstDayOfCycle)) {
    console.error('This should not happen');
    return cycle;
  }

  const observationalDaysRange =
    getObservationalDaysRange(observationalDays, getDateRange(firstDayOfCycle, lastDayOfCycle));

  cycle.data = observationalDaysRange;
  cycle.firstDay = new Date(firstDayOfCycle);
  cycle.lastDay = new Date(lastDayOfCycle);

  return cycle;
};

/* Get "numberOfCycles" cycles before the day "lastDay" */
export const getCycles =
  (data: IObservationalDays, numberOfCycles = DEFAULT_NR_OF_CYCLES, lastDay = today(),
    cycleStack: ICycle[] = []): ICycle[] => {
    /* We have reached the max number of cycles to get */
    if (numberOfCycles <= 0) {
      return cycleStack;
    }

    const cycle = getCycle(data, lastDay);

    /* If we reached the case cycle length = 0, it means that there is no more cycle to get */
    if (cycle.length() === 0) {
      return cycleStack;
    } else {
      console.debug(`Cycle length: ${cycle.length()}`);
    }

    return getCycles(data, numberOfCycles - 1, getYesterday(cycle.firstDay), [cycle, ...cycleStack]);
  };

export const isTheSameCycle = (c1: ICycle, c2: ICycle) =>
  isDefined(c1)
    ? isDefined(c2)
      ? c1.length() === c2.length() &&
      isTheSameDay(c1.firstDay, c2.firstDay) &&
      isTheSameDay(c1.lastDay, c2.lastDay)
      : false
    : isDefined(c1)
      ? false
      : true;

export const getCycleIndex = (cycles: ICycle[], cycle: ICycle) => cycles.findIndex((c: ICycle) => isTheSameCycle(c, cycle));

export const getSpecificCycle = (cycles: ICycle[], n: number): ICycle =>
  (n > 0)
    ? isDefined(cycles[n])
      ? cycles[n]
      : getSpecificCycle(cycles, n - 1)
    : cycles[0] || createCycle();

export const getPPPCount = (cycle: ICycle) =>
  cycle.data.reduce((count: number, data: ICycleDay) =>
    isAPicDay(data.observationalDay)
      ? 0
      : count >= 0
        ? count + 1
        : count
  , -1);
