import jsPDF, { jsPDFOptions } from 'jspdf';
import 'jspdf-autotable';
import { isDefined, isTruthy, getDeepProperty, IHashKeyString } from '../../utils/utils';
import { truncateAndTransformArray, mergeArrayLines } from '../../utils/array';
import { isFilledWithBabyPicture } from '../object-helpers/observational-data';
import { BABY_PICTURE } from '../baby';
import {
  getDataSummaryToString,
  doesObservationalDayKeyContainData,
  IObservationalDay,
  getIntercourseValue,
  isACorrectedDay,
  isAnAppointmentDay,
  getPicDataSummary,
} from '../objects/observational-day';
import { getPPPCount, ICycle } from './cycle';
import { getRealShortDayString } from '../../utils/date';
import { EB_GARAMOND_NORMAL } from '../fonts/EBGaramond-normal';
// import { EB_GARAMOND_BOLD } from '../fonts/EBGaramond-bold';
import { EB_GARAMOND_EXTRA_BOLD } from '../fonts/EBGaramond-extra-bold';
import { getDateString } from '../../i18n/dates';
import { translate } from 'i18n/i18nService';
import { Language } from 'entities/languages';
import { STICKER_COLOR_HEX } from 'entities/color';

const DEFAULT_NR_OF_DAYS_TO_DISPLAY_PER_LINE = 35;
const DEFAULT_NR_OF_LINES_PER_PAGE = 8;
const MIN_NR_OF_LINES_PER_PAGE = 4;

const PDF_NAME = 'FertilityCareCycles.pdf';

const getStickerData = (observationalDay: IObservationalDay): IHashKeyString =>
  doesObservationalDayKeyContainData('sticker', observationalDay)
    ? {
      color: STICKER_COLOR_HEX[observationalDay.sticker],
      content: getPicDataSummary(observationalDay) + '\n' + getIntercourseValue(observationalDay),
    } : { color: '#ffffff', content: '?' };

const getPPPString = (pppCount: number) =>
  (pppCount > 0)
    ? `                          PPP = ${pppCount}`
    : '';

const getSeparatorLine = (cycle: ICycle) => (maxLineSize: number) => (lang: Language) =>
  [[{
    content: `   ${translate({ name: 'cycle.begin.message', lang, parameters: { date: getDateString(cycle.firstDay, lang) } })}${getPPPString(getPPPCount(cycle))}`,
    // content: '',
    colSpan: maxLineSize,
    styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#f2f2f2', halign: 'left' },
  }]];

const getStickerTextStyle = (obsDay: IObservationalDay) =>
  isACorrectedDay(obsDay)
    ? { textColor: '#D30B0B' }
    : { textColor: '#101010' };

const getInfoTextStyle = (obsDay: IObservationalDay) =>
  isACorrectedDay(obsDay)
    ? { textColor: '#D30B0B', fontStyle: 'bold' }
    : { textColor: '#101010' };

const getDateStyle = (obsDay: IObservationalDay) =>
  isAnAppointmentDay(obsDay)
    ? { lineColor: '#D30B0B', lineWidth: 0.8, textColor: '#D30B0B' }
    : {};

/*
  Input: [[cycle1],[cycle2],[cycle3]]
  Outpout: [[line1],[line2],[line3],[line4],[line5]]
*/
const formatCyclesForOutput =
  (cycles: ICycle[], maxLineSize: number = DEFAULT_NR_OF_DAYS_TO_DISPLAY_PER_LINE) => (lang: Language) =>
    cycles.reduce((accumulator: any[][], cycle) => {
      if (cycle.length() !== 0) {
        const stickerLine = cycle.data.map((data) => {
          const stickerData = getStickerData(data.observationalDay);
          return {
            content: stickerData.content, styles: {
              fillColor: stickerData.color, fontStyle: 'bold', minCellHeight: 17,
              halign: 'right', valign: 'top', ...getStickerTextStyle(data.observationalDay),
            },
          };
        });
        const dateLine = cycle.data.map((data) => ({ content: getRealShortDayString(data.date), styles: { fontSize: 9, ...getDateStyle(data.observationalDay) } }));
        const infosLine = cycle.data.map((data) => ({
          content: getDataSummaryToString(data.observationalDay), styles: {
            fontSize: 9, valign: 'top', ...getInfoTextStyle(data.observationalDay),
          },
        }));
        /*
          We will merge (mergeArrayLines) stickerLine(s) with dateLine(s) and infosLine(s) for cycle X
          Input:
            stickerLine(s) from truncateAndTransformArray:
            [
              [stickerLine1],
              [stickerLine2],
              ...
            ]
            dateLine(s) from truncateAndTransformArray:
            [
              [dateLine1],
              [dateLine2],
              ...
            ]
            infosLine(s) from truncateAndTransformArray:
            [
              [infosLine1],
              [infosLine2],
              ...
            ]
          Output:
            [
              [stickerLine1],
              [dateLine1],
              [infosLine1],
              [stickerLine2],
              [dateLine2],
              [infosLine2],
              ...
            ]
        */
        return accumulator.concat([...getSeparatorLine(cycle)(maxLineSize)(lang),
          ...mergeArrayLines([
            truncateAndTransformArray(stickerLine, maxLineSize),
            truncateAndTransformArray(dateLine, maxLineSize),
            truncateAndTransformArray(infosLine, maxLineSize)])]);
      } else {
        return accumulator;
      }
    }, []);

const generateHeader = (maxLineSize = DEFAULT_NR_OF_DAYS_TO_DISPLAY_PER_LINE) =>
  [...Array(maxLineSize).keys()].map((x) => x + 1);

const isLineSeparator = (line: any[]) => isTruthy(line.length === 1);

const getAdjustedLinesPerPage = (lines: any[][], maxLinesPerPage: number, index = 0): number => {
  if (!isDefined(lines[index])) {
    return index;
  }
  const lastIndex = (isLineSeparator(lines[index])) ? index + 3 : index + 2;
  if (lastIndex >= maxLinesPerPage || !isDefined(lines[lastIndex])) {
    return index;
  } else {
    return getAdjustedLinesPerPage(lines, maxLinesPerPage, lastIndex + 1);
  }
};

const validateMaxLinesPerPage = (linesPerPage: number) =>
  (linesPerPage < MIN_NR_OF_LINES_PER_PAGE) ? MIN_NR_OF_LINES_PER_PAGE : linesPerPage;

/*
  Input: [[[line1],[line2],[line3],[line4],[line5]][line6],etc.]
  Output: [
    [[line1],[line2],[line3],[line4]],	<--- page 1
    [[line5],[line6],etc.],				<--- page 2
    [[etc.]]							<--- page 3
  ]
*/
const getCyclePages =
  (lines: any[][], maxLinesPerPage = DEFAULT_NR_OF_LINES_PER_PAGE, accumulator: any[] = []): any[] => {
    if (lines.length === 0) {
      return accumulator;
    } else {
      const linesPerPageAdjusted = getAdjustedLinesPerPage(lines, validateMaxLinesPerPage(maxLinesPerPage));
      if (linesPerPageAdjusted <= 0) {
        return accumulator;
      }
      const page = lines.slice(0, linesPerPageAdjusted);
      return getCyclePages(lines.slice(linesPerPageAdjusted, lines.length), maxLinesPerPage, [...accumulator, page]);
    }
  };


const isCellFilledWithBabyPicture = (data: any) => {
  const cellFillColor = getDeepProperty(['cell', 'raw', 'styles', 'fillColor'], data);
  return isDefined(cellFillColor) ? isFilledWithBabyPicture(data.cell.raw.styles.fillColor) : false;
};

export const exportToPDF = (cycles: ICycle[]) => (lang: Language) => {
  const numberOfDays = 35;

  //console.debug(cycles);

  const cyclesFormated = formatCyclesForOutput(cycles, numberOfDays)(lang);
  //console.debug(cyclesFormated);

  const pages = getCyclePages(cyclesFormated, 21);

  const header = generateHeader(numberOfDays);

  //console.debug(pages);

  const globalFormat = {
    format: 'a3',
    orientation: 'l',
  } as jsPDFOptions;

  const doc = new jsPDF(globalFormat) as any;

  doc.addFileToVFS('EBGaramond-normal.ttf', EB_GARAMOND_NORMAL);
  doc.addFileToVFS('EBGaramond-bold.ttf', EB_GARAMOND_EXTRA_BOLD);
  doc.addFont('EBGaramond-normal.ttf', 'EBGaramond', 'normal');
  doc.addFont('EBGaramond-bold.ttf', 'EBGaramond', 'bold');

  pages.forEach((page, index) => {
    if (index > 0) {
      doc.addPage(globalFormat);
    }
    doc.autoTable({
      theme: 'grid',
      styles: {
        halign: 'center', valign: 'middle', lineColor: '#4F4F4F',
        lineWidth: 0.4, overflow: 'linebreak', font: 'EBGaramond',
      },
      headStyles: { fillColor: '#0080ff' },
      tableLineWidth: 0.6,
      tableLineColor: '#0F0F0F',
      head: [header],
      body: page,
      didDrawCell: (data: any) => {
        if (data.section === 'body' && isCellFilledWithBabyPicture(data)) {
          doc.addImage(BABY_PICTURE, 'png', data.cell.x + 1, data.cell.y + 1, 5, 11);
        }
      },
    });
  });

  doc.save(PDF_NAME);
};
