import React from 'react';

import { Spacing } from '../../entities/spacing';
import { classNames } from '../../utils/classnames';
import { Children } from '../../types/children';
import './inline.css';

type Props = {
  children: Children;
  justify?: 'CENTER' | 'START' | 'END' | 'SPACE_BETWEEN' | 'SPACE_AROUND' | 'SPACE_EVENLY';
  align?: 'CENTER' | 'START' | 'END' | 'BASELINE' | 'STRETCH';
  columns?: number;
  wrap?: boolean;
  spacing?: Spacing;
  spacingX?: Spacing;
  spacingY?: Spacing;
  height?: string;
};
export function Inline({ children, height, justify, spacing, spacingX, spacingY, align, columns: nb, wrap = true }: Props) {
  return (
    <div className={classNames([
      's-inline',
      {
        's-inline--wrap': wrap,
        's-inline--sized': !!nb,
        [`s-inline--justify-${justify}`]: justify != null,
        [`s-inline--align-${align}`]: align != null,
        [`s-inline--spacing-${spacing}`]: spacing != null,
        [`s-inline--spacing-x-${spacingX}`]: spacingX != null,
        [`s-inline--spacing-y-${spacingY}`]: spacingY != null,
      },
    ])}
    style={{
      '--inline-count': nb,
      'height' : height,
    } as unknown}>
      {children}
    </div>
  );
}
