import React from 'react';

import { Children } from '../../types/children';
import { classNames } from '../../utils/classnames';
import { Spacing } from '../../entities/spacing';
import { Shadow } from '../../entities/shadow';
import { AllColors } from '../../entities/color';
import './container.css';
import { ZIndex } from 'entities/z-index';

type Props = {
  children: Children;
  isInline?: boolean;
  margin?: Spacing;
  padding?: Spacing;
  paddingX?: Spacing;
  paddingY?: Spacing;
  color?: AllColors;
  border?: 'default' | 'light';
  radius?: 'default' | 'rounded';
  grow?: boolean;
  shrink?: boolean;
  shadow?: Shadow;
  overflow?: boolean;
  zIndex?: ZIndex;
  scroll?: boolean;
  height?: string;
  maxHeight?: string;
};
export function Container({ isInline, children, shadow, grow, shrink, margin, padding, paddingX, paddingY, color, border, radius, overflow = true, zIndex, scroll, height, maxHeight }: Props) {
  return (
    <div className={classNames([
      `s-container${isInline ? '--inline' : ''}`,
      {
        ['s-container--no-overflow']: !overflow,
        [`s-container--${grow ? '' : 'no-'}grow`]: grow != null,
        [`s-container--${shrink ? '' : 'no-'}shrink`]: shrink != null,
        [`s-container--margin-${margin}`]: margin != null,
        [`s-container--padding-${padding}`]: padding != null,
        [`s-container--padding-x-${paddingX}`]: paddingX != null,
        [`s-container--padding-y-${paddingY}`]: paddingY != null,
        [`s-container--color-${color}`]: color != null,
        [`s-container--border-${border}`]: border != null,
        [`s-container--radius-${radius}`]: radius != null,
        [`s-container--shadow-${shadow}`]: shadow != null,
        [`s-container--z-${zIndex}`]: zIndex != null,
        ['s-container--scroll']: scroll != null,
      },
    ])}
    style={{
      ...(height ? { height } : {}),
      ...(maxHeight ? { maxHeight } : {}),
    }}
    >
      {children}
    </div>
  );
}