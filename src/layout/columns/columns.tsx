import React, { ReactNode } from 'react';

import { classNames } from '../../utils/classnames';
import { Spacing } from '../../entities/spacing';

import './columns.css';

type Props = {
  children: ReactNode;
  justify?: 'CENTER' | 'START' | 'END' | 'SPACE_BETWEEN' | 'SPACE_AROUND' | 'SPACE_EVENLY';
  align?: 'CENTER' | 'START' | 'END' | 'BASELINE' | 'STRETCH';
  spacing?: Spacing;
};
export function Columns({ children, justify, spacing, align = 'CENTER' }: Props) {
  return (
    <div className={classNames([
      's-columns',
      {
        [`s-columns--justify-${justify}`]: justify != null,
        [`s-columns--align-${align}`]: align != null,
        [`s-columns--spacing-${spacing}`]: spacing != null,
      },
    ])}
    >
      {children}
    </div>
  );
}