import React, { ReactNode } from 'react';

import { classNames } from '../../../utils/classnames';
import './__column.css';

export type Props = {
  children: ReactNode;
  align?: 'CENTER' | 'START' | 'END' | 'BASELINE' | 'STRETCH';
  grow?: boolean;
  shrink?: boolean;
  contentFit?: boolean;
  height?: string;
};
export function __Column({ children, height, align, grow = false, shrink = true, contentFit = false }: Props) {
  return (
    <div className={classNames([
      's-column',
      {
        's-column--grow': grow,
        's-column--shrink': shrink,
        's-column--content-fit': contentFit,
        [`s-column--align-${align}`]: align != null,
      },
    ])}
    style={
      height ? { height } : {}}
    >
      {children}
    </div>
  );
}