import React from 'react';

import { Children } from '../../types/children';
import { Spacing } from '../../entities/spacing';
import { classNames } from '../../utils/classnames';
import './stack.css';

type OverflowY = 'visible' | 'hidden' | 'clip' | 'scroll' | 'auto';

type Props = {
  children: Children;
  spacing?: Spacing;
  height?: string;
  overflowY?: OverflowY;
};
export function Stack({ children, height, spacing, overflowY = 'auto' }: Props) {
  return (
    <div className={classNames([
      's-stack',
      { [`s-stack--spacing-${spacing}`]: spacing != null },
    ])}
      style={
        height ? { height, overflowY: overflowY } : {}
      }
    >
      {children}
    </div>
  );
}