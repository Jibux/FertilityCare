import React, { createContext, Reducer, useEffect, useReducer } from 'react';
import { getIntialState, persistState } from '../utils/persist-state';
import { SyncWorkingStatus, SYNC_WORKING_STATUS } from 'entities/status';
import { getRealFullDayString, today } from 'utils/date';
import { IConfig, defaultConfig } from 'entities/app-config';
import { IObservationalDay, IObservationalDays } from 'entities/objects/observational-day';
import { ILogin, resetLogin, SyncMethod, SYNC_METHOD } from 'entities/sync/sync';
import { Optional } from 'types/optional';

const STORAGE_KEY = 'appState';

export interface AppState {
  syncStatus: SyncWorkingStatus,
  syncMethod: SyncMethod,
  focusedDate: string,
  observationalDays: IObservationalDays,
  login: ILogin,
  config: IConfig,
  onLineStatus: boolean,
}

export type AppStateOptional = Optional<AppState>;

type AppStateKeys = keyof AppState;

const defaultState: AppState = {
  syncStatus: SYNC_WORKING_STATUS.UNKNOWN,
  syncMethod: SYNC_METHOD.FULL_SYNC,
  focusedDate: getRealFullDayString(today()),
  observationalDays: {},
  login: resetLogin(),
  config: defaultConfig(),
  onLineStatus: window.navigator.onLine,
};

const sessionStorageKeys: AppStateKeys[] = ['focusedDate', 'syncMethod'];
const localStorageKeys: AppStateKeys[] = ['observationalDays', 'login', 'config'];

type SessionStorageKey = typeof sessionStorageKeys[number];
type LocalStorageKey = typeof localStorageKeys[number];
type StorageKey = SessionStorageKey | LocalStorageKey;
type ROStorageKeyArray = readonly StorageKey[];


const loadInitialState = (): AppState => {
  const sessionStorageState = getIntialState(STORAGE_KEY, window.sessionStorage) ?? {};
  const localStorageState = getIntialState(STORAGE_KEY, window.localStorage) ?? {};
  const state = { ...defaultState, ...sessionStorageState, ...localStorageState };
  // console.debug(state);
  return state;
};

const initialState: AppState = loadInitialState();

export type AppActions =
  | { type: 'SET_STATE', state: AppStateOptional }
  | { type: 'SET_SYNC_STATUS', syncStatus: SyncWorkingStatus }
  | { type: 'SET_SYNC_METHOD', syncMethod: SyncMethod }
  | { type: 'SET_FOCUSED_DATE', focusedDate: string }
  | { type: 'SAVE_OBSERVATIONAL_DAY', dateString: string, observationalDay: IObservationalDay }
  | { type: 'SET_OBSERVATIONAL_DAYS', observationalDays: IObservationalDays }
  | { type: 'SET_LOGIN_DATA', login: ILogin }
  | { type: 'SET_CONFIG_DATA', config: IConfig }
  | { type: 'SET_ONLINE_STATUS', onLineStatus: boolean };


export interface AppProviderValue {
  state: AppState,
  dispatch(action: AppActions): void;
}
// Create an initial provider value.
const providerValue: AppProviderValue = {
  state: initialState,
  dispatch: () => { }, // << This will be overwritten
};
// Create the store or 'context'.
const appStore = createContext(providerValue);
const { Provider } = appStore;


const reducer = (state: AppState, action: AppActions): AppState => {
  switch (action.type) {
    case 'SET_STATE':
      return { ...state, ...action.state };
    case 'SET_SYNC_STATUS':
      return {
        ...state,
        syncStatus: action.syncStatus,
      };
    case 'SET_SYNC_METHOD':
      return {
        ...state,
        syncMethod: action.syncMethod,
      };
    case 'SET_FOCUSED_DATE':
      return {
        ...state,
        focusedDate: action.focusedDate,
      };
    case 'SAVE_OBSERVATIONAL_DAY':
      return {
        ...state,
        observationalDays: { ...state.observationalDays, [action.dateString]: action.observationalDay },
      };
    case 'SET_OBSERVATIONAL_DAYS':
      return {
        ...state,
        observationalDays: action.observationalDays,
      };
    case 'SET_LOGIN_DATA':
      return {
        ...state,
        login: action.login,
        syncStatus: SYNC_WORKING_STATUS.UNKNOWN,
      };
    case 'SET_CONFIG_DATA':
      return {
        ...state,
        config: action.config,
      };
    case 'SET_ONLINE_STATUS':
      return {
        ...state,
        onLineStatus: action.onLineStatus,
      };
    default:
      throw new Error('Action invalid.');
  }
};

const SetupEffect = (state: AppState, keys: ROStorageKeyArray, storage: Storage) => {
  useEffect(() => {
    if (keys.length !== 0) {
      const storageState = keys.reduce((acc, key) => ({ [key]: state[key], ...acc }), {});
      // console.debug(storageState);
      persistState(STORAGE_KEY, storageState, storage);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, keys.map((key) => state[key]));
};

const AppStateProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer<Reducer<AppState, any>>(reducer, initialState);
  const providerValue2 = { state, dispatch };
  SetupEffect(state, sessionStorageKeys, window.sessionStorage);
  SetupEffect(state, localStorageKeys, window.localStorage);

  return <Provider value={providerValue2}>{children}</Provider>;
};

export { appStore, AppStateProvider };
