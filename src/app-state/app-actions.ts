import { IConfig } from 'entities/app-config';
import { Language, LANGUAGE_CHOICE } from 'entities/languages';
import { IObservationalDay, createObservationalDay, updateModificationDate, IObservationalDays } from 'entities/objects/observational-day';
import { SyncWorkingStatus } from 'entities/status';
import { resetLogin, SyncMethod, LOGIN_SYNC_METHOD } from 'entities/sync/sync';
import { getDefaultLanguage } from 'i18n/i18nService';
import { getDateFromString, getRealFullDayString } from 'utils/date';
import { IHashKeyString, isDefined } from 'utils/utils';
import { AppProviderValue, AppStateOptional } from './app-state';


export const setState = ({ dispatch }: AppProviderValue, state: AppStateOptional) => {
  dispatch({ type: 'SET_STATE', state });
};

export const setSyncStatus = ({ dispatch }: AppProviderValue, syncStatus: SyncWorkingStatus) => {
  dispatch({ type: 'SET_SYNC_STATUS', syncStatus });
};

export const resetLoginSyncMethod = ({ dispatch }: AppProviderValue) => {
  dispatch({ type: 'SET_SYNC_METHOD', syncMethod: LOGIN_SYNC_METHOD.FULL_SYNC });
};

export const setSyncMethod = ({ dispatch }: AppProviderValue, syncMethod: SyncMethod) => {
  dispatch({ type: 'SET_SYNC_METHOD', syncMethod });
};

export const getFocusedDate = ({ state: { focusedDate } }: AppProviderValue): Date =>
  getDateFromString(focusedDate);

export const setFocusedDate = (context: AppProviderValue, focusedDate: Date) => {
  const { dispatch } = context;
  dispatch({ type: 'SET_FOCUSED_DATE', focusedDate: getRealFullDayString(focusedDate) });
};

export const getObservationalDayLazy = ({ state: { observationalDays } }: AppProviderValue, date: Date): IObservationalDay => {
  const fullDayString = getRealFullDayString(date);
  return observationalDays[fullDayString];
};

export const getObservationalDay = (context: AppProviderValue, date: Date): IObservationalDay =>
  createObservationalDay(getObservationalDayLazy(context, date));

export const getLanguage = ({ state: { config: { lang } } }: AppProviderValue): Language =>
  isDefined(lang) && lang !== LANGUAGE_CHOICE.BROWSER
    ? lang
    : getDefaultLanguage();

export const saveObservationalDay = (context: AppProviderValue, date: Date, observationalDay: IObservationalDay) => {
  const { dispatch } = context;
  dispatch({ type: 'SAVE_OBSERVATIONAL_DAY', dateString: getRealFullDayString(date), observationalDay: updateModificationDate(observationalDay) });
};

export const deleteObservationalDay = (context: AppProviderValue, date: Date) => {
  const { dispatch } = context;
  /* FIXME maybe find better code... */
  dispatch({ type: 'SAVE_OBSERVATIONAL_DAY', dateString: getRealFullDayString(date), observationalDay: updateModificationDate(createObservationalDay()) });
};

export const setObservationalDays = (context: AppProviderValue, observationalDays: IObservationalDays) => {
  const { dispatch } = context;
  dispatch({ type: 'SET_OBSERVATIONAL_DAYS', observationalDays });
};

export const setLoginData = (context: AppProviderValue, value: IHashKeyString) => {
  const { dispatch } = context;
  dispatch({
    type: 'SET_LOGIN_DATA', login: {
      status: true,
      provider: value.provider,
      data: value.data,
    },
  });
};

export const resetLoginData = (context: AppProviderValue) => {
  const { dispatch } = context;
  dispatch({ type: 'SET_LOGIN_DATA', login: resetLogin() });
};

export const setConfigData = (context: AppProviderValue, config: IConfig) => {
  const { state, dispatch } = context;
  dispatch({ type: 'SET_CONFIG_DATA', config: { ...state.config, ...config } });
};

export const updateOnLineStatus = (context: AppProviderValue) => {
  const { dispatch } = context;
  dispatch({ type: 'SET_ONLINE_STATUS', onLineStatus: window.navigator.onLine });
};
