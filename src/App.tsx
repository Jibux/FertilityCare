import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom';

import { AppStateProvider } from './app-state/app-state';
import { routes, Route as RouteDefinition } from './routes';

import './App.css';

function RouteWithSubRoutes(route: RouteDefinition) {
  return (
    <Route
      path={route.path}
      render={(props) => (
        <route.component {...props} />
      )}
    />
  );
}

function App() {
  return (
    <AppStateProvider>
      <BrowserRouter>
        <Switch>
          {
            routes.map(
              (route: RouteDefinition, i: number) => (<RouteWithSubRoutes key={i} {...route} />),
            )
          }
        </Switch>
      </BrowserRouter>
    </AppStateProvider>
  );
}

export default App;
