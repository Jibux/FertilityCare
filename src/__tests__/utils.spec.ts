import { getDeepProperty } from '../utils/utils';

describe('utils', () => {
  it('getDeepProperties', () => {
    const object = { test0: { test01: { test001: 'toto001' }, test02: 'toto02' }, test1: 'toto1' };

    expect(getDeepProperty(['test'], object)).toBe(undefined);
    expect(getDeepProperty(['test', 'test2'], object)).toBe(undefined);
    expect(getDeepProperty(['test1'], object)).toBe('toto1');
    expect(getDeepProperty(['test0', 'test02'], object)).toBe('toto02');
    expect(getDeepProperty(['test0', 'test01', 'test001'], object)).toBe('toto001');
  });
});
