import {
  createObservationalDay, addToObservationalDay,
  removeFromObservationalDay, doesObservationalDayKeyContainData, setOldObservationalDay, migrateObservationalDay,
} from '../entities/objects/observational-day';
import { KEYS_DATA, OD_MAX_VERSION } from '../entities/observational-data/observational-data';

describe('ObservationalDay', () => {
  const stickersLength = Object.keys(KEYS_DATA.sticker).length;
  const periodLength = Object.keys(KEYS_DATA.period).length;
  const mucusLength = Object.keys(KEYS_DATA.mucus).length;
  const mucusExtendedLength = Object.keys(KEYS_DATA.mucusExtended).length;
  const frequencyLength = Object.keys(KEYS_DATA.frequency).length;
  const miscLength = Object.keys(KEYS_DATA.misc).length;

  const sticker = stickersLength - 1;
  const _period = periodLength - 1;
  const _frequency = frequencyLength - 1;
  const mucus = mucusLength - 1;
  const mucusExtended = mucusExtendedLength - 1;
  const _misc = miscLength - 1;
  const comment = 'Windows is stinking';
  const extendableValue = 4;
  const nonExtendableValue = 1;
  const badValue = -1;

  it('stickers can be set and unset', () => {
    let obsDay = createObservationalDay();

    obsDay = addToObservationalDay('sticker', sticker)(obsDay);
    expect(obsDay.sticker).toBe(sticker);
    obsDay = addToObservationalDay('sticker', badValue)(obsDay);
    expect(obsDay.sticker).toBe(sticker);
    obsDay = addToObservationalDay('sticker', '1')(obsDay);
    expect(obsDay.sticker).toBe(sticker);
    obsDay = removeFromObservationalDay('sticker')()(obsDay);
    expect(obsDay.sticker).toBe(undefined);
  });

  it('mucusExtended can be add and deleted', () => {
    let obsDay = createObservationalDay();

    expect(obsDay.mucusExtended).toBe(undefined);
    obsDay = addToObservationalDay('mucusExtended', mucusExtended)(obsDay);
    expect(obsDay.mucusExtended.includes(mucusExtended)).toBe(true);
    obsDay = addToObservationalDay('mucusExtended', 2)(obsDay);
    expect(obsDay.mucusExtended.includes(2)).toBe(true);
    obsDay = addToObservationalDay('mucusExtended', '1')(obsDay);
    expect(obsDay.mucusExtended.includes(1)).toBe(false);
    obsDay = removeFromObservationalDay('mucusExtended')(2)(obsDay);
    expect(obsDay.mucusExtended.includes(2)).toBe(false);
    expect(obsDay.mucusExtended.includes(mucusExtended)).toBe(true);
    obsDay = removeFromObservationalDay('mucusExtended')()(obsDay);
    expect(obsDay.mucusExtended).toBe(undefined);
  });

  it('mucus can be set and unset', () => {
    let obsDay = createObservationalDay();

    obsDay = addToObservationalDay('mucus', mucus)(obsDay);
    expect(obsDay.mucus).toBe(mucus);
    obsDay = addToObservationalDay('mucus', 1000)(obsDay);
    expect(obsDay.mucus).toBe(mucus);
    obsDay = addToObservationalDay('mucus', badValue)(obsDay);
    expect(obsDay.mucus).toBe(mucus);
    obsDay = removeFromObservationalDay('mucus')()(obsDay);
    expect(obsDay.mucus).toBe(undefined);
  });

  it('mucus can trigger mucusExtended', () => {
    let obsDay = createObservationalDay();

    // Remove mucus => remove mucusExtended
    obsDay = addToObservationalDay('mucus', extendableValue)(obsDay);
    expect(obsDay.mucus).toBe(extendableValue);
    obsDay = addToObservationalDay('mucusExtended', mucusExtended)(obsDay);
    expect(obsDay.mucusExtended.includes(mucusExtended)).toBe(true);
    obsDay = removeFromObservationalDay('mucus')()(obsDay);
    expect(obsDay.mucusExtended).toBe(undefined);

    // Change mucus to nonExtendableValue => remove mucusExtended
    obsDay = addToObservationalDay('mucus', extendableValue)(obsDay);
    expect(obsDay.mucus).toBe(extendableValue);
    obsDay = addToObservationalDay('mucusExtended', mucusExtended)(obsDay);
    expect(obsDay.mucusExtended.includes(mucusExtended)).toBe(true);
    obsDay = addToObservationalDay('mucus', nonExtendableValue)(obsDay);
    expect(obsDay.mucus).toBe(nonExtendableValue);
    expect(obsDay.mucusExtended).toBe(undefined);
  });

  it('comment can be set and unset', () => {
    let obsDay = createObservationalDay();

    expect(obsDay.comment).toBe(undefined);
    obsDay = addToObservationalDay('comment', comment)(obsDay);
    expect(obsDay.comment).toBe(comment);
    obsDay = addToObservationalDay('comment', 12345)(obsDay);
    expect(obsDay.comment).toBe(comment);
    obsDay = removeFromObservationalDay('comment')()(obsDay);
    expect(obsDay.comment).toBe(undefined);
  });

  it('test doesObservationalDayKeyContainData', () => {
    let obsDay = createObservationalDay();

    expect(doesObservationalDayKeyContainData('mucus', obsDay)).toBe(false);
    expect(doesObservationalDayKeyContainData('mucusExtended', obsDay)).toBe(false);

    obsDay = addToObservationalDay('mucus', extendableValue)(obsDay);
    expect(obsDay.mucus).toBe(extendableValue);
    expect(doesObservationalDayKeyContainData('mucus', obsDay)).toBe(true);
    obsDay = addToObservationalDay('mucusExtended', mucusExtended)(obsDay);
    expect(obsDay.mucusExtended.includes(mucusExtended)).toBe(true);
    expect(doesObservationalDayKeyContainData('mucusExtended', obsDay)).toBe(true);
  });

  it('oldObservationalDay can be set but not overwritten', () => {
    let obsDay = createObservationalDay({ sticker: 1, mucus: 3, frequency: 2 });

    expect(obsDay.mucus).toBe(3);
    expect(obsDay.oldObservationalDay).toBe(undefined);
    obsDay = setOldObservationalDay(obsDay);
    expect(obsDay.mucus).toBe(obsDay.oldObservationalDay.mucus);
    expect(obsDay.sticker).toBe(obsDay.oldObservationalDay.sticker);
    expect(obsDay.frequency).toBe(obsDay.oldObservationalDay.frequency);
    obsDay = addToObservationalDay('similarity', 0)(obsDay);
    expect(obsDay.similarity).toBe(0);
    expect(obsDay.oldObservationalDay.similarity).toBe(undefined);
    obsDay = setOldObservationalDay(obsDay);
    expect(obsDay.oldObservationalDay.similarity).toBe(undefined);
  });

  it('Data migration', () => {
    let oldObsDay = createObservationalDay({ sticker: 1, misc: [0, 2] });
    let obsDay = createObservationalDay({ sticker: 2, misc: [0, 2], oldObservationalDay: oldObsDay });
    let newObsDay = migrateObservationalDay(obsDay);
    let newOldObsDay = newObsDay.oldObservationalDay;
    expect(newObsDay.sticker).toBe(2);
    expect(newObsDay.misc).toStrictEqual([1]);
    expect(newObsDay.pic).toBe(0);
    expect(newObsDay.v).toBe(OD_MAX_VERSION);
    expect(newOldObsDay.sticker).toBe(1);
    expect(newOldObsDay.misc).toStrictEqual([1]);
    expect(newOldObsDay.pic).toBe(0);
    expect(newOldObsDay.v).toBe(OD_MAX_VERSION);
  });
});
