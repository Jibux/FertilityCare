import { saveAs } from 'file-saver';

export const newBlob = (data: any, type: string) =>
  new Blob([data], { type });

/* Download JSON data */
export const downloadData = (data: any, fileName: string) => {
  const blob = newBlob(JSON.stringify(data), 'application/json;charset=utf-8');
  saveAs(blob, fileName);
};

/* Upload JSON data - Promise which resolve the parsed JSON content */
export const uploadData = (file: any) =>
  new Promise((resolve, reject) => {
    if (file) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const target = e.target;
        const contents = (target) ? target.result : null;
        const jsonContent = JSON.parse(contents as string);
        // console.log(jsonContent);
        resolve(jsonContent);
      };
      reader.readAsText(file);
    } else {
      reject('Failed to load file');
    }
  });
