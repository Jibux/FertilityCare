import { FirstDaysOfWeek, FIRST_DAYS_OF_WEEK } from '../entities/locales';

export const WEEKS_PER_MONTH = 5;
export const DAYS_PER_WEEK = 7;

export const today = (): Date => {
  let date = new Date();
  //date.setDate(30);
  //date.setMonth(8);
  return date;
};

export const timestamp = (): number => Math.round(Date.now() / 1000);

export const getDay = (date: Date, weekFirstDay: FirstDaysOfWeek) =>
  (weekFirstDay === FIRST_DAYS_OF_WEEK.MONDAY) ? (6 + date.getDay()) % 7 : date.getDay();

export const addDays = (nb: number, date: Date = today()) => {
  const result = new Date(date.valueOf());
  result.setDate(date.getDate() + nb);
  return result;
};

export const format = (date: Date): string =>
  date.toDateString();

export const isTheSameYear = (date1: Date, date2: Date): boolean =>
  date1.getFullYear() === date2.getFullYear();
export const isTheSameMonth = (date1: Date, date2: Date): boolean =>
  isTheSameYear(date1, date2)
  && date1.getMonth() === date2.getMonth();
export const isTheSameDay = (date1: Date, date2: Date): boolean =>
  isTheSameMonth(date1, date2)
  && date1.getDate() === date2.getDate();

export const isToday = (date: Date) => isTheSameDay(date, today());
export const isDateBefore = (date1: Date, date2: Date) => date1.getTime() < date2.getTime();
export const isDateBeforeOrEqual = (date1: Date, date2: Date) => date1.getTime() <= date2.getTime();
export const isBeforeToday = (date: Date) => isDateBefore(date, today());
export const getTodaysMonth = () => today().getMonth();
export const getTodaysYear = () => today().getFullYear();
export const getMonthString = (date: Date) =>
  (date.getMonth()).toString().padStart(2, '0');
export const getDayString = (date: Date) =>
  date.getDate().toString().padStart(2, '0');
export const getFullDayString = (date: Date): string =>
  date.getFullYear().toString()
  + getMonthString(date)
  + getDayString(date);
const getHoursString = (date: Date) =>
  date.getHours().toString().padStart(2, '0');
const getMinutesString = (date: Date) =>
  date.getMinutes().toString().padStart(2, '0');
const getSecondsString = (date: Date) =>
  date.getSeconds().toString().padStart(2, '0');
const getMillisecString = (date: Date) =>
  getSecondsString(date) +
  date.getMilliseconds().toString().padStart(3, '0');
export const getLogDate = (date: Date = new Date()): string =>
  date.getFullYear().toString() + '-'
  + getMonthString(date) + '-'
  + getDayString(date) + ' '
  + getHoursString(date) + ':'
  + getMinutesString(date) + ':'
  + getMillisecString(date);
export const getLogFileDate = (date: Date = new Date()): string =>
  date.getFullYear().toString() + '-'
  + getMonthString(date) + '-'
  + getDayString(date) + '_'
  + getHoursString(date) +
  + getMinutesString(date) +
  + getSecondsString(date);

export const getRealMonthString = (date: Date) =>
  (date.getMonth() + 1).toString().padStart(2, '0');
export const getRealFullDayString = (date: Date, separator = ''): string =>
  date.getFullYear().toString() + separator
  + getRealMonthString(date) + separator
  + getDayString(date);

export const getRealShortDayString = (date: Date): string =>
  date.getDate().toString().padStart(2, '0') + '/'
  + (date.getMonth() + 1).toString().padStart(2, '0');

export const getDateFromString = (dateString: string, separator = '') =>
  new Date(
    parseInt(dateString.substring(0, 4), 10),
    parseInt(dateString.substring(4 + separator.length, 6 + separator.length), 10) - 1,
    parseInt(dateString.substring(6 + (separator.length * 2), 8 + (separator.length * 2)), 10));


// TODO: code this in functional immutable
export const offsetDate = (date: Date, offset: number) => {
  const returnDate = new Date(date);
  returnDate.setDate(date.getDate() + offset);
  return returnDate;
};

export const getYesterday = (date: Date = today()) => offsetDate(date, -1);

export const getDateRange = (beginDate: Date, endDate: Date, dateRange: Date[] = []): Date[] =>
  (isTheSameDay(beginDate, endDate))
    // Stop condition: we return the date range
    ? [...dateRange, beginDate]
    // Return function with beginDate + 1 day
    : getDateRange(offsetDate(beginDate, 1), endDate, [...dateRange, beginDate]);

export const isCurrentMonth = (date: Date) =>
  isTheSameMonth(today(), date);

export const isCurrentYear = (date: Date) =>
  isTheSameYear(today(), date);

export const getFirstDayOfMonth = (date: Date = today(), monthOffset = 0) =>
  new Date(date.getFullYear(), date.getMonth() + monthOffset, 1);

export const getLastDayOfMonth = (date: Date = today(), monthOffset = 0) =>
  new Date(date.getFullYear(), date.getMonth() + monthOffset + 1, 0);

export const offsetMonth = (date: Date, offset: number) =>
  new Date(date.getFullYear(), date.getMonth() + offset, date.getDate());

export const offsetWeek = (date: Date, offset: number) =>
  offsetDate(date, offset * 7);

export const getFirstDayOfWeek = (weekFirstDay: FirstDaysOfWeek) => (date: Date = today()) =>
  offsetDate(date, -getDay(date, weekFirstDay));

export const getLastDayOfWeek = (weekFirstDay: FirstDaysOfWeek) => (date: Date = today()) =>
  offsetDate(getFirstDayOfWeek(weekFirstDay)(date), 6);

export const getMonthCalendarFirstDay = (weekFirstDay: FirstDaysOfWeek) => (date: Date = today(), monthOffset = 0) =>
  getFirstDayOfWeek(weekFirstDay)(getFirstDayOfMonth(date, monthOffset));

export const getMonthCalendarLastDay = (weekFirstDay: FirstDaysOfWeek) => (date: Date = today(), monthOffset = 0) =>
  getLastDayOfWeek(weekFirstDay)(addDays((WEEKS_PER_MONTH * DAYS_PER_WEEK) - 1, getMonthCalendarFirstDay(weekFirstDay)(date, monthOffset)));

export const getWeekDaysRange = (weekFirstDay: FirstDaysOfWeek) =>
  getDateRange(getFirstDayOfWeek(weekFirstDay)(), getLastDayOfWeek(weekFirstDay)());

export const getDateDiffInDays = (date1: Date, date2: Date) =>
  Math.abs(date1.getTime() - date2.getTime()) / (1000 * 3600 * 24);

export const isValidDate = (d: any) => {
  if (Object.prototype.toString.call(d) === '[object Date]') {
    if (isNaN(d.getTime())) {
      return false;
    } else {
      return true;
    }
  } else {
    return false;
  }
};

export const filterDay = (day: Date) => isValidDate(day) ? day : today();
