import axios from 'axios';
import { isOnLine } from './utils';

export const postData = (url: string, data: {} | string, headers: {}): Promise<any> =>
  new Promise((resolve, reject) => {
    /* if (process.env.NODE_ENV !== 'production') {
      console.log('Not a production environment, assume status is OK');
      resolve({status: HTTP_STATUS.OK, data: {status: SW_STATUS.OK}});
    } else */
    if (isOnLine()) {
      axios.post(url, data, { headers })
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    } else {
      reject('Network error: please check your internet connectivity');
    }
  });

/* export const loginToServer = (credentials: IHashKeyString) =>
  postData('login', credentials);

export const logoutFromServer = () =>
  postData('logout');

export const synchronizeWithServer = (observationalDays: IObservationalDays) =>
  postData('sync', observationalDays); */

