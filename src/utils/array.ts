import { curry } from 'ramda';
import { isDefined, removeProperty } from './utils';

/*
  Input: line = [elem1, elem2, etc., elemX]
  Output: [
    [elem1, elem2, etc., elemK](K < maxLineSize)
    [elemK+1, elemK+2, etc., elemL](L < maxLineSize)
    [etc., elemX]
  ]
*/
export const truncateAndTransformArray = (line: any[], maxLineSize: number): any[][] => {
  if (line.length <= maxLineSize) {
    return [line];
  } else {
    const linePrefix: any[] = line.slice(0, maxLineSize);
    const lineSuffix: any[] = line.slice(maxLineSize, line.length);
    return [linePrefix].concat(truncateAndTransformArray(lineSuffix, maxLineSize));
  }
};

// FIXME: what if array1.length != array2.length
/*
  Input: [[
    [line1a],
    [line2a],
    ...
  ],
  [
    [line1b],
    [line2b],
    ...
  ],
  ...]
  Outpout: [
    [line1a],
    [line1b],
    [line2a],
    [line2b],
    ...
  ]
*/
export const mergeArrayLines = (arrays: any[][]) => {
  if (arrays.length < 2) {
    return arrays;
  }
  const arraySuffix = arrays.slice(1);
  return arrays[0].reduce((acc, line, index) =>
    ([...acc, line, ...arraySuffix.map((array) => array[index])])
  , []);
};

type ArrayType = 'string' | 'number';

export const getArray = (array: any[]) => isDefined(array) ? array : [];
export const isEmptyArray = (array: any[]) => array.length === 0;
export const isValueInArray = (array: any[], value: any) => array.includes(value);
export const sortArrayOfNumber = (arr: number[]) => arr.sort((a, b) => a - b);
export const sortArray = (array: any[], arrayType: ArrayType) =>
  arrayType === 'number'
    ? sortArrayOfNumber(array)
    : array.sort();

export const getArrayElemFromEnd = (array: any[], index: number) =>
  (array.length >= index && index > 0) ? array[array.length - index] : array[0];

export const removeFromArray = (array: any[], value: any): any[] => array.filter((v: any) => v !== value);
export const addToArrayUnique = (array: any[], value: any): any[] =>
  isValueInArray(array, value)
    ? array
    : [...array, value];

interface IArrayPropertyOption {
  type: ArrayType;
  sorted: boolean;
}

const arraySortAction = (array: any[], options: IArrayPropertyOption) =>
  options.sorted
    ? sortArray(array, options.type)
    : array;

const addToArray = (array: any[], value: any, options: IArrayPropertyOption) =>
  arraySortAction(
    addToArrayUnique(getArray(array), value),
    options,
  );

export const setArrayProperty = curry(
  (key: string, value: any, options: IArrayPropertyOption, obj: any) =>
    ({ ...obj, [key]: addToArray(getArray(obj[key]), value, options) }),
);

/* Remove property (array) if its length is 0 */
export const reduceArrayProperty = curry(
  (key: string, obj: any) =>
    obj[key].length === 0
      ? removeProperty(key, obj)
      : obj,
);

export const removeArrayProperty = curry(
  (key: string, value: any, obj: any) =>
    isDefined(obj[key]) ?
      reduceArrayProperty(key, { ...obj, [key]: removeFromArray(obj[key], value) })
      : obj,
);

export const arraysEqual = (a1: any[], a2: any[]) => JSON.stringify(a1) === JSON.stringify(a2);
