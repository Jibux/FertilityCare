import { getLogDate, getLogFileDate } from './date';
import { toStringObj } from './string';

const FC_LOG_FILE_NAME = 'FertilityCare.log';

export class Log {
  static logs = [];

  private static write = (data: any) => {
    Log.logs.push(`${getLogDate()} ${data}`);
  };

  static debug = (data: any) => {
    console.debug(data);
    Log.write(`[DEBUG] ${toStringObj(data)}`);
  };

  static log = (data: any) => {
    console.log(data);
    Log.write(`[INFO] ${toStringObj(data)}`);
  };

  static error = (data: any) => {
    console.error(data);
    Log.write(`[ERROR] ${toStringObj(data)}`);
  };

  static download = () => {
    const url = window.URL.createObjectURL(
      new Blob([Log.logs.join('\n')]),
    );
    const fc_log_file_name = `${getLogFileDate()}_${FC_LOG_FILE_NAME}`;
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute(
      'download',
      fc_log_file_name,
    );
    // Append to html link element page
    document.body.appendChild(link);
    // Start download
    link.click();
    // Clean up and remove the link
    link.parentNode.removeChild(link);
  };
}