export const persistState = (storageKey: string, state: object, storage: Storage): void => {
  storage.setItem(storageKey, JSON.stringify(state));
};

export const getIntialState = (storageKey: string, storage: Storage): any => {
  const savedState = storage.getItem(storageKey);
  try {
    if (!savedState) {
      return undefined;
    }
    return JSON.parse(savedState ?? '{}');
  } catch (e) {
    console.error('Error loading state : ' + storageKey);
    return undefined;
  }
};
