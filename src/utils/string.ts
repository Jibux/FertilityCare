export const pascalCase = (str: string) => str
  .split('|||')
  .map(([firstLetter, ...tail]) => `${firstLetter.toUpperCase()}${tail.join('').toLowerCase()}`)
  .join('|||');


export const camelCase = (stringToFormat: string) => (
  stringToFormat.toLowerCase().replace(/^\w|\s\w/g, (letter: string) =>
    letter.toUpperCase())
);

export const sentenceCase = (str: string) =>
  str.toLowerCase()
    .split('|||')
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join('|||');

export const toStringObj = (obj: any) =>
  (typeof obj == 'object') ? JSON.stringify(obj) :
    (typeof obj == 'string') ? obj : obj.toString();
