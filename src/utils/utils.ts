import {
  set,
  prop,
  lensProp,
  curry,
  path,
  lensPath,
  dissoc,
  dissocPath,
} from 'ramda';

export const isTruthy = (a: any): boolean => !!a;
export const isFalsy = (a: any): boolean => !a;
export const isDefined = (a: any): boolean => a != null;
// Si a = null, isEmpty retourne false... c'est l'effet voulu ?
export const isStringEmpty = (a: string): boolean => a != null && a.trim() === '';
export const isNotEmpty = (a: string): boolean => a != null && a.trim() !== '';

export const filterPositiveInteger = (number: number, defaultValue: number) =>
  Number.isInteger(number) && number >= 0 ? number : defaultValue;

export const filterAbsolutePositiveInteger = (number: number, defaultValue: number) =>
  number === 0 ? defaultValue : filterPositiveInteger(number, defaultValue);

export const filterMax = (number: number, max: number) =>
  number > max ? max : number;

/* Finds key of hash where hash[key] === value */
export const getKeyOfHashMatchingValue = curry(
  (value: any, obj: any) => Object.keys(obj).find((key) => obj[key] === value),
);

export const getProperty = curry(
  (key: string, obj: any) => prop(key, obj),
);
export const getDeepProperty = curry(
  // tslint:disable-next-line:variable-name
  (_path: Array<string | number>, obj: any): any => path(_path, obj),
);

export const setProperty = curry(
  (key: string, value: any, obj: any) => set(lensProp((key as string)), value, obj),
);
export const setDeepProperty = curry(
  (keys: Array<string | number>, value: any, obj: any) => set(lensPath(keys), value, obj),
);

export const removeProperty = curry(
  (key: string, obj: any) => dissoc((key as string), obj),
);
export const removeDeepProperty = curry(
  (keys: Array<string | number>, obj: any) => dissocPath(keys, obj),
);

export const getRandomInt = (min: number, max: number) => {
  min = Math.ceil(min);
  max = Math.floor(max + 1);
  return Math.floor(Math.random() * (max - min)) + min;
};

export const reverseObject = (obj: {}) => Object.keys(obj).reduce((stack, k) => ({ ...stack, [obj[k]]: k }), {});

export const keysOfHashSorted = (hash: {}) => Object.keys(hash).sort();
export const keysOfHashSortedReverted = (hash: {}) => Object.keys(hash).reverse();

export const getNavigator = () => navigator as Navigator;
export const isOnLine = () => getNavigator().onLine;

export interface IHashKeyNumber { [key: number]: any; }
export interface IHashKeyString { [key: string]: any; }
