import { useHistory } from 'react-router-dom';

const fromString = <A extends { [key: string]: string | undefined }>(
  queryParams: string,
  char = '?',
) =>
    decodeURI(queryParams)
      .replace(char, '')
      .split('&')
      .map(param => param.split('='))
      .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {} as A);

const queryParamsToString = (queryParams: { [key: string]: string | undefined }) =>
  `?${Object.entries(queryParams)
    .map(entry => entry.map(a => encodeURI(a || '')))
    .map(([key, value]) => `${key}=${value}`)
    .join('&')}`;

export function useQueryParams<
  A extends { [key: string]: string | undefined },
>() {
  const history = useHistory();

  const queryParams: A = fromString(history.location.search);
  const setQueryParams = (newParams: A) => {
    const params = Object.entries(newParams)
      .filter(([key, value]) => key.length > 0 && value && value.length > 0)
      .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {} as A);

    history.push({
      ...history.location,
      search: queryParamsToString(params),
    });
  };

  return [queryParams, setQueryParams] as const;
}

// No more used
/*export function useHashParams<
  A extends { [key: string]: string | undefined }
>() {
  const history = useHistory();

  const hashParams: A = fromString(history.location.hash, '#');

  const resetHash = () => {
    history.push({
      ...history.location,
      hash: '',
    });
  }

  return [hashParams, resetHash] as const;
};*/
