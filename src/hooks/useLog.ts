import { useEffect } from 'react';
import { Log } from 'utils/logs';

export const useLog = (message: string, data: any, level = 'debug') => {
  return useEffect(() => {
    Log[level](message);
  }, [data]);
};
