import { ComponentLibrary } from './pages/components-library/components-libary';
import { FCApp } from './pages/fc-app/fc-app';

export const ROUTES = {
  COMPONENTS_LIBRARY: '/__lib',
  FC_APP: '/',
} as const;

export type Route = {
  path: ValueOf<typeof ROUTES>,
  component: (...arg: any[]) => JSX.Element,
  routes?: Route[]
};

export const routes: Route[] = [
  {
    path: ROUTES.COMPONENTS_LIBRARY,
    component: ComponentLibrary,
  },
  {
    path: ROUTES.FC_APP,
    component: FCApp,
  },
];
