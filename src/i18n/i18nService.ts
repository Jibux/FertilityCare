import { Language, LANGUAGES } from '../entities/languages';
import { Translation } from 'types/translations';
import { default as TRANSLATIONS_FR } from './fr/index.json';
import { default as TRANSLATIONS_EN } from './en/index.json';
import { isValueInArray } from 'utils/array';

export const DEFAULT_LANG = 'fr';

export const TRANSLATIONS = {
  'fr': TRANSLATIONS_FR,
  'en': TRANSLATIONS_EN,
} as const;

export const getDefaultLanguage = (): Language => {
  const languageList = Object.values(LANGUAGES);
  const matchedLanguages = navigator.languages.filter((x: Language) => isValueInArray(languageList, x)) as Language[];
  const bestLanguage = (matchedLanguages.length > 0) ? matchedLanguages[0] : DEFAULT_LANG;
  return bestLanguage;
};

export type TranslateProps = {
  name: Translation;
  lang?: Language;
  filter?: (s: string) => string;
  parameters?: { [key: string]: unknown };
};
export const translate = ({ lang,
  name,
  filter = (s) => s,
  parameters = {},
}: TranslateProps) => {
  const translations = TRANSLATIONS[lang];
  const translation = Object.entries(parameters).reduce(
    (result, [key, value]) =>
      result.replace(
        new RegExp(`%{${key.replace(/\./g, '.')}}`, 'g'),
        `${value}`,
      ),
    translations[name] || name || '',
  );
  return filter(translation);
};
