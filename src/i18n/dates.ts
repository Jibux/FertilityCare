import { fold } from '../utils/union-type-helper';
import { ViewType } from '../entities/view-type';
import { Language, LANGUAGES } from '../entities/languages';
import { FirstDaysOfWeek } from '../entities/locales';
import { getWeekDaysRange } from '../utils/date';
import { getDefaultLanguage } from './i18nService';


const defaultLanguage = getDefaultLanguage();

const getParts = (date: Date, formatter: Intl.DateTimeFormat) => formatter
  .formatToParts(date)
  .filter((part: { type: string, value: string }) => part.type !== 'literal')
  .reduce((acc, { type, value }) => ({ ...acc, [type]: value }), {});

export const getPeriodParameters = (type: ViewType) => (date: Date, lang = defaultLanguage) => fold<ViewType, { [key: string]: string | number }>({
  cycle: () => getParts(date, new Intl.DateTimeFormat(lang, { month: 'short', day: 'numeric' })) as {
    month: string;
    day: number;
  },
  monthly: () => getParts(date, new Intl.DateTimeFormat(lang, { month: 'long', year: 'numeric' })) as {
    month: string;
    year: number;
  },
  /*  daily: () => getParts(date, new Intl.DateTimeFormat(lang, { weekday: 'short', day: 'numeric', month: 'long', year: 'numeric' })) as {
      weekday: number;
      month: string;
      day: number;
      year: number;
    },*/
})(type);

export const getWeekDays = (weekFirstDay: FirstDaysOfWeek) => (type: 'short' | 'long' = 'short', lang: Language = defaultLanguage): string[] => {
  const range = getWeekDaysRange(weekFirstDay);

  return range.map((date) => {
    const { weekday } = getParts(date, new Intl.DateTimeFormat(lang, { weekday: type })) as {
      weekday: string;
    };
    return weekday;
  });
};

export const getDateStringFR = (lang: Language = defaultLanguage) => (date: Date) => {
  const parameters = getParts(date, new Intl.DateTimeFormat(lang, { weekday: 'long', day: 'numeric', month: 'long', year: 'numeric' })) as {
    weekday: number;
    month: string;
    day: number;
    year: number;
  };
  return `${parameters.weekday} ${parameters.day} ${parameters.month} ${parameters.year}`;
};

export const getDateStringNumericFR = (lang: Language = defaultLanguage) => (date: Date) => {
  const parameters = getParts(date, new Intl.DateTimeFormat(lang, { day: 'numeric', month: 'numeric' })) as {
    month: number;
    day: number;
  };
  return `${parameters.day}/${parameters.month}`;
};

export const getDateStringNumericEN = (lang: Language = defaultLanguage) => (date: Date) => {
  const parameters = getParts(date, new Intl.DateTimeFormat(lang, { day: 'numeric', month: 'numeric' })) as {
    month: number;
    day: number;
  };
  return `${parameters.month}/${parameters.day}`;
};

export const getDateStringEN = (date: Date) => date.toDateString();

export const getDateString = (date: Date, lang: Language = defaultLanguage) =>
  ((lang === LANGUAGES.FR)
    ? getDateStringFR(lang)
    : getDateStringEN)(date);

export const getDateStringNumeric = (date: Date, lang: Language = defaultLanguage) =>
  ((lang === LANGUAGES.FR)
    ? getDateStringNumericFR
    : getDateStringNumericEN)(lang)(date);

export const getFullDateStringNumeric = (date: Date, lang: Language = defaultLanguage) =>
  `${getDateStringNumeric(date, lang)}/${date.getFullYear()}`;