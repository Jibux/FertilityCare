#!/bin/bash


BUILD="true"
[ "$1" = "no_build" ] && BUILD="false"

if [ "$BUILD" = "true" ]; then
	npm run build || exit 1
fi

firebase deploy

