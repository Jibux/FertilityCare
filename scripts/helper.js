const { readdirSync, readFileSync } = require('fs');

const getLanguagesList = () => readdirSync(`${__dirname}/../src/i18n/`, { withFileTypes: true })
  .filter(dirent => dirent.isDirectory())
  .map(dirent => dirent.name);

const getLanguageTranslations = (lang) => Promise.resolve()
  .then(() => readFileSync(`${__dirname}/../src/i18n/${lang}/index.json`))
  .then(JSON.parse)
  .then(translations => ({
    lang,
    translations
  }));

  module.exports = {
    getLanguageTranslations,
    getLanguagesList
  };